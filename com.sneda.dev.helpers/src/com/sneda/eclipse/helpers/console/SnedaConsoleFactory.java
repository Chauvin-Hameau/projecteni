/*
 * Copyright SNEDA
 * (c) All rights reserved 2009.
 */
package com.sneda.eclipse.helpers.console;

import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleFactory;
import org.eclipse.ui.console.IConsoleManager;

import com.sneda.eclipse.helpers.internal.Activator;

/**
 * @author nio
 * 
 */
public class SnedaConsoleFactory implements IConsoleFactory {

  public void openConsole() {
    showConsole();
  }

  public static void showConsole() {
    SnedaOutputConsole console = Activator.getDefault().getConsole();
    if (console != null) {
      IConsoleManager manager = ConsolePlugin.getDefault().getConsoleManager();
      IConsole[] existing = manager.getConsoles();
      boolean exists = false;
      for (int i = 0; i < existing.length; i++) {
        if (console == existing[i]) exists = true;
      }
      if (!exists) manager.addConsoles(new IConsole[] {console});
      manager.showConsoleView(console);
    }
  }

  public static void closeConsole() {
    IConsoleManager manager = ConsolePlugin.getDefault().getConsoleManager();
    SnedaOutputConsole console = Activator.getDefault().getConsole();
    if (console != null) {
      manager.removeConsoles(new IConsole[] {console});
      // ConsolePlugin.getDefault().getConsoleManager().addConsoleListener(console.new
      // MyLifecycle());
    }
  }
}

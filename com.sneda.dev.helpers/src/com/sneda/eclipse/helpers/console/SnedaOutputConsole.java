/*
 * Copyright SNEDA
 * (c) All rights reserved 2009.
 */
package com.sneda.eclipse.helpers.console;

import org.eclipse.ui.console.MessageConsole;

/**
 * @author nio
 * 
 */
public class SnedaOutputConsole extends MessageConsole {

  public static final String CONSOLE_ID = "SNEDA";

  public SnedaOutputConsole() {
    super(CONSOLE_ID, null);
  }

  public void shutdown() {
    super.dispose();
  }
}

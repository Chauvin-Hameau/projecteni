/*
 * Copyright SNEDA
 * (c) All rights reserved 2009.
 */
package com.sneda.eclipse.helpers.console;

import org.eclipse.ui.IActionBars;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsolePageParticipant;
import org.eclipse.ui.part.IPageBookViewPage;

import com.sneda.eclipse.helpers.internal.console.ConsoleRemoveAction;

/**
 * @author nio
 * 
 */
public class SnedaConsolePageParticipant implements IConsolePageParticipant {

  private ConsoleRemoveAction consoleRemoveAction;

  public void init(IPageBookViewPage page, IConsole console) {
    this.consoleRemoveAction = new ConsoleRemoveAction();
    IActionBars bars = page.getSite().getActionBars();
    bars.getToolBarManager().appendToGroup(IConsoleConstants.LAUNCH_GROUP, consoleRemoveAction);
  }

  public void dispose() {
    this.consoleRemoveAction = null;
  }

  public void activated() {}

  public void deactivated() {}

  @SuppressWarnings("rawtypes")
  public Object getAdapter(Class adapter) {
    return null;
  }
}

package com.sneda.eclipse.helpers.template;

import java.util.UUID;

import org.eclipse.jface.text.templates.TemplateContext;
import org.eclipse.jface.text.templates.TemplateVariableResolver;

/**
 * @author nio
 */
public class UUIDResolver extends TemplateVariableResolver {

  @Override
  protected String resolve(TemplateContext context) {
    return UUID.randomUUID().toString();
  }
}

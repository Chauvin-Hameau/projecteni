package com.sneda.eclipse.helpers.internal.wizzard.impl.editorihmetendue;

import com.sun.net.httpserver.Filter.Chain;

public class test {

	public static void main(String[] args) {
		chaines("/test/src/main/java");
	}

	private static String[] chaines(String chaine){
		String[] splitChaine = chaine.substring(1, chaine.length()).split("/");
		if(splitChaine.length >= 4) {
			if("src".equals(splitChaine[1]) && "main".equals(splitChaine[2]) && "java".equals(splitChaine[3])) {
				return createSrcMainJava(splitChaine);
			}else {
				return createSrc(splitChaine);
			}
		}else {
			return createSrc(splitChaine);
		}
	}

	private static String[] createSrc(String[] splitChaine) {
		String[] chaineSrc = new String[2];
		StringBuilder sb = new StringBuilder();
		chaineSrc[0] = '\\' +splitChaine[0] + '\\' + splitChaine[1];

		for(int i = 2; i < splitChaine.length; i++) {
			if(!(i + 1 == splitChaine.length)) {
				sb.append(splitChaine[i] + '\\');
			}else {
				sb.append(splitChaine[i]);
			}
		}
		chaineSrc[1] = sb.toString().replaceAll(java.util.regex.Matcher.quoteReplacement("\\"), ".");
		System.out.println("Source folder : " + chaineSrc[0] + "  Package : " + chaineSrc[1]);
		return chaineSrc;
 	}

	private static String[] createSrcMainJava(String[] splitChaine) {
		String[] chaineSrc = new String[2];
		StringBuilder sb = new StringBuilder();
		chaineSrc[0] = '\\' +splitChaine[0] + '\\' + splitChaine[1] + '\\' + splitChaine[2] + '\\' + splitChaine[3];

		for(int i = 4; i < splitChaine.length; i++) {
			if(!(i + 1 == splitChaine.length)) {
				sb.append(splitChaine[i] + '\\');
			}else {
				sb.append(splitChaine[i]);
			}
		}
		chaineSrc[1] = sb.toString().replaceAll(java.util.regex.Matcher.quoteReplacement("\\"), ".");
		System.out.println("Source folder : " + chaineSrc[0] + "  Package : " + chaineSrc[1]);
		return chaineSrc;
	}

}

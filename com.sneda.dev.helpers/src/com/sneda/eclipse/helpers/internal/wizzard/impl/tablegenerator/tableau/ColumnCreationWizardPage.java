package com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator.tableau;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableItem;

import com.sneda.eclipse.helpers.internal.wizzard.GenericWizardModelObject;

/**
 * @author dimitric
 *
 */
public class ColumnCreationWizardPage extends WizardPage {

  public static final String NAME = "com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator.tableau.ColumnCreationWizardPage";

  private GenericWizardModelObject genericWizardModelObject;

  private ColumnCreationWizard columnCreationWizard;

  private Button btnaddColumn;
  private Button btnDellColumn;

  private TableCreateColumn table;

  private Column columnSelectionnee;

  private Boolean columnSizeIsNumber;

  private List<Column> bufferColumns;

  /**
   * @param pageName
   * @param columnCreationWizard
   */
  protected ColumnCreationWizardPage(String pageName, GenericWizardModelObject genericWizardModelObject,
      ColumnCreationWizard columnCreationWizard) {
    super(pageName);
    this.setTitle(pageName);
    this.setDescription("Veuillez saisir les informations des colonnes.");
    this.genericWizardModelObject = genericWizardModelObject;
    this.columnCreationWizard = columnCreationWizard;
  }

  /*
   * (non-Javadoc)
   * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
   */
  @Override
  public void createControl(Composite parent) {
    Composite composite = new Composite(parent, SWT.NONE);
    composite.setLayout(new GridLayout(2, false));
    GridDataFactory.fillDefaults().grab(true, true).applyTo(composite);

    tableComposite(composite);
    gestionComposite(composite);

    setControl(composite);
    addListener();
    init();
  }

  public void tableComposite(Composite parent) {
    Composite cpoTable = new Composite(parent, SWT.NONE);
    cpoTable.setLayout(new GridLayout(1, false));
    GridDataFactory.fillDefaults().grab(true, true).applyTo(cpoTable);

    table = new TableCreateColumn(cpoTable, SWT.BORDER | SWT.FULL_SELECTION, this);
  }

  public void gestionComposite(Composite parent) {
    Composite cpoGestion = new Composite(parent, SWT.NONE);
    cpoGestion.setLayout(new GridLayout(1, true));
    GridDataFactory.fillDefaults().grab(true, true).applyTo(cpoGestion);

    btnaddColumn = new Button(cpoGestion, SWT.NONE);
    btnaddColumn.setText("Ajouter colonne");
    btnaddColumn.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false));

    btnDellColumn = new Button(cpoGestion, SWT.NONE);
    btnDellColumn.setText("Supprimer colonne");
    btnDellColumn.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false));
  }

  /**
   * Initialisation du tableau avec une liste vide d'objet "Column" si 1e ouverture.
   */
  public void init() {
    Wizard page = (Wizard) this.getWizard();
    page.setWindowTitle("Création de colonnes");

    bufferColumns = new ArrayList<>();

    if (!getColumnCreationWizard().getColumns().isEmpty()) {
      bufferColumns.addAll(CopyData(getColumnCreationWizard().getColumns()));
      getTable().setInput(getBufferColumns());
    }
    this.setPageComplete(false);
  }

  /**
   * Ajout des différent évenement de la page
   */
  public void addListener() {
    getTable().addSelectionChangedListener(new ISelectionChangedListener() {

      @Override
      public void selectionChanged(SelectionChangedEvent event) {
        IStructuredSelection selection = (IStructuredSelection) event.getSelection();
        setColumnSelectionnee((Column) selection.getFirstElement());
        validatePage();
      }
    });

    btnaddColumn.addSelectionListener(new SelectionListener() {

      @Override
      public void widgetSelected(SelectionEvent e) {
        Column newColumn = new Column("Nom", "100", "Integer", "true", "false", true);
        getBufferColumns().add(newColumn);
        getTable().setInput(getBufferColumns());
        getTable().refresh();
        validatePage();
      }

      @Override
      public void widgetDefaultSelected(SelectionEvent e) {
        /* Do nothing */
      }
    });

    btnDellColumn.addSelectionListener(new SelectionListener() {

      @Override
      public void widgetSelected(SelectionEvent e) {
        if (getColumnSelectionnee() != null) {
          getBufferColumns().remove(getColumnSelectionnee());
          getTable().refresh();
          validatePage();
        }
      }

      @Override
      public void widgetDefaultSelected(SelectionEvent e) {
        /* Do nothing */
      }
    });
  }

  /**
   * Récupère toutes les lignes du tableau, convertie les lignes en objet "Column" et fabrique la
   * chaîne de caractère final.
   */
  public void buildAllColumn() {
    List<Column> columns = new ArrayList<>();
    StringBuilder sb = new StringBuilder();
    int indexTable = 0;

    for (TableItem item : getTable().getTable().getItems()) {
      if (item.getData() instanceof Column) {
        Column col = (Column) item.getData();
        columns.add(col);
      }
    }

    for (Column col : columns) {
      sb.append(buildColumn(col));
      if (indexTable < columns.size()) {
        if (col.isEditable()) {
          sb.append(";");
          sb.append(indexTable);
        }
        if (indexTable < columns.size() - 1) {
          sb.append("//");
        }
      }
      indexTable++;
    }

    getColumnCreationWizard().setColumns(columns);
    getGenericWizardModelObject().setValue(sb.toString());
  }

  /**
   * @param col correspond à l'objet Column
   * @return une chaine comme "name;type;size;resizable;masquable:editable" de type StringBuilder
   */
  public String buildColumn(Column col) {
    StringBuilder sb = new StringBuilder();

    sb.append(col.getNameColumn().trim() + ";");
    sb.append(col.getSizeColumn().trim() + ";");
    sb.append(col.getTypeColumn().trim() + ";");
    sb.append(col.getResizableColumn().trim() + ";");
    sb.append(col.getMasquableColumn().trim() + ";");
    sb.append(col.isEditable());
    return sb.toString();
  }

  /**
   * Verify if the page is valid or not. If is false we can't click on finish button
   */
  public void validatePage() {
    this.setPageComplete(getBufferColumns().isEmpty() ? false : true);
    this.setPageComplete(verifyColumns(getBufferColumns()) ? false : true);
  }

  public boolean verifyColumns(List<Column> columns) {
    boolean error = false;
    boolean nameEmpty = false;
    boolean sizeFalse = false;
    StringBuilder sb = new StringBuilder();

    for (Column currentColumn : columns) {
      if (StringUtils.isBlank(currentColumn.getNameColumn().trim())) {
        nameEmpty = true;
        error = true;
      }

      if (!NumberUtils.isNumber(currentColumn.getSizeColumn())) {
        currentColumn.setSizeColumn("");
        sizeFalse = true;
        error = true;
      }
    }

    if (!error) {
      table.getColumnCreationWizardPage().setErrorMessage(null);
    } else {
      if (nameEmpty) {
        sb.append("Le champ 'Nom' ne peut pas être vide." + "\n");
      }
      if (sizeFalse) {
        sb.append("Le champ 'Taille' n'accepte que des entiers." + "\n");
      }
      table.getColumnCreationWizardPage().setErrorMessage(sb.toString());
    }

    return error;
  }

  /**
   * @param columnList Copy data from list to another list
   * @return list of data Column
   */
  public List<Column> CopyData(List<Column> columnList) {
    List<Column> columns = new ArrayList<>();

    for (Column current : columnList) {
      columns.add(new Column(current.getNameColumn(), current.getSizeColumn(), current.getTypeColumn(),
          current.getResizableColumn(), current.getMasquableColumn(), current.isEditable()));
    }

    return columns;
  }

  /**
   * @return the table
   */
  public TableCreateColumn getTable() {
    return table;
  }

  /**
   * @param table the table to set
   */
  public void setTable(TableCreateColumn table) {
    this.table = table;
  }

  /**
   * @return the columnSelectionnee
   */
  public Column getColumnSelectionnee() {
    return columnSelectionnee;
  }

  /**
   * @param columnSelectionnee the columnSelectionnee to set
   */
  public void setColumnSelectionnee(Column columnSelectionnee) {
    this.columnSelectionnee = columnSelectionnee;
  }

  /**
   * @return the genericWizardModelObject
   */
  public GenericWizardModelObject getGenericWizardModelObject() {
    return genericWizardModelObject;
  }

  /**
   * @param genericWizardModelObject the genericWizardModelObject to set
   */
  public void setGenericWizardModelObject(GenericWizardModelObject genericWizardModelObject) {
    this.genericWizardModelObject = genericWizardModelObject;
  }

  /**
   * @return the columnSizeIsNumber
   */
  public Boolean getColumnSizeIsNumber() {
    return columnSizeIsNumber;
  }

  /**
   * @param columnSizeIsNumber the columnSizeIsNumber to set
   */
  public void setColumnSizeIsNumber(Boolean columnSizeIsNumber) {
    this.columnSizeIsNumber = columnSizeIsNumber;
  }

  /**
   * @return the columnCreationWizard
   */
  public ColumnCreationWizard getColumnCreationWizard() {
    return columnCreationWizard;
  }

  /**
   * @return the bufferColumns
   */
  public List<Column> getBufferColumns() {
    return bufferColumns;
  }

  /**
   * @param bufferColumns the bufferColumns to set
   */
  public void setBufferColumns(List<Column> bufferColumns) {
    this.bufferColumns = bufferColumns;
  }

}

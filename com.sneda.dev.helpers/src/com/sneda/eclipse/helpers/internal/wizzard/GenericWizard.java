package com.sneda.eclipse.helpers.internal.wizzard;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.wizard.Wizard;

import com.sneda.eclipse.helpers.internal.Activator;

/**
 * @author nio
 */
public abstract class GenericWizard extends Wizard {

	public GenericWizardPage page;

	public GenericWizard(String windowTitle) {
		setWindowTitle(windowTitle);
	}

	protected abstract String getPageTitle();

	protected abstract String getPageDescription();

	protected abstract GenericWizardModel getModel();

	protected abstract String getSettingsId();


	@Override
	public IDialogSettings getDialogSettings() {
		IDialogSettings settings = Activator.getDefault().getDialogSettings();
		IDialogSettings section = settings.getSection(getSettingsId());
		if (section == null) {
			section = settings.addNewSection(getSettingsId());
		}
		return section;
	}

	@Override
	public void addPages() {
		if (getPage(GenericWizardPage.UUID) == null) {
			addPage(new GenericWizardPage(getPageTitle(), getPageDescription(), getSettingsId()));
		}
	}

	protected void saveState() {
		// Save state
		for (GenericWizardModelObject elmt : getModel().getElements()) {
			getDialogSettings().put(elmt.getMsge(), elmt.getValue().toString());
		}
	}

	protected abstract Object getCmdParam();

	protected abstract boolean executeCmd(Object param);

	@Override
	public boolean performFinish() {
		if (getPage(GenericWizardPage.UUID) == null) {
			return false;
		}

		saveState();

		final Object param = getCmdParam();
		Job job = new Job("Executing command...") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				executeCmd(param);
				return new Status(IStatus.OK, Activator.PLUGIN_ID, "success");
			}
		};

		// if short action, otherwise is long Job.LONG
		job.setPriority(Job.SHORT);

		// start as soon as possible
		job.schedule();

		return true;
	}
}

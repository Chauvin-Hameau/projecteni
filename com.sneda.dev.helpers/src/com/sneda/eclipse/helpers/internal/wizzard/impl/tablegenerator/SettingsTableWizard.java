package com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.wizard.Wizard;

import com.sneda.eclipse.helpers.internal.wizzard.GenericWizardModelObject;

/**
 * @author dchauvinhameau
 *
 */
public class SettingsTableWizard extends Wizard {

  SettingsTableWizardPage settingsTableWizardPage;
  GenericWizardModelObject genericWizardModelObject;
  TableViewer tableViewer;

  /**
   * @param elmt
   * @param tableViewer
   */
  public SettingsTableWizard(GenericWizardModelObject elmt, TableViewer tableViewer) {
    this.genericWizardModelObject = elmt;
    this.tableViewer = tableViewer;
    this.settingsTableWizardPage = new SettingsTableWizardPage("Configuration tableau modifiable",
        genericWizardModelObject);
  }

  @Override
  public boolean performFinish() {
    settingsTableWizardPage.buildLineSettings();
    tableViewer.refresh();
    return true;
  }

  @Override
  public void addPages() {
    addPage(settingsTableWizardPage);
  }

}

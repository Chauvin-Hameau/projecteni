package com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator;

import java.util.List;
import java.util.Map;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;
import com.sneda.eclipse.helpers.internal.cmd.jet.UpdateTableGenerator;
import com.sneda.eclipse.helpers.internal.wizzard.GenericGeneratorWizard;
import com.sneda.eclipse.helpers.internal.wizzard.impl.Messages;

/**
 * @author dchauvinhameau
 *
 */
public class UpdateTableGeneratorWizard extends GenericGeneratorWizard {

  /**
   * @param windowTitle
   */
  public UpdateTableGeneratorWizard(String windowTitle) {
    super(windowTitle);
  }

  @Override
  protected Map<String, ValueObject> buildInput() {
    return new UpdateTableGenerator().buildInput();
  }

  @Override
  protected String getPageTitle() {
    return Messages.UpdateTableGeneratorWizard_1;
  }

  @Override
  protected String getPageDescription() {
    return Messages.UpdateTableGeneratorWizard_0;
  }

  @Override
  protected String getSettingsId() {
    return "UPDATE_TABLE_GENERATOR_SETTINGS"; //$NON-NLS-1$
  }

  @Override
  protected boolean executeCmd(Object param) {
    new UpdateTableGenerator().execute((List<ValueObject>) param);
    return true;
  }

}

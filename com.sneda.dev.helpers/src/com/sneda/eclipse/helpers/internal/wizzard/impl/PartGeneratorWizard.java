package com.sneda.eclipse.helpers.internal.wizzard.impl;

import java.util.List;
import java.util.Map;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;
import com.sneda.eclipse.helpers.internal.cmd.jet.PartGenerator;
import com.sneda.eclipse.helpers.internal.wizzard.GenericGeneratorWizard;

/**
 * @author nio
 */
public class PartGeneratorWizard extends GenericGeneratorWizard {

  public PartGeneratorWizard() {
    super("PartGenerator"); //$NON-NLS-1$
  }

  @Override
  protected String getPageDescription() {
    return Messages.PartGeneratorWizard_1;
  }

  @Override
  protected String getPageTitle() {
    return Messages.PartGeneratorWizard_2;
  }

  @Override
  protected String getSettingsId() {
    return "PART_GENERATOR_SETTINGS"; //$NON-NLS-1$
  }

  @Override
  protected Map<String, ValueObject> buildInput() {
    return new PartGenerator().buildInput();
  }

  @SuppressWarnings("unchecked")
  @Override
  protected boolean executeCmd(Object param) {
    new PartGenerator().execute((List<ValueObject>) param);
    return true;
  }
}

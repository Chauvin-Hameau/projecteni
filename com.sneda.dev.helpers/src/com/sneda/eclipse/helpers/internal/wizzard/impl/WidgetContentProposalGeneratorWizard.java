package com.sneda.eclipse.helpers.internal.wizzard.impl;

import java.util.List;
import java.util.Map;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;
import com.sneda.eclipse.helpers.internal.cmd.jet.WidgetContentProposalGenerator;
import com.sneda.eclipse.helpers.internal.wizzard.GenericGeneratorWizard;

/**
 * @author daf
 */
public class WidgetContentProposalGeneratorWizard extends GenericGeneratorWizard {

  public WidgetContentProposalGeneratorWizard() {
    super("WidgetContentProposalGenerator"); //$NON-NLS-1$
  }

  @Override
  protected String getPageDescription() {
    return Messages.WizardGeneratorWizard_2;
  }

  @Override
  protected String getPageTitle() {
    return Messages.WidgetContentProposalGeneratorWizard_titre;
  }

  @Override
  protected String getSettingsId() {
    return "WIDGET_CONTENT_PROPOSAL_GENERATOR_SETTINGS"; //$NON-NLS-1$
  }

  @Override
  protected Map<String, ValueObject> buildInput() {
    return new WidgetContentProposalGenerator().buildInput();
  }

  @SuppressWarnings("unchecked")
  @Override
  protected boolean executeCmd(Object param) {
    new WidgetContentProposalGenerator().execute((List<ValueObject>) param);
    return true;
  }
}

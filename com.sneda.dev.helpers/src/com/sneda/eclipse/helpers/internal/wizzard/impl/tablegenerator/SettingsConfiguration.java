package com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator;

/**
 * @author dchauvinhameau
 *
 */
public class SettingsConfiguration {

  private boolean isAssistant;
  private boolean isCells;
  private boolean isCellAndAssistant;
  private boolean isAddItem;
  private boolean isUpdateItem;
  private boolean isUpdateItems;
  private boolean isDellItem;
  private boolean isDuplicateItem;
  private boolean isMovingItem;
  private boolean isMonoSelection;
  private boolean isMultiSelection;

  /**
   * @param isAssistant
   * @param isCells
   * @param isCellAndAssistant
   * @param isAddItem
   * @param isUpdateItem
   * @param isUpdateItems
   * @param isDellItem
   * @param isDuplicateItem
   * @param isMovingItem
   * @param isMonoSelection
   * @param isMultiSelection
   */
  public SettingsConfiguration(boolean isAssistant, boolean isCells, boolean isCellAndAssistant, boolean isAddItem,
      boolean isUpdateItem, boolean isUpdateItems, boolean isDellItem, boolean isDuplicateItem, boolean isMovingItem,
      boolean isMonoSelection, boolean isMultiSelection) {
    super();
    this.isAssistant = isAssistant;
    this.isCells = isCells;
    this.isCellAndAssistant = isCellAndAssistant;
    this.isAddItem = isAddItem;
    this.isUpdateItem = isUpdateItem;
    this.isUpdateItems = isUpdateItems;
    this.isDellItem = isDellItem;
    this.isDuplicateItem = isDuplicateItem;
    this.isMovingItem = isMovingItem;
    this.isMonoSelection = isMonoSelection;
    this.isMultiSelection = isMultiSelection;
  }

  /**
   * @return the isAssistant
   */
  public boolean isAssistant() {
    return isAssistant;
  }

  /**
   * @param isAssistant the isAssistant to set
   */
  public void setAssistant(boolean isAssistant) {
    this.isAssistant = isAssistant;
  }

  /**
   * @return the isCells
   */
  public boolean isCells() {
    return isCells;
  }

  /**
   * @param isCells the isCells to set
   */
  public void setCells(boolean isCells) {
    this.isCells = isCells;
  }

  /**
   * @return the isAddItem
   */
  public boolean isAddItem() {
    return isAddItem;
  }

  /**
   * @param isAddItem the isAddItem to set
   */
  public void setAddItem(boolean isAddItem) {
    this.isAddItem = isAddItem;
  }

  /**
   * @return the isUpdateItem
   */
  public boolean isUpdateItem() {
    return isUpdateItem;
  }

  /**
   * @param isUpdateItem the isUpdateItem to set
   */
  public void setUpdateItem(boolean isUpdateItem) {
    this.isUpdateItem = isUpdateItem;
  }

  /**
   * @return the isUpdateItems
   */
  public boolean isUpdateItems() {
    return isUpdateItems;
  }

  /**
   * @param isUpdateItems the isUpdateItems to set
   */
  public void setUpdateItems(boolean isUpdateItems) {
    this.isUpdateItems = isUpdateItems;
  }

  /**
   * @return the isDellItem
   */
  public boolean isDellItem() {
    return isDellItem;
  }

  /**
   * @param isDellItem the isDellItem to set
   */
  public void setDellItem(boolean isDellItem) {
    this.isDellItem = isDellItem;
  }

  /**
   * @return the isDuplicateItem
   */
  public boolean isDuplicateItem() {
    return isDuplicateItem;
  }

  /**
   * @param isDuplicateItem the isDuplicateItem to set
   */
  public void setDuplicateItem(boolean isDuplicateItem) {
    this.isDuplicateItem = isDuplicateItem;
  }

  /**
   * @return the isMovingItem
   */
  public boolean isMovingItem() {
    return isMovingItem;
  }

  /**
   * @param isMovingItem the isMovingItem to set
   */
  public void setMovingItem(boolean isMovingItem) {
    this.isMovingItem = isMovingItem;
  }

  /**
   * @return the isMonoSelection
   */
  public boolean isMonoSelection() {
    return isMonoSelection;
  }

  /**
   * @param isMonoSelection the isMonoSelection to set
   */
  public void setMonoSelection(boolean isMonoSelection) {
    this.isMonoSelection = isMonoSelection;
  }

  /**
   * @return the isMultiSelection
   */
  public boolean isMultiSelection() {
    return isMultiSelection;
  }

  /**
   * @param isMultiSelection the isMultiSelection to set
   */
  public void setMultiSelection(boolean isMultiSelection) {
    this.isMultiSelection = isMultiSelection;
  }

  /**
   * @return the isCellAndAssistant
   */
  public boolean isCellAndAssistant() {
    return isCellAndAssistant;
  }

  /**
   * @param isCellAndAssistant the isCellAndAssistant to set
   */
  public void setCellAndAssistant(boolean isCellAndAssistant) {
    this.isCellAndAssistant = isCellAndAssistant;
  }

}

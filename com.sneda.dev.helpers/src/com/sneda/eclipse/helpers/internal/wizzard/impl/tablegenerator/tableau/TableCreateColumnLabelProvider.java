package com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator.tableau;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ViewerCell;

/**
 * @author dimitric
 *
 */
public class TableCreateColumnLabelProvider extends CellLabelProvider {

  private final TableCreateColumnColumn colonne;

  /**
   * <p>
   * Constructeur.
   * </p>
   *
   * @param column Colonne � initialiser.
   * @param modele Mod�le de l'use case utilisant le tableau modifiable.
   */
  public TableCreateColumnLabelProvider(TableCreateColumnColumn column) {
    super();
    this.colonne = column;
  }

  /*
   * (non-Javadoc)
   * @see org.eclipse.jface.viewers.CellLabelProvider#update(org.eclipse.jface.viewers.ViewerCell)
   */
  @Override
  public void update(ViewerCell cell) {
    if (cell != null) {
      String text = ""; //$NON-NLS-1$
      Object element = cell.getElement();
      if (element instanceof Column) {
        Column column = (Column) element;

        switch (colonne) {
          case NAME:
            if (StringUtils.isNotBlank(column.getNameColumn())) {
              text = column.getNameColumn();
            }
            break;

          case SIZE:
            if (StringUtils.isNotBlank(column.getSizeColumn())) {
              text = column.getSizeColumn();
            }
            break;

          case TYPE:
            if (StringUtils.isNotBlank(column.getTypeColumn())) {
              text = column.getTypeColumn();
            }
            break;

          case RESIZABLE:
            if (StringUtils.isNotBlank(column.getResizableColumn())) {
              text = column.getResizableColumn();
            }
            break;

          case MASQUABLE:
            if (StringUtils.isNotBlank(column.getMasquableColumn())) {
              text = column.getMasquableColumn();
            }
            break;
        }
        cell.setText(text);
      }
    }

  }
}

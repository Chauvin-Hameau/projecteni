/*
 * Copyright SNEDA
 * (c) All rights reserved 2009.
 */
package com.sneda.eclipse.helpers.internal.wizzard.impl;

import java.util.List;
import java.util.Map;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;
import com.sneda.eclipse.helpers.internal.cmd.jet.EditorGenerator;
import com.sneda.eclipse.helpers.internal.wizzard.GenericGeneratorWizard;

/**
 * @author nio
 */
public class EditorGeneratorWizard extends GenericGeneratorWizard {

  public EditorGeneratorWizard() {
    super("EditorGenerator"); //$NON-NLS-1$
  }

  @Override
  protected String getPageDescription() {
    return Messages.EditorGeneratorWizard_0;
  }

  @Override
  protected String getPageTitle() {
    return Messages.EditorGeneratorWizard_1;
  }

  @Override
  protected String getSettingsId() {
    return "EDITOR_GENERATOR_SETTINGS"; //$NON-NLS-1$
  }

  @Override
  protected Map<String, ValueObject> buildInput() {
    return new EditorGenerator().buildInput();
  }

  @SuppressWarnings("unchecked")
  @Override
  protected boolean executeCmd(Object param) {
    new EditorGenerator().execute((List<ValueObject>) param);
    return true;
  }



}

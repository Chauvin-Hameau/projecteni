package com.sneda.eclipse.helpers.internal.wizzard.impl.editorihmetendue;

public class EditeurIhmEtendueParams {

	private boolean isIhmEtendue;
	private String nomEntite;
	private String cheminDAOSrcFolder;
	private String cheminDAOPackage;
	private String cheminNMCMapperSrcFolder;
	private String cheminNMCMapperPackage;
	private String cheminInputCreatorChargerSrcFolder;
	private String cheminInputCreatorChargerPackage;
	private String cheminInputCreatorCreeModifierSrcFolder;
	private String cheminInputCreatorCreerModifierPackage;
	private String cheminProperties;
	private String libelleParametre;
	private String variableParametre;

	public EditeurIhmEtendueParams(boolean isIhmEtendue, String nomEntite, String cheminDAOSrcFolder,
			String cheminDAOPackage, String cheminNMCMapperSrcFolder, String cheminNMCMapperPackage,
			String cheminInputCreatorChargerSrcFolder, String cheminInputCreatorChargerPackage,
			String cheminInputCreatorCreeModifierSrcFolder, String cheminInputCreatorCreerModifierPackage,
			String cheminProperties, String libelleParametre, String variableParametre) {
		this.isIhmEtendue = isIhmEtendue;
		this.nomEntite = nomEntite;
		this.cheminDAOSrcFolder = cheminDAOSrcFolder;
		this.cheminDAOPackage = cheminDAOPackage;
		this.cheminNMCMapperSrcFolder = cheminNMCMapperSrcFolder;
		this.cheminNMCMapperPackage = cheminNMCMapperPackage;
		this.cheminInputCreatorChargerSrcFolder = cheminInputCreatorChargerSrcFolder;
		this.cheminInputCreatorChargerPackage = cheminInputCreatorChargerPackage;
		this.cheminInputCreatorCreeModifierSrcFolder = cheminInputCreatorCreeModifierSrcFolder;
		this.cheminInputCreatorCreerModifierPackage = cheminInputCreatorCreerModifierPackage;
		this.cheminProperties = cheminProperties;
		this.libelleParametre = libelleParametre;
		this.variableParametre = variableParametre;
	}

	public EditeurIhmEtendueParams() {
	}


	public boolean isIhmEtendue() {
		return isIhmEtendue;
	}



	public void setIhmEtendue(boolean isIhmEtendue) {
		this.isIhmEtendue = isIhmEtendue;
	}



	public String getNomEntite() {
		return nomEntite;
	}



	public void setNomEntite(String nomEntite) {
		this.nomEntite = nomEntite;
	}



	public String getCheminDAOSrcFolder() {
		return cheminDAOSrcFolder;
	}



	public void setCheminDAOSrcFolder(String cheminDAOSrcFolder) {
		this.cheminDAOSrcFolder = cheminDAOSrcFolder;
	}



	public String getCheminDAOPackage() {
		return cheminDAOPackage;
	}



	public void setCheminDAOPackage(String cheminDAOPackage) {
		this.cheminDAOPackage = cheminDAOPackage;
	}



	public String getCheminNMCMapperSrcFolder() {
		return cheminNMCMapperSrcFolder;
	}



	public void setCheminNMCMapperSrcFolder(String cheminNMCMapperSrcFolder) {
		this.cheminNMCMapperSrcFolder = cheminNMCMapperSrcFolder;
	}



	public String getCheminNMCMapperPackage() {
		return cheminNMCMapperPackage;
	}



	public void setCheminNMCMapperPackage(String cheminNMCMapperPackage) {
		this.cheminNMCMapperPackage = cheminNMCMapperPackage;
	}



	public String getCheminInputCreatorChargerSrcFolder() {
		return cheminInputCreatorChargerSrcFolder;
	}



	public void setCheminInputCreatorChargerSrcFolder(String cheminInputCreatorChargerSrcFolder) {
		this.cheminInputCreatorChargerSrcFolder = cheminInputCreatorChargerSrcFolder;
	}



	public String getCheminInputCreatorChargerPackage() {
		return cheminInputCreatorChargerPackage;
	}



	public void setCheminInputCreatorChargerPackage(String cheminInputCreatorChargerPackage) {
		this.cheminInputCreatorChargerPackage = cheminInputCreatorChargerPackage;
	}



	public String getCheminInputCreatorCreeModifierSrcFolder() {
		return cheminInputCreatorCreeModifierSrcFolder;
	}



	public void setCheminInputCreatorCreeModifierSrcFolder(String cheminInputCreatorCreeModifierSrcFolder) {
		this.cheminInputCreatorCreeModifierSrcFolder = cheminInputCreatorCreeModifierSrcFolder;
	}



	public String getCheminInputCreatorCreerModifierPackage() {
		return cheminInputCreatorCreerModifierPackage;
	}



	public void setCheminInputCreatorCreerModifierPackage(String cheminInputCreatorCreerModifierPackage) {
		this.cheminInputCreatorCreerModifierPackage = cheminInputCreatorCreerModifierPackage;
	}



	public String getCheminProperties() {
		return cheminProperties;
	}



	public void setCheminProperties(String cheminProperties) {
		this.cheminProperties = cheminProperties;
	}



	public String getLibelleParametre() {
		return libelleParametre;
	}



	public void setLibelleParametre(String libelleParametre) {
		this.libelleParametre = libelleParametre;
	}



	public String getVariableParametre() {
		return variableParametre;
	}



	public void setVariableParametre(String variableParametre) {
		this.variableParametre = variableParametre;
	}




}

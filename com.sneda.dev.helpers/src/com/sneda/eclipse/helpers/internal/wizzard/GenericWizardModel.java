package com.sneda.eclipse.helpers.internal.wizzard;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;

/**
 * @author nio
 */
public class GenericWizardModel {

  private List<GenericWizardModelObject> elements;

  public GenericWizardModel() {

  }

  public GenericWizardModel(Collection<GenericWizardModelObject> input) {
    elements = new ArrayList<GenericWizardModelObject>(input);
  }

  public List<GenericWizardModelObject> getElements() {
    return elements;
  }

  public List<ValueObject> getValueObjects() {
    List<ValueObject> visitor = new ArrayList<ValueObject>();
    for (GenericWizardModelObject each : getElements()) {
      each.accept(visitor);
    }
    return visitor;
  }

  public void setElements(List<GenericWizardModelObject> elements) {
    this.elements = elements;
  }
}

package com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import com.sneda.eclipse.helpers.internal.wizzard.GenericWizardModelObject;

/**
 * @author dchauvinhameau
 *
 */
/**
 * @author dchauvinhameau
 *
 */
public class SettingsTableWizardPage extends WizardPage {

  static final String SPLIT = ";";
  static final String IS_FALSE = "false";

  private Label lbTableUpdate;
  private Label lbOptionTable;

  private Button btrAssistant;
  private Button btrCell;
  private Button btrCellAndAssistant;
  private Button chkAddItem;
  private Button chkUpdateItem;
  private Button chkUpdateItems;
  private Button chkDellItem;
  private Button chkDuplicateItem;
  private Button chkMovingItem;

  private Button btrMonoSelection;
  private Button btrMultiSelection;

  private Group groupChkTableUpdate;

  SettingsConfiguration settingsConfiguration;
  GenericWizardModelObject genericWizardModelObject;

  /**
   * @param pageName
   * @param genericWizardModelObject
   */
  protected SettingsTableWizardPage(String pageName, GenericWizardModelObject genericWizardModelObject) {
    super(pageName);
    this.setTitle(pageName);
    this.setDescription("Veuillez renseigner les configurations du tableau modifiable");
    this.settingsConfiguration = new SettingsConfiguration(false, false, false, false, false, false, false, false,
        false, true, false);
    this.genericWizardModelObject = genericWizardModelObject;
  }

  @Override
  public void createControl(Composite parent) {
    Composite cpoMain = new Composite(parent, SWT.NONE);
    cpoMain.setLayout(new GridLayout(1, false));
    GridDataFactory.fillDefaults().grab(true, true).applyTo(cpoMain);

    createCompositeTableUpdate(cpoMain);
    createCompositeOptionTable(cpoMain);
    createCompositeButtonAccess(cpoMain);

    init();
    addListener();

    setControl(cpoMain);
  }

  public void createCompositeTableUpdate(Composite parent) {
    Composite cpoTableUpdate = new Composite(parent, SWT.NONE);
    cpoTableUpdate.setLayout(new GridLayout(4, false));
    GridDataFactory.fillDefaults().grab(true, false).applyTo(cpoTableUpdate);

    lbTableUpdate = new Label(cpoTableUpdate, SWT.NONE);
    lbTableUpdate.setText("Tableau modifiable par : ");

    btrAssistant = new Button(cpoTableUpdate, SWT.RADIO);
    btrAssistant.setText("Assistant");

    btrCell = new Button(cpoTableUpdate, SWT.RADIO);
    btrCell.setText("Cellule");

    btrCellAndAssistant = new Button(cpoTableUpdate, SWT.RADIO);
    btrCellAndAssistant.setText("Les deux");

  }

  public void createCompositeOptionTable(Composite parent) {
    Composite cpoOptionTable = new Composite(parent, SWT.NONE);
    cpoOptionTable.setLayout(new GridLayout(3, false));
    GridDataFactory.fillDefaults().grab(true, false).applyTo(cpoOptionTable);

    lbOptionTable = new Label(cpoOptionTable, SWT.NONE);
    lbOptionTable.setText("Tableau accessible en : ");

    btrMonoSelection = new Button(cpoOptionTable, SWT.RADIO);
    btrMonoSelection.setText("Mono sélection");

    btrMultiSelection = new Button(cpoOptionTable, SWT.RADIO);
    btrMultiSelection.setText("Multi sélection");
  }

  public void createCompositeButtonAccess(Composite parent) {
    Composite cpoButtonAccess = new Composite(parent, SWT.NONE);
    cpoButtonAccess.setLayout(new GridLayout(1, false));
    GridDataFactory.fillDefaults().grab(true, true).applyTo(cpoButtonAccess);

    groupChkTableUpdate = new Group(cpoButtonAccess, SWT.NONE);
    groupChkTableUpdate.setText("Bouton(s) accessible(s) pour le tableau");
    GridDataFactory.fillDefaults().grab(true, true).applyTo(groupChkTableUpdate);

    chkAddItem = new Button(groupChkTableUpdate, SWT.CHECK);
    chkAddItem.setText("Ajouter un item");
    chkAddItem.setLocation(20, 20);
    chkAddItem.pack();

    chkUpdateItem = new Button(groupChkTableUpdate, SWT.CHECK);
    chkUpdateItem.setText("Modifier un item");
    chkUpdateItem.setLocation(20, 40);
    chkUpdateItem.pack();

    chkUpdateItems = new Button(groupChkTableUpdate, SWT.CHECK);
    chkUpdateItems.setText("Modifier plusieurs items");
    chkUpdateItems.setLocation(20, 60);
    chkUpdateItems.pack();

    chkDellItem = new Button(groupChkTableUpdate, SWT.CHECK);
    chkDellItem.setText("Supprimer un item");
    chkDellItem.setLocation(200, 20);
    chkDellItem.pack();

    chkDuplicateItem = new Button(groupChkTableUpdate, SWT.CHECK);
    chkDuplicateItem.setText("Dupliquer un item");
    chkDuplicateItem.setLocation(200, 40);
    chkDuplicateItem.pack();

    chkMovingItem = new Button(groupChkTableUpdate, SWT.CHECK);
    chkMovingItem.setText("Déplacer l'item d'une ligne");
    chkMovingItem.setLocation(200, 60);
    chkMovingItem.pack();

  }

  /**
   * Init data for each checkbox with SettingsConfiguration class
   */
  public void init() {
    Wizard page = (Wizard) this.getWizard();
    page.setWindowTitle("Configuration tableau modifiable");

    getBtrAssistant().setSelection(getSettingsConfiguration().isAssistant());
    getBtrCell().setSelection(getSettingsConfiguration().isCells());
    getBtrCellAndAssistant().setSelection(getSettingsConfiguration().isCellAndAssistant());
    getBtrMultiSelection().setSelection(getSettingsConfiguration().isMultiSelection());
    getBtrMonoSelection().setSelection(getSettingsConfiguration().isMonoSelection());
    getChkAddItem().setSelection(getSettingsConfiguration().isAddItem());
    getChkUpdateItem().setSelection(getSettingsConfiguration().isUpdateItem());
    getChkUpdateItems().setSelection(getSettingsConfiguration().isUpdateItems());
    getChkDellItem().setSelection(getSettingsConfiguration().isDellItem());
    getChkDuplicateItem().setSelection(getSettingsConfiguration().isDuplicateItem());
    getChkMovingItem().setSelection(getSettingsConfiguration().isMovingItem());
  }

  public void addListener() {
    getBtrAssistant().addSelectionListener(new SelectionListener() {

      @Override
      public void widgetSelected(SelectionEvent e) {
        getSettingsConfiguration().setAssistant(getBtrAssistant().getSelection());
      }

      @Override
      public void widgetDefaultSelected(SelectionEvent e) {/* Do nothing */}
    });

    getBtrCell().addSelectionListener(new SelectionListener() {

      @Override
      public void widgetSelected(SelectionEvent e) {
        getSettingsConfiguration().setCells(getBtrCell().getSelection());
      }

      @Override
      public void widgetDefaultSelected(SelectionEvent e) {
        /* Do nothing */
      }
    });

    getBtrCellAndAssistant().addSelectionListener(new SelectionListener() {

      @Override
      public void widgetSelected(SelectionEvent e) {
        getSettingsConfiguration().setCellAndAssistant(getBtrCellAndAssistant().getSelection());
      }

      @Override
      public void widgetDefaultSelected(SelectionEvent e) {
        /* Do nothing */
      }
    });

    getBtrMultiSelection().addSelectionListener(new SelectionListener() {

      @Override
      public void widgetSelected(SelectionEvent e) {
        getSettingsConfiguration().setMultiSelection(getBtrMultiSelection().getSelection());
      }

      @Override
      public void widgetDefaultSelected(SelectionEvent e) {/* Do nothing */}
    });

    getBtrMonoSelection().addSelectionListener(new SelectionListener() {

      @Override
      public void widgetSelected(SelectionEvent e) {
        getSettingsConfiguration().setMonoSelection(getBtrMonoSelection().getSelection());
      }

      @Override
      public void widgetDefaultSelected(SelectionEvent e) {
        /* Do nothing */
      }
    });

    getChkAddItem().addSelectionListener(new SelectionListener() {

      @Override
      public void widgetSelected(SelectionEvent e) {
        getSettingsConfiguration().setAddItem(getChkAddItem().getSelection());

      }

      @Override
      public void widgetDefaultSelected(SelectionEvent e) {
        /* Do nothing */
      }
    });

    getChkUpdateItem().addSelectionListener(new SelectionListener() {

      @Override
      public void widgetSelected(SelectionEvent e) {
        getSettingsConfiguration().setUpdateItem(getChkUpdateItem().getSelection());
      }

      @Override
      public void widgetDefaultSelected(SelectionEvent e) {
        /* Do nothing */
      }
    });

    getChkUpdateItems().addSelectionListener(new SelectionListener() {

      @Override
      public void widgetSelected(SelectionEvent e) {
        getSettingsConfiguration().setUpdateItems(getChkUpdateItems().getSelection());
      }

      @Override
      public void widgetDefaultSelected(SelectionEvent e) {
        /* Do nothing */
      }
    });

    getChkDellItem().addSelectionListener(new SelectionListener() {

      @Override
      public void widgetSelected(SelectionEvent e) {
        getSettingsConfiguration().setDellItem(getChkDellItem().getSelection());

      }

      @Override
      public void widgetDefaultSelected(SelectionEvent e) {
        /* Do nothing */
      }
    });

    getChkDuplicateItem().addSelectionListener(new SelectionListener() {

      @Override
      public void widgetSelected(SelectionEvent e) {
        getSettingsConfiguration().setDuplicateItem(getChkDuplicateItem().getSelection());
      }

      @Override
      public void widgetDefaultSelected(SelectionEvent e) {
        /* Do nothing */
      }
    });

    getChkMovingItem().addSelectionListener(new SelectionListener() {

      @Override
      public void widgetSelected(SelectionEvent e) {
        getSettingsConfiguration().setMovingItem(getChkMovingItem().getSelection());
      }

      @Override
      public void widgetDefaultSelected(SelectionEvent e) {
        /* Do nothing */
      }
    });
  }

  public void buildLineSettings() {
    StringBuilder sb = new StringBuilder();

    sb.append(settingsConfiguration.isAssistant() ? "Assistant" + SPLIT : IS_FALSE + SPLIT);
    sb.append(settingsConfiguration.isCells() ? "Cellule" + SPLIT : IS_FALSE + SPLIT);
    sb.append(settingsConfiguration.isCellAndAssistant() ? "CellAndAssistant" + SPLIT : IS_FALSE + SPLIT);
    sb.append(settingsConfiguration.isMonoSelection() ? "monoSelection" + SPLIT : IS_FALSE + SPLIT);
    sb.append(settingsConfiguration.isMultiSelection() ? "multiSelection" + SPLIT : IS_FALSE + SPLIT);
    sb.append(settingsConfiguration.isAddItem() ? "addItem" + SPLIT : IS_FALSE + SPLIT);
    sb.append(settingsConfiguration.isUpdateItem() ? "updateItem" + SPLIT : IS_FALSE + SPLIT);
    sb.append(settingsConfiguration.isUpdateItems() ? "updateItems" + SPLIT : IS_FALSE + SPLIT);
    sb.append(settingsConfiguration.isDellItem() ? "dellItem" + SPLIT : IS_FALSE + SPLIT);
    sb.append(settingsConfiguration.isDuplicateItem() ? "duplicateItem" + SPLIT : IS_FALSE + SPLIT);
    sb.append(settingsConfiguration.isMovingItem() ? "movingItem" + SPLIT : IS_FALSE + SPLIT);

    genericWizardModelObject.setValue(sb.toString());
  }

  /**
   * @return the lbTableUpdate
   */
  public Label getLbTableUpdate() {
    return lbTableUpdate;
  }

  /**
   * @param lbTableUpdate the lbTableUpdate to set
   */
  public void setLbTableUpdate(Label lbTableUpdate) {
    this.lbTableUpdate = lbTableUpdate;
  }

  /**
   * @return the btrAssistant
   */
  public Button getBtrAssistant() {
    return btrAssistant;
  }

  /**
   * @param btrAssistant the btrAssistant to set
   */
  public void setBtrAssistant(Button btrAssistant) {
    this.btrAssistant = btrAssistant;
  }

  /**
   * @return the btrCell
   */
  public Button getBtrCell() {
    return btrCell;
  }

  /**
   * @param btrCell the btrCell to set
   */
  public void setBtrCell(Button btrCell) {
    this.btrCell = btrCell;
  }

  /**
   * @return the chkAddItem
   */
  public Button getChkAddItem() {
    return chkAddItem;
  }

  /**
   * @param chkAddItem the chkAddItem to set
   */
  public void setChkAddItem(Button chkAddItem) {
    this.chkAddItem = chkAddItem;
  }

  /**
   * @return the chkUpdateItem
   */
  public Button getChkUpdateItem() {
    return chkUpdateItem;
  }

  /**
   * @param chkUpdateItem the chkUpdateItem to set
   */
  public void setChkUpdateItem(Button chkUpdateItem) {
    this.chkUpdateItem = chkUpdateItem;
  }

  /**
   * @return the chkUpdateItems
   */
  public Button getChkUpdateItems() {
    return chkUpdateItems;
  }

  /**
   * @param chkUpdateItems the chkUpdateItems to set
   */
  public void setChkUpdateItems(Button chkUpdateItems) {
    this.chkUpdateItems = chkUpdateItems;
  }

  /**
   * @return the chkDellItem
   */
  public Button getChkDellItem() {
    return chkDellItem;
  }

  /**
   * @param chkDellItem the chkDellItem to set
   */
  public void setChkDellItem(Button chkDellItem) {
    this.chkDellItem = chkDellItem;
  }

  /**
   * @return the chkDuplicateItem
   */
  public Button getChkDuplicateItem() {
    return chkDuplicateItem;
  }

  /**
   * @param chkDuplicateItem the chkDuplicateItem to set
   */
  public void setChkDuplicateItem(Button chkDuplicateItem) {
    this.chkDuplicateItem = chkDuplicateItem;
  }

  /**
   * @return the chkMovingItem
   */
  public Button getChkMovingItem() {
    return chkMovingItem;
  }

  /**
   * @param chkMovingItem the chkMovingItem to set
   */
  public void setChkMovingItem(Button chkMovingItem) {
    this.chkMovingItem = chkMovingItem;
  }

  /**
   * @return the groupChkTableUpdate
   */
  public Group getGroupChkTableUpdate() {
    return groupChkTableUpdate;
  }

  /**
   * @param groupChkTableUpdate the groupChkTableUpdate to set
   */
  public void setGroupChkTableUpdate(Group groupChkTableUpdate) {
    this.groupChkTableUpdate = groupChkTableUpdate;
  }

  /**
   * @return the settingsConfiguration
   */
  public SettingsConfiguration getSettingsConfiguration() {
    return settingsConfiguration;
  }

  /**
   * @param settingsConfiguration the settingsConfiguration to set
   */
  public void setSettingsConfiguration(SettingsConfiguration settingsConfiguration) {
    this.settingsConfiguration = settingsConfiguration;
  }

  /**
   * @return the btrMonoSelection
   */
  public Button getBtrMonoSelection() {
    return btrMonoSelection;
  }

  /**
   * @param btrMonoSelection the btrMonoSelection to set
   */
  public void setBtrMonoSelection(Button btrMonoSelection) {
    this.btrMonoSelection = btrMonoSelection;
  }

  /**
   * @return the btrMultiSelection
   */
  public Button getBtrMultiSelection() {
    return btrMultiSelection;
  }

  /**
   * @param btrMultiSelection the btrMultiSelection to set
   */
  public void setBtrMultiSelection(Button btrMultiSelection) {
    this.btrMultiSelection = btrMultiSelection;
  }

  /**
   * @return the btrCellAndAssistant
   */
  public Button getBtrCellAndAssistant() {
    return btrCellAndAssistant;
  }

  /**
   * @param btrCellAndAssistant the btrCellAndAssistant to set
   */
  public void setBtrCellAndAssistant(Button btrCellAndAssistant) {
    this.btrCellAndAssistant = btrCellAndAssistant;
  }

}

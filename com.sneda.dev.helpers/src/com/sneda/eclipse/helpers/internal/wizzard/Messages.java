package com.sneda.eclipse.helpers.internal.wizzard;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

  private static final String BUNDLE_NAME = "com.sneda.eclipse.helpers.internal.wizzard.messages"; //$NON-NLS-1$
  public static String GenericWizardPage_Contexte;
  public static String GenericWizardPage_Entrees;
  public static String GenericWizardPage_Saisies;
  static {
    // initialize resource bundle
    NLS.initializeMessages(BUNDLE_NAME, Messages.class);
  }

  private Messages() {}
}

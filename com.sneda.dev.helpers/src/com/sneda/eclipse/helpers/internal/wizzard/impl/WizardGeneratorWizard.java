package com.sneda.eclipse.helpers.internal.wizzard.impl;

import java.util.List;
import java.util.Map;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;
import com.sneda.eclipse.helpers.internal.cmd.jet.WizardGenerator;
import com.sneda.eclipse.helpers.internal.wizzard.GenericGeneratorWizard;

/**
 * @author nio
 */
public class WizardGeneratorWizard extends GenericGeneratorWizard {

  public WizardGeneratorWizard() {
    super("WizardGenerator"); //$NON-NLS-1$
  }

  @Override
  protected String getPageDescription() {
    return Messages.WizardGeneratorWizard_2;
  }

  @Override
  protected String getPageTitle() {
    return Messages.WizardGeneratorWizard_3;
  }

  @Override
  protected String getSettingsId() {
    return "WIZARD_GENERATOR_SETTINGS"; //$NON-NLS-1$
  }

  @Override
  protected Map<String, ValueObject> buildInput() {
    return new WizardGenerator().buildInput();
  }

  @SuppressWarnings("unchecked")
  @Override
  protected boolean executeCmd(Object param) {
    new WizardGenerator().execute((List<ValueObject>) param);
    return true;
  }
}

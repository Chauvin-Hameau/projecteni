package com.sneda.eclipse.helpers.internal.wizzard.impl;

import java.util.List;
import java.util.Map;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;
import com.sneda.eclipse.helpers.internal.cmd.jet.ViewGenerator;
import com.sneda.eclipse.helpers.internal.wizzard.GenericGeneratorWizard;

/**
 * @author nio
 */
public class ViewGeneratorWizard extends GenericGeneratorWizard {

  public ViewGeneratorWizard() {
    super("ViewGenerator"); //$NON-NLS-1$
  }

  @Override
  protected String getPageDescription() {
    return Messages.ViewGeneratorWizard_0;
  }

  @Override
  protected String getPageTitle() {
    return Messages.ViewGeneratorWizard_1;
  }

  @Override
  protected String getSettingsId() {
    return "VIEW_GENERATOR_SETTINGS"; //$NON-NLS-1$
  }

  @Override
  protected Map<String, ValueObject> buildInput() {
    return new ViewGenerator().buildInput();
  }

  @SuppressWarnings("unchecked")
  @Override
  protected boolean executeCmd(Object param) {
    new ViewGenerator().execute((List<ValueObject>) param);
    return true;
  }
}

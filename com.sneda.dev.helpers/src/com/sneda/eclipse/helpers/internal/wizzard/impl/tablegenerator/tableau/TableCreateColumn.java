package com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator.tableau;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;

/**
 * @author dimitric
 *
 */
public class TableCreateColumn extends TableViewer {

  private TableViewerColumn column;
  private ColumnCreationWizardPage columnCreationWizardPage;

  /**
   * @param parent
   * @param style
   * @param columnCreationWizardPage
   * @param columnCreationWizardPage
   */
  public TableCreateColumn(Composite parent, int style, ColumnCreationWizardPage columnCreationWizardPage) {
    super(parent, style);
    this.columnCreationWizardPage = columnCreationWizardPage;
    Table table = getTable();
    GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
    table.setLayoutData(gridData);
    createColumns();
    table.setHeaderVisible(true);
    table.setLinesVisible(true);
    setContentProvider(new TableCreateColumnContentProvider());
  }

  /**
   * This methode creates the columns
   */
  private void createColumns() {
    int index = 0;
    for (TableCreateColumnColumn tableCreateColumnColumn : TableCreateColumnColumn.values()) {
      column = createTableViewerColumn(tableCreateColumnColumn.getTitle(), tableCreateColumnColumn.getWidth(), index);
      column.setEditingSupport(new TableCreateColumnEditingSupport(this, tableCreateColumnColumn));
      if (column.getColumn().getText().equals(TableCreateColumnColumn.EDITABLE.getTitle())) {
        column.setLabelProvider(new CheckBoxCellProvider(column.getViewer()) {

          @Override
          protected boolean isChecked(Object element) {
            return ((Column) element).isEditable();
          }
        });
      } else {
        column.setLabelProvider(new TableCreateColumnLabelProvider(tableCreateColumnColumn));
      }
      index++;
    }
  }

  private TableViewerColumn createTableViewerColumn(String header, int width, int idx) {
    TableViewerColumn newColumn = new TableViewerColumn(this, SWT.LEFT, idx);
    newColumn.getColumn().setText(header);
    newColumn.getColumn().setWidth(width);
    newColumn.getColumn().setResizable(true);
    newColumn.getColumn().setMoveable(true);

    return newColumn;
  }

  /**
   * @return the columnCreationWizardPage
   */
  public ColumnCreationWizardPage getColumnCreationWizardPage() {
    return columnCreationWizardPage;
  }

}

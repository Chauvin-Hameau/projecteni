package com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator.tableau;

import org.eclipse.swt.SWT;

/**
 * @author dimitric
 *
 */
public enum TableCreateColumnColumn {

  NAME("Nom", 100, SWT.LEFT), SIZE("Taille", 100, SWT.LEFT), TYPE("Type", 100, SWT.RIGHT), RESIZABLE(
      "Redimensionnable", 150, SWT.RIGHT), MASQUABLE("Caché", 100, SWT.RIGHT), EDITABLE("Editable", 100, SWT.RIGHT);

  private String title;
  private int style;
  private int width;

  TableCreateColumnColumn(String title, int width, int style) {
    this.title = title;
    this.style = style;
    this.width = width;
  }

  public String getTitle() {
    return title;
  }

  public int getStyle() {
    return style;
  }

  public int getWidth() {
    return width;
  }

}

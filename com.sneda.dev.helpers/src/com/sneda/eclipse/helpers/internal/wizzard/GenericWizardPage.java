package com.sneda.eclipse.helpers.internal.wizzard;

import java.util.List;

import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.databinding.observable.map.IObservableMap;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.databinding.viewers.ObservableMapLabelProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

import com.sneda.eclipse.helpers.internal.cmd.jet.AbstractJetGenerator;
import com.sneda.eclipse.helpers.internal.util.EclipsePluginTools;
import com.sneda.eclipse.helpers.internal.wizzard.impl.editorihmetendue.EditeurIhmEtendueWizard;
import com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator.SettingsTableWizard;
import com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator.tableau.ColumnCreationWizard;
import com.sneda.eclipse.helpers.tools.ReadModifyProperties;

/**
 * @author nio
 */
public class GenericWizardPage extends WizardPage {

	protected static final String UUID = "e81d6ff3-205b-4cb4-aaff-ddef9ed47016"; //$NON-NLS-1$

	private static double[] COLUMN_WEIGHTS = { 0.0, 1.0 };
	private static int[] COLUMN_MINS = { 220, 300 };
	private static final String CREATE_COLUMN = "/input/elmt/table/createColumn";
	private static final String SETTINGS_TABLE = "/input/elmt/table/settingsUpdate";
	private static final String IHM_ETENDUE = "/input/elmt/editor/ihmEtendue";

	private Table table;
	private TableColumn tblclmnKey;
	private TableColumn tblclmnValeur;
	private TableViewer tableViewer;
	private Text text;
	private Link link;
	private String idSettings;

	private Button btModifyPropertie;

	private ReadModifyProperties readModifyProperties;

	private ColumnCreationWizard columnCreationWizard;
	private EditeurIhmEtendueWizard editeurIhmEtendueWizard;

	/**
	 * Create the wizard.
	 *
	 * @param idSettings
	 */
	public GenericWizardPage(String title, String description, String idSettings) {
		super(UUID);
		setTitle(title);
		setDescription(description);
		this.idSettings = idSettings;
		this.readModifyProperties = new ReadModifyProperties(
				"D:\\projecteni\\com.sneda.dev.helpers\\src\\com\\sneda\\eclipse\\helpers\\internal\\wizzard\\xwiki.properties",
				idSettings);
	}

	@Override
	public GenericWizard getWizard() {
		return (GenericWizard) super.getWizard();
	}

	public GenericWizardModel getModel() {
		return getWizard().getModel();
	}

	/**
	 * Create contents of the wizard.
	 *
	 * @param parent
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		setControl(container);
		container.setLayout(new GridLayout(1, false));
		{
			tableViewer = new TableViewer(container, SWT.BORDER | SWT.FULL_SELECTION);
			table = tableViewer.getTable();
			table.setLinesVisible(true);
			table.setHeaderVisible(true);
			table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
			{
				TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
				tblclmnKey = tableViewerColumn.getColumn();
				tblclmnKey.setWidth(COLUMN_MINS[0]);
				tblclmnKey.setText(Messages.GenericWizardPage_Entrees);
			}
			{
				TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
				tableViewerColumn.setEditingSupport(new EditingSupport(tableViewer) {

					@Override
					protected boolean canEdit(Object element) {
						return true;
					}

					@Override
					protected CellEditor getCellEditor(Object element) {
						return new TextCellEditor((tableViewer).getTable());
					}

					@Override
					protected Object getValue(Object element) {
						Object out = ((GenericWizardModelObject) element).getValue();
						return (out != null) ? out : ""; //$NON-NLS-1$
					}

					@Override
					protected void setValue(Object element, Object value) {
						// CleanUp
						String valStr = String.valueOf(value).trim();

						// Advanced CleanUp for packages
						if (AbstractJetGenerator.MSGE_PACKAGE.equals(((GenericWizardModelObject) element).getMsge())) {
							EclipsePluginTools.cleanUpPackageName(valStr);
						}

						((GenericWizardModelObject) element).setValue(valStr);
						getViewer().update(element, null);

						// Update Buttons
						GenericWizardPage.this.getWizard().getContainer().updateButtons();
					}
				});
				tblclmnValeur = tableViewerColumn.getColumn();
				tblclmnValeur.setWidth(COLUMN_MINS[1]);
				tblclmnValeur.setText(Messages.GenericWizardPage_Saisies);
			}
		}
		{
			Label label = new Label(container, SWT.NONE);
			label.setText(Messages.GenericWizardPage_Contexte);
		}
		{
			text = new Text(container, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.READ_ONLY | SWT.V_SCROLL);
			text.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 1, 1));
		}

		Composite composite = new Composite(container, SWT.NONE);
		composite.setLayout(new GridLayout(3, false));

		Label labelWiki = new Label(composite, SWT.NONE);
		labelWiki.setText("Lien XWiki : ");

		link = new Link(composite, SWT.NONE);
		if ("Aucun XWiki".equals(readModifyProperties.readPropertie())) {
			link.setText(readModifyProperties.readPropertie());
		} else {
			link.setText("<A>" + readModifyProperties.readPropertie() + "</A>");
		}

		link.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (!"Aucun XWiki".equals(readModifyProperties.readPropertie())) {
					Program.launch(readModifyProperties.readPropertie());
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		IObservableList observableList = new WritableList(getModel().getElements(), GenericWizardModelObject.class);
		getModel().setElements(observableList);

		MyTableViewerContentProvider contentProvider = new MyTableViewerContentProvider();
		tableViewer.setContentProvider(contentProvider);

		// this does not have to correspond to the columns in the table,
		// we just list all attributes that affect the table content.
		IObservableMap[] attributes = BeansObservables.observeMaps(contentProvider.getKnownElements(),
				GenericWizardModelObject.class, new String[] { "msge", "value" }); //$NON-NLS-1$ //$NON-NLS-2$

		tableViewer.setLabelProvider(new MyTableLabelProvider(attributes));

		tableViewer.setInput(observableList);

		addListeners();
	}

	protected void addListeners() {
		tableViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				StructuredSelection selection = (StructuredSelection) event.getSelection();
				if (selection != null) {
					if (selection.getFirstElement() instanceof GenericWizardModelObject) {
						final GenericWizardModelObject elmt = (GenericWizardModelObject) selection.getFirstElement();
						text.setText(elmt.getHelp());
						if (elmt.getPath().equals(CREATE_COLUMN)) {
							Display.getDefault().asyncExec(new Runnable() {

								@Override
								public void run() {
									Shell shell = new Shell();
									if (getColumnCreationWizard() == null) {
										setColumnCreationWizard(
												new ColumnCreationWizard(elmt, tableViewer, getWizard()));
									}
									WizardDialog dialog = new WizardDialog(shell, getColumnCreationWizard());
									dialog.open();
								}
							});

						} else if (elmt.getPath().equals(SETTINGS_TABLE)) {
							Display.getDefault().asyncExec(new Runnable() {

								@Override
								public void run() {
									Shell shell = new Shell();
									WizardDialog dialog = new WizardDialog(shell,
											new SettingsTableWizard(elmt, tableViewer));
									dialog.open();
								}
							});
						} else if (elmt.getPath().equals(IHM_ETENDUE)) {
							Display.getDefault().asyncExec(new Runnable() {

								@Override
								public void run() {
									Shell shell = new Shell();
									if (getEditeurIhmEtendueWizard() == null) {
										setEditeurIhmEtendueWizard(new EditeurIhmEtendueWizard(elmt, tableViewer));
									}
									WizardDialog dialog = new WizardDialog(shell, getEditeurIhmEtendueWizard());
									dialog.setPageSize(700, 250);
									dialog.open();
								}
							});
						}
					}
				}
			}
		});

		tableViewer.getTable().addControlListener(new ControlAdapter() {

			@Override
			public void controlResized(ControlEvent e) {
				Rectangle area = table.getClientArea();
				TableColumn[] columns = table.getColumns();
				int ws = EclipsePluginTools.sum(COLUMN_MINS);
				int extraSpace = area.width - ws;
				double sumWeights = EclipsePluginTools.sum(COLUMN_WEIGHTS);
				if (extraSpace > 0) {
					for (int i = 0; i < columns.length; i++) {
						int w = COLUMN_MINS[i] + (int) (COLUMN_WEIGHTS[i] * extraSpace / sumWeights);
						columns[i].setWidth(w);
					}
				}
			}
		});
	}

	@Override
	public boolean isPageComplete() {
		List<GenericWizardModelObject> elmts = getModel().getElements();
		for (GenericWizardModelObject each : elmts) {
			if (each.getValue() == null || each.getValue().toString().isEmpty()) {
				return false;
			}
		}
		return true;
	}

	static class MyTableViewerContentProvider extends ObservableListContentProvider {
	}

	static class MyTableLabelProvider extends ObservableMapLabelProvider implements ITableLabelProvider {

		public MyTableLabelProvider(IObservableMap[] attributeMap) {
			super(attributeMap);
		}
	}

	/**
	 * @return the columnCreationWizard
	 */
	public ColumnCreationWizard getColumnCreationWizard() {
		return columnCreationWizard;
	}

	public TableViewer getTableViewer() {
		return tableViewer;
	}

	/**
	 * @param columnCreationWizard
	 *            the columnCreationWizard to set
	 */
	public void setColumnCreationWizard(ColumnCreationWizard columnCreationWizard) {
		this.columnCreationWizard = columnCreationWizard;
	}

	public EditeurIhmEtendueWizard getEditeurIhmEtendueWizard() {
		return editeurIhmEtendueWizard;
	}

	public void setEditeurIhmEtendueWizard(EditeurIhmEtendueWizard editeurIhmEtendueWizard) {
		this.editeurIhmEtendueWizard = editeurIhmEtendueWizard;
	}

}

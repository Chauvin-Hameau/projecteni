package com.sneda.eclipse.helpers.internal.wizzard;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IResource;
import org.eclipse.jface.dialogs.IDialogSettings;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;
import com.sneda.eclipse.helpers.internal.cmd.jet.AbstractJetGenerator;
import com.sneda.eclipse.helpers.internal.util.EclipsePluginTools;

/**
 * @author nio
 */
public abstract class GenericGeneratorWizard extends GenericWizard {

  private GenericWizardModel model;

  public GenericGeneratorWizard(String windowTitle) {
    super(windowTitle);

    // Get values for Model
    Map<String, GenericWizardModelObject> mapForModel = new LinkedHashMap<String, GenericWizardModelObject>();
    for (Map.Entry<String, ValueObject> each : buildInput().entrySet()) {
      // Do not initialize state as we guess values from selection
      // String defValue = getDialogSettings().get(each.getKey());

      GenericWizardModelObject elmt = new GenericWizardModelObject(each.getValue());
      mapForModel.put(each.getValue().get(ValueObject.PROP_MSGE).toString(), elmt);
    }

    // Guess some of the entries from current selection
    if (EclipsePluginTools.getCurrentSelection() != null) {
      IResource resource = EclipsePluginTools.getCurrentSelection();
      String srcDir = EclipsePluginTools.guessSrcDir(resource);
      String packageName = EclipsePluginTools.guessPackageName(resource);
      String bundleSymbolicName = EclipsePluginTools.guessBundleSymbolicName(resource);
      if (srcDir != null) {
        mapForModel.get(AbstractJetGenerator.MSGE_SRCFOLDER).setValue(srcDir);
      }
      if (packageName != null) {
        mapForModel.get(AbstractJetGenerator.MSGE_PACKAGE).setValue(packageName + ".mypackage");
      }
      if (bundleSymbolicName != null) {
        mapForModel.get(AbstractJetGenerator.MSGE_BUNDLENAME).setValue(bundleSymbolicName);
      }
    }
    mapForModel.get(AbstractJetGenerator.MSGE_AUTHOR).setValue(System.getProperty("user.name"));

    // Fill model
    model = new GenericWizardModel(mapForModel.values());
  }

  protected abstract Map<String, ValueObject> buildInput();

  @Override
  protected GenericWizardModel getModel() {
    return model;
  }

  @Override
  protected List<ValueObject> getCmdParam() {
    return getModel().getValueObjects();
  }

  @Override
  public IDialogSettings getDialogSettings() {
    IDialogSettings settings = super.getDialogSettings();
    if (settings.get(AbstractJetGenerator.MSGE_AUTHOR) == null) {
      Object user = System.getProperties().get("user.name");
      settings.put(AbstractJetGenerator.MSGE_AUTHOR, (String) user);
    }
    return settings;
  }

  protected List<ValueObject> asValueObjects(List<GenericWizardModelObject> objects) {
    List<ValueObject> visitor = new ArrayList<ValueObject>();
    for (GenericWizardModelObject each : objects) {
      each.accept(visitor);
    }
    return visitor;
  }

  @Override
  protected void saveState() {}
}

package com.sneda.eclipse.helpers.internal.wizzard.impl.editorihmetendue;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.sneda.eclipse.helpers.internal.util.EclipsePluginTools;
import com.sneda.eclipse.helpers.internal.wizzard.GenericWizardModelObject;


public class EditeurIhmEtendueWizardPage extends WizardPage {

	static final String SPLIT = ";";
	static final String IS_NULL = "null";
	static final String IS_FALSE = "false";
	static final String IS_EMPTY = "";

	GenericWizardModelObject genericWizardModelObject;

	private Group groupChkTableUpdate;

	private Button rbOuiIhmEtendu;
	private Button rbNonIhmEtendu;

	private Label lbIhmEtendue;
	private Label lbNameEntite;
	private Label lbPathFileDAO;
	private Label lbPathFileNMCMapper;
	private Label lbPathFileInputCreatorCharger;
	private Label lbPathFileInputCreatorCreerModifier;
	private Label lbLibelleParam;
	private Label lbVariableParam;
	private Label lbPathFileProperties;

	private Text txtNameEntite;
	private Text txtPathFileDAO;
	private Text txtPathFileNMCMapper;
	private Text txtPathFileInputCreatorCharger;
	private Text txtPathFileInputCreatorCreerModifier;
	private Text txtLibelleParam;
	private Text txtVariableParam;
	private Text txtPathFileProperties;

	private EditeurIhmEtendueParams editeurIhmEtendueParams;
	private EditeurIhmEtendueWizard editeurIhmEtendueWizard;

	protected EditeurIhmEtendueWizardPage(String pageName,  GenericWizardModelObject genericWizardModelObject, EditeurIhmEtendueWizard editeurIhmEtendueWizard) {
		super(pageName);
		setTitle("Paramètre IHM étendue");
		setDescription("Définition des différents paramètres d'une IHM étendue");
		this.genericWizardModelObject = genericWizardModelObject;
		this.editeurIhmEtendueWizard = editeurIhmEtendueWizard;
		this.editeurIhmEtendueParams = new EditeurIhmEtendueParams();
	}

	@Override
	public void createControl(Composite parent) {
		Composite cpoMain = new Composite(parent, SWT.NONE);
		cpoMain.setLayout(new GridLayout(1, false));
		GridDataFactory.fillDefaults().grab(true, true).applyTo(cpoMain);

		createCompositeIfIhmEtendue(cpoMain);
		createCompositeParamIhmEtendue(cpoMain);

		init();
		addListeners();
		setControl(cpoMain);
	}

	private void init() {
		Wizard page = (Wizard) this.getWizard();
		page.setWindowTitle("Paramètrage IHM étendue");
		if(editeurIhmEtendueWizard.getEditeurIhmEtendueParams() != null) {
			editeurIhmEtendueParams = editeurIhmEtendueWizard.getEditeurIhmEtendueParams();
			rbOuiIhmEtendu.setSelection(editeurIhmEtendueParams.isIhmEtendue());
			rbNonIhmEtendu.setSelection(!editeurIhmEtendueParams.isIhmEtendue());
			txtNameEntite.setText(isBlank(editeurIhmEtendueParams.getNomEntite()) ? IS_EMPTY : editeurIhmEtendueParams.getNomEntite());
			txtPathFileDAO.setText(isBlank(editeurIhmEtendueParams.getCheminDAOSrcFolder()) ? IS_EMPTY : editeurIhmEtendueParams.getCheminDAOSrcFolder());
			txtPathFileNMCMapper.setText(isBlank(editeurIhmEtendueParams.getCheminNMCMapperSrcFolder()) ? IS_EMPTY : editeurIhmEtendueParams.getCheminNMCMapperSrcFolder());
			txtPathFileInputCreatorCharger.setText(isBlank(editeurIhmEtendueParams.getCheminInputCreatorChargerSrcFolder()) ? IS_EMPTY : editeurIhmEtendueParams.getCheminInputCreatorChargerSrcFolder());
			txtPathFileInputCreatorCreerModifier.setText(isBlank(editeurIhmEtendueParams.getCheminInputCreatorCreeModifierSrcFolder()) ? IS_EMPTY : editeurIhmEtendueParams.getCheminInputCreatorCreeModifierSrcFolder());
			txtPathFileProperties.setText(isBlank(editeurIhmEtendueParams.getCheminProperties()) ?  IS_EMPTY : editeurIhmEtendueParams.getCheminProperties());
			txtLibelleParam.setText(isBlank(editeurIhmEtendueParams.getLibelleParametre()) ? IS_EMPTY : editeurIhmEtendueParams.getLibelleParametre());
			txtVariableParam.setText(isBlank(editeurIhmEtendueParams.getVariableParametre()) ? IS_EMPTY : editeurIhmEtendueParams.getVariableParametre());
		}
	}

	private void addListeners() {
		rbOuiIhmEtendu.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				editeurIhmEtendueParams.setIhmEtendue(true);

		}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				//nothing

			}
		});
		rbNonIhmEtendu.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				editeurIhmEtendueParams.setIhmEtendue(false);
				refresh();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// nothing

			}
		});
	}


	private void createCompositeIfIhmEtendue(Composite cpoParent) {
		Composite cpoIfIhmEtendue = new Composite(cpoParent, SWT.NONE);
	    cpoIfIhmEtendue.setLayout(new GridLayout(3, false));
	    GridDataFactory.fillDefaults().grab(true, false).applyTo(cpoIfIhmEtendue);

	    lbIhmEtendue = new Label(cpoIfIhmEtendue, SWT.NONE);
	    lbIhmEtendue.setText("IHM étendue : ");

	    rbOuiIhmEtendu = new Button(cpoIfIhmEtendue, SWT.RADIO);
	    rbOuiIhmEtendu.setText("Oui");
	    rbNonIhmEtendu = new Button(cpoIfIhmEtendue, SWT.RADIO);
	    rbNonIhmEtendu.setText("Non");
	}

	private void createCompositeParamIhmEtendue(Composite cpoParent) {
		Composite cpoParamIhmEtendue = new Composite(cpoParent, SWT.NONE);
	    cpoParamIhmEtendue.setLayout(new GridLayout(2, false));
	    GridDataFactory.fillDefaults().grab(true, true).applyTo(cpoParamIhmEtendue);

	    groupChkTableUpdate = new Group(cpoParamIhmEtendue, SWT.NONE);
	    groupChkTableUpdate.setText("Paramètres IHM étendue");
	    GridDataFactory.fillDefaults().grab(true, true).applyTo(groupChkTableUpdate);

	    lbNameEntite = new Label(groupChkTableUpdate, SWT.NONE);
	    lbNameEntite.setText("Nom de l'entité");
	    lbNameEntite.setLocation(20,20);
	    lbNameEntite.pack();
	    txtNameEntite = new Text(groupChkTableUpdate, SWT.NONE);
	    txtNameEntite.setLocation(275,20);
	    txtNameEntite.pack();
	    txtNameEntite.setSize(300,15);

	    lbPathFileDAO = new Label(groupChkTableUpdate, SWT.NONE);
	    lbPathFileDAO.setText("Chemin du fichier DAO");
	    lbPathFileDAO.setLocation(20, 40);
	    lbPathFileDAO.pack();
	    txtPathFileDAO = new Text(groupChkTableUpdate, SWT.NONE);
	    txtPathFileDAO.setLocation(275,40);
	    txtPathFileDAO.pack();
	    txtPathFileDAO.setSize(300,15);

	    lbPathFileNMCMapper = new Label(groupChkTableUpdate, SWT.NONE);
	    lbPathFileNMCMapper.setText("Chemin du fichier NMCMapper");
	    lbPathFileNMCMapper.setLocation(20, 60);
	    lbPathFileNMCMapper.pack();
	    txtPathFileNMCMapper = new Text(groupChkTableUpdate, SWT.NONE);
	    txtPathFileNMCMapper.setLocation(275,60);
	    txtPathFileNMCMapper.pack();
	    txtPathFileNMCMapper.setSize(300,15);

	    lbPathFileInputCreatorCharger = new Label(groupChkTableUpdate, SWT.NONE);
	    lbPathFileInputCreatorCharger.setText("Chemin du fichier InputCreatorCharger");
	    lbPathFileInputCreatorCharger.setLocation(20,80);
	    lbPathFileInputCreatorCharger.pack();
	    txtPathFileInputCreatorCharger = new Text(groupChkTableUpdate, SWT.NONE);
	    txtPathFileInputCreatorCharger.setLocation(275,80);
	    txtPathFileInputCreatorCharger.pack();
	    txtPathFileInputCreatorCharger.setSize(300,15);

	    lbPathFileInputCreatorCreerModifier = new Label(groupChkTableUpdate, SWT.NONE);
	    lbPathFileInputCreatorCreerModifier.setText("Chemin du fichier InputCreatorCreerModifier");
	    lbPathFileInputCreatorCreerModifier.setLocation(20, 100);
	    lbPathFileInputCreatorCreerModifier.pack();
	    txtPathFileInputCreatorCreerModifier = new Text(groupChkTableUpdate, SWT.NONE);
	    txtPathFileInputCreatorCreerModifier.setLocation(275,100);
	    txtPathFileInputCreatorCreerModifier.pack();
	    txtPathFileInputCreatorCreerModifier.setSize(300,15);

	    lbPathFileProperties = new Label(groupChkTableUpdate, SWT.NONE);
	    lbPathFileProperties.setText("Chemin du fichier Properties");
	    lbPathFileProperties.setLocation(20, 120);
	    lbPathFileProperties.pack();
	    txtPathFileProperties = new Text(groupChkTableUpdate, SWT.NONE);
	    txtPathFileProperties.setLocation(275,120);
	    txtPathFileProperties.pack();
	    txtPathFileProperties.setSize(300,15);

	    lbLibelleParam = new Label(groupChkTableUpdate, SWT.NONE);
	    lbLibelleParam.setText("Libellé du paramètre");
	    lbLibelleParam.setLocation(20,140);
	    lbLibelleParam.pack();
	    txtLibelleParam = new Text(groupChkTableUpdate, SWT.NONE);
	    txtLibelleParam.setLocation(275,140);
	    txtLibelleParam.pack();
	    txtLibelleParam.setSize(300,15);


	    lbVariableParam= new Label(groupChkTableUpdate, SWT.NONE);
	    lbVariableParam.setText("Variable du paramètre");
	    lbVariableParam.setLocation(20,160);
	    lbVariableParam.pack();
	    txtVariableParam = new Text(groupChkTableUpdate, SWT.NONE);
	    txtVariableParam.setLocation(275,160);
	    txtVariableParam.pack();
	    txtVariableParam.setSize(300,15);
	}

	public void buildLineParametres() {
	    StringBuilder sb = new StringBuilder();

	    sb.append(editeurIhmEtendueParams.isIhmEtendue() ? "ihmEtendue" + SPLIT : IS_FALSE + SPLIT);
	    sb.append(isBlank(txtNameEntite.getText()) ? IS_NULL + SPLIT : txtNameEntite.getText() + SPLIT);
	    if(!isBlank(txtPathFileDAO.getText())) {
	    	String[] chaineCut = EclipsePluginTools.chaines(txtPathFileDAO.getText());
	    	sb.append(chaineCut[0] + SPLIT);
	    	sb.append(chaineCut[1] + SPLIT);
	    }
	    if(!isBlank(txtPathFileNMCMapper.getText())) {
	    	String[] chaineCut = EclipsePluginTools.chaines(txtPathFileNMCMapper.getText());
	    	sb.append(chaineCut[0] + SPLIT);
	    	sb.append(chaineCut[1] + SPLIT);
	    }
	    if(!isBlank(txtPathFileInputCreatorCharger.getText())) {
	    	String[] chaineCut = EclipsePluginTools.chaines(txtPathFileInputCreatorCharger.getText());
	    	sb.append(chaineCut[0] + SPLIT);
	    	sb.append(chaineCut[1] + SPLIT);
	    }
	    if(!isBlank(txtPathFileInputCreatorCreerModifier.getText())) {
	    	String[] chaineCut = EclipsePluginTools.chaines(txtPathFileInputCreatorCreerModifier.getText());
	    	sb.append(chaineCut[0] + SPLIT);
	    	sb.append(chaineCut[1] + SPLIT);
	    }
	    sb.append(isBlank(txtPathFileProperties.getText()) ? IS_NULL + SPLIT : txtPathFileProperties.getText() + SPLIT);
	    sb.append(isBlank(txtLibelleParam.getText()) ? IS_NULL + SPLIT : txtLibelleParam.getText() + SPLIT);
	    sb.append(isBlank(txtVariableParam.getText()) ? IS_NULL + SPLIT : txtVariableParam.getText() + SPLIT);


	    implementationModele();
	    genericWizardModelObject.setValue(sb.toString());
	  }

	/**
	 * Permet de mettre à jour les champs du modèle avec les données saisies par l'utilisateur
	 */
	private void implementationModele() {
		editeurIhmEtendueParams.setNomEntite(txtNameEntite.getText());
		editeurIhmEtendueParams.setCheminDAOSrcFolder(txtPathFileDAO.getText());
		editeurIhmEtendueParams.setCheminNMCMapperSrcFolder(txtPathFileNMCMapper.getText());
		editeurIhmEtendueParams.setCheminInputCreatorChargerSrcFolder(txtPathFileInputCreatorCharger.getText());
		editeurIhmEtendueParams.setCheminInputCreatorCreeModifierSrcFolder(txtPathFileInputCreatorCreerModifier.getText());
		editeurIhmEtendueParams.setCheminProperties(txtPathFileProperties.getText());
		editeurIhmEtendueParams.setLibelleParametre(txtLibelleParam.getText());
		editeurIhmEtendueParams.setVariableParametre(txtVariableParam.getText());
		editeurIhmEtendueWizard.setEditeurIhmEtendueParams(editeurIhmEtendueParams);
	}


	public static boolean isBlank(final CharSequence cs) {
        int strLen;
        if (cs == null || (strLen = cs.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (Character.isWhitespace(cs.charAt(i)) == false) {
                return false;
            }
        }
        return true;
    }

	private void refresh() {
		txtLibelleParam.setText(IS_EMPTY);
		txtPathFileInputCreatorCharger.setText(IS_EMPTY);
		txtPathFileNMCMapper.setText(IS_EMPTY);
		txtNameEntite.setText(IS_EMPTY);
		txtPathFileDAO.setText(IS_EMPTY);
		txtPathFileInputCreatorCreerModifier.setText(IS_EMPTY);
		txtPathFileProperties.setText(IS_EMPTY);
		txtVariableParam.setText(IS_EMPTY);
	}

}

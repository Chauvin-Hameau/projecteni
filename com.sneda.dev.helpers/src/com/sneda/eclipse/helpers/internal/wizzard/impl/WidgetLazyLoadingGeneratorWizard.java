package com.sneda.eclipse.helpers.internal.wizzard.impl;

import java.util.List;
import java.util.Map;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;
import com.sneda.eclipse.helpers.internal.cmd.jet.WidgetLazyLoadingGenerator;
import com.sneda.eclipse.helpers.internal.wizzard.GenericGeneratorWizard;

/**
 * @author daf
 */
public class WidgetLazyLoadingGeneratorWizard extends GenericGeneratorWizard {

  public WidgetLazyLoadingGeneratorWizard() {
    super("WidgetLazyLoadingGenerator"); //$NON-NLS-1$
  }

  @Override
  protected String getPageDescription() {
    return Messages.WizardGeneratorWizard_2;
  }

  @Override
  protected String getPageTitle() {
    return Messages.WidgetLazyLoadingGeneratorWizard_titre;
  }

  @Override
  protected String getSettingsId() {
    return "WIDGET_LAZY_LOADING_GENERATOR_SETTINGS"; //$NON-NLS-1$
  }

  @Override
  protected Map<String, ValueObject> buildInput() {
    return new WidgetLazyLoadingGenerator().buildInput();
  }

  @SuppressWarnings("unchecked")
  @Override
  protected boolean executeCmd(Object param) {
    new WidgetLazyLoadingGenerator().execute((List<ValueObject>) param);
    return true;
  }
}

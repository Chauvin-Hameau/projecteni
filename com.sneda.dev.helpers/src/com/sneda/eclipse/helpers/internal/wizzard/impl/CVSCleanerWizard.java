package com.sneda.eclipse.helpers.internal.wizzard.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.dialogs.IDialogSettings;

import com.sneda.eclipse.helpers.internal.cmd.cleaner.CVSCleaner;
import com.sneda.eclipse.helpers.internal.wizzard.GenericWizard;
import com.sneda.eclipse.helpers.internal.wizzard.GenericWizardModel;
import com.sneda.eclipse.helpers.internal.wizzard.GenericWizardModelObject;

/**
 * @author nio
 */
public class CVSCleanerWizard extends GenericWizard {

  private final GenericWizardModel model;

  public CVSCleanerWizard() {
    super("CVSCleaner"); //$NON-NLS-1$

    List<GenericWizardModelObject> elements = new ArrayList<GenericWizardModelObject>();

    String defValue = getDialogSettings().get(CVSCleaner.INPUT_ROOTDIR);
    elements.add(new GenericWizardModelObject(CVSCleaner.INPUT_ROOTDIR, Messages.CVSCleanerWizard_0, defValue));

    defValue = getDialogSettings().get(CVSCleaner.INPUT_COMPONENTS);
    elements.add(new GenericWizardModelObject(CVSCleaner.INPUT_COMPONENTS, Messages.CVSCleanerWizard_1, defValue));

    defValue = getDialogSettings().get(CVSCleaner.INPUT_APPLICATION);
    elements.add(new GenericWizardModelObject(CVSCleaner.INPUT_APPLICATION, Messages.CVSCleanerWizard_2, defValue));

    defValue = getDialogSettings().get(CVSCleaner.INPUT_PRESENTATION);
    elements.add(new GenericWizardModelObject(CVSCleaner.INPUT_PRESENTATION, Messages.CVSCleanerWizard_3, defValue));

    defValue = getDialogSettings().get(CVSCleaner.INPUT_TEST_INTEGRATION);
    elements.add(new GenericWizardModelObject(CVSCleaner.INPUT_TEST_INTEGRATION, Messages.CVSCleanerWizard_4, defValue));

    defValue = getDialogSettings().get(CVSCleaner.INPUT_DRY_RUN);
    elements.add(new GenericWizardModelObject(CVSCleaner.INPUT_DRY_RUN, Messages.CVSCleanerWizard_5, defValue));

    model = new GenericWizardModel();
    model.setElements(elements);
  }

  @Override
  protected String getPageDescription() {
    return Messages.CVSCleanerWizard_6;
  }

  @Override
  protected String getPageTitle() {
    return Messages.CVSCleanerWizard_7;
  }

  @Override
  protected Map<String, String> getCmdParam() {
    Map<String, String> valuesByMsges = new HashMap<String, String>();
    for (GenericWizardModelObject each : model.getElements()) {
      valuesByMsges.put(each.getMsge(), each.getValue().toString());
    }
    return valuesByMsges;
  }

  @Override
  protected boolean executeCmd(Object param) {
    @SuppressWarnings("unchecked")
    Map<String, String> valuesByMsges = (Map<String, String>) param;

    String root = valuesByMsges.get(CVSCleaner.INPUT_ROOTDIR) + "/"; //$NON-NLS-1$
    String application = valuesByMsges.get(CVSCleaner.INPUT_APPLICATION) + "/"; //$NON-NLS-1$
    String components = valuesByMsges.get(CVSCleaner.INPUT_COMPONENTS) + "/"; //$NON-NLS-1$
    String presentation = valuesByMsges.get(CVSCleaner.INPUT_PRESENTATION) + "/"; //$NON-NLS-1$
    String integration = valuesByMsges.get(CVSCleaner.INPUT_TEST_INTEGRATION) + "/"; //$NON-NLS-1$
    Boolean dryRun = Boolean.parseBoolean(valuesByMsges.get(CVSCleaner.INPUT_DRY_RUN));

    if (root != null && application != null && components != null && presentation != null) {
      new CVSCleaner(dryRun).cleanDirCVS(root + application, root + components, root + presentation, root + integration);
      new CVSCleaner(dryRun).cleanFileEntries(root + application, root + components, root + presentation, root
          + integration);
    }

    return true;
  }

  @Override
  protected GenericWizardModel getModel() {
    return model;
  }

  @Override
  protected String getSettingsId() {
    return "CVS_CLEANER_SETTINGS"; //$NON-NLS-1$
  }

  @Override
  public IDialogSettings getDialogSettings() {
    IDialogSettings settings = super.getDialogSettings();
    if (settings.get(CVSCleaner.INPUT_DRY_RUN) == null) {
      settings.put(CVSCleaner.INPUT_DRY_RUN, true);
    }
    return settings;
  }
}

package com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator.tableau;

/**
 * @author dimitric
 *
 */
public class Column {

  private String nameColumn;
  private String sizeColumn;
  private String typeColumn;
  private String resizableColumn;
  private String masquableColumn;
  private boolean editable;

  /**
   * @param nameColumn
   * @param sizeColumn
   * @param typeColumn
   * @param resizableColumn
   * @param masquableColumn
   */
  public Column(String nameColumn, String sizeColumn, String typeColumn, String resizableColumn, String masquableColumn) {
    super();
    this.nameColumn = nameColumn;
    this.sizeColumn = sizeColumn;
    this.typeColumn = typeColumn;
    this.resizableColumn = resizableColumn;
    this.masquableColumn = masquableColumn;
  }

  /**
   * @param nameColumn
   * @param sizeColumn
   * @param typeColumn
   * @param resizableColumn
   * @param masquableColumn
   * @param editable
   */
  public Column(String nameColumn, String sizeColumn, String typeColumn, String resizableColumn,
      String masquableColumn, boolean editable) {
    super();
    this.nameColumn = nameColumn;
    this.sizeColumn = sizeColumn;
    this.typeColumn = typeColumn;
    this.resizableColumn = resizableColumn;
    this.masquableColumn = masquableColumn;
    this.editable = editable;
  }

  /**
   * @return the nameColumn
   */
  public String getNameColumn() {
    return nameColumn;
  }

  /**
   * @param nameColumn the nameColumn to set
   */
  public void setNameColumn(String nameColumn) {
    this.nameColumn = nameColumn;
  }

  /**
   * @return the sizeColumn
   */
  public String getSizeColumn() {
    return sizeColumn;
  }

  /**
   * @param sizeColumn the sizeColumn to set
   */
  public void setSizeColumn(String sizeColumn) {
    this.sizeColumn = sizeColumn;
  }

  /**
   * @return the typeColumn
   */
  public String getTypeColumn() {
    return typeColumn;
  }

  /**
   * @param typeColumn the typeColumn to set
   */
  public void setTypeColumn(String typeColumn) {
    this.typeColumn = typeColumn;
  }

  /**
   * @return the resizableColumn
   */
  public String getResizableColumn() {
    return resizableColumn;
  }

  /**
   * @param resizableColumn the resizableColumn to set
   */
  public void setResizableColumn(String resizableColumn) {
    this.resizableColumn = resizableColumn;
  }

  /**
   * @return the masquableColumn
   */
  public String getMasquableColumn() {
    return masquableColumn;
  }

  /**
   * @param masquableColumn the masquableColumn to set
   */
  public void setMasquableColumn(String masquableColumn) {
    this.masquableColumn = masquableColumn;
  }

  /**
   * @return the editable
   */
  public boolean isEditable() {
    return editable;
  }

  /**
   * @param editable the editable to set
   */
  public void setEditable(boolean editable) {
    this.editable = editable;
  }

}

package com.sneda.eclipse.helpers.internal.wizzard.impl;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

  private static final String BUNDLE_NAME = "com.sneda.eclipse.helpers.internal.wizzard.impl.messages"; //$NON-NLS-1$

  public static String CVSCleanerWizard_0;

  public static String CVSCleanerWizard_1;

  public static String CVSCleanerWizard_2;

  public static String CVSCleanerWizard_3;

  public static String CVSCleanerWizard_4;

  public static String CVSCleanerWizard_5;

  public static String CVSCleanerWizard_6;

  public static String CVSCleanerWizard_7;

  public static String EditorGeneratorWizard_0;

  public static String EditorGeneratorWizard_1;

  public static String PartGeneratorWizard_1;

  public static String PartGeneratorWizard_2;

  public static String TableGeneratorWizard_0;

  public static String TableGeneratorWizard_1;

  public static String UpdateTableGeneratorWizard_0;

  public static String UpdateTableGeneratorWizard_1;

  public static String ViewGeneratorWizard_0;

  public static String ViewGeneratorWizard_1;

  public static String WidgetContentProposalGeneratorWizard_titre;

  public static String WidgetLazyLoadingGeneratorWizard_titre;

  public static String WizardGeneratorWizard_0;

  public static String WizardGeneratorWizard_1;

  public static String WizardGeneratorWizard_2;

  public static String WizardGeneratorWizard_3;

  static {
    // initialize resource bundle
    NLS.initializeMessages(BUNDLE_NAME, Messages.class);
  }

  private Messages() {}
}

package com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator.tableau;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * @author dimitric
 *
 */
public class TableCreateColumnContentProvider implements IStructuredContentProvider {

  /*
   * (non-Javadoc)
   * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
   */
  public Object[] getElements(Object inputElement) {
    List<Column> columns = (List<Column>) inputElement;
    return columns.toArray();
  }

  public void dispose() {
    /* Do nothing */
  }

  public void inputChanger(Viewer viewer, Object oldIput, Object newInput) {
    /* Do nothing */
  }

}
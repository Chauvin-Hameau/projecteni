package com.sneda.eclipse.helpers.internal.wizzard.impl.editorihmetendue;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.wizard.Wizard;

import com.sneda.eclipse.helpers.internal.wizzard.GenericWizardModelObject;
import com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator.tableau.ColumnCreationWizardPage;

public class EditeurIhmEtendueWizard extends Wizard {

	private EditeurIhmEtendueWizardPage editeurIhmEtendueWizardPage;
	private GenericWizardModelObject genericWizardModelObject;
	private TableViewer tableViewer;
	private EditeurIhmEtendueParams editeurIhmEtendueParams;

	public EditeurIhmEtendueWizard(GenericWizardModelObject genericWizardModelObject, TableViewer tableViewer) {
		this.genericWizardModelObject = genericWizardModelObject;
		this.tableViewer = tableViewer;
	}

	@Override
	public void addPages() {
		if (editeurIhmEtendueWizardPage == null) {
			editeurIhmEtendueWizardPage = new EditeurIhmEtendueWizardPage("Configuration IHM étendue",
					genericWizardModelObject, this);
			addPage(editeurIhmEtendueWizardPage);
		}
	}

	@Override
	public boolean performFinish() {
		editeurIhmEtendueWizardPage.buildLineParametres();
		tableViewer.refresh();
		return true;
	}

	public EditeurIhmEtendueWizardPage getEditeurIhmEtendueWizardPage() {
		return editeurIhmEtendueWizardPage;
	}

	public void setEditeurIhmEtendueWizardPage(EditeurIhmEtendueWizardPage editeurIhmEtendueWizardPage) {
		this.editeurIhmEtendueWizardPage = editeurIhmEtendueWizardPage;
	}

	public GenericWizardModelObject getGenericWizardModelObject() {
		return genericWizardModelObject;
	}

	public void setGenericWizardModelObject(GenericWizardModelObject genericWizardModelObject) {
		this.genericWizardModelObject = genericWizardModelObject;
	}

	public TableViewer getTableViewer() {
		return tableViewer;
	}

	public void setTableViewer(TableViewer tableViewer) {
		this.tableViewer = tableViewer;
	}

	public EditeurIhmEtendueParams getEditeurIhmEtendueParams() {
		return editeurIhmEtendueParams;
	}

	public void setEditeurIhmEtendueParams(EditeurIhmEtendueParams editeurIhmEtendueParams) {
		this.editeurIhmEtendueParams = editeurIhmEtendueParams;
	}

}

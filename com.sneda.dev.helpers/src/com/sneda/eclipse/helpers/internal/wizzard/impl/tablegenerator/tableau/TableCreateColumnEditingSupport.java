package com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator.tableau;

import org.apache.commons.lang3.math.NumberUtils;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;

/**
 * @author dimitric
 *
 */
public class TableCreateColumnEditingSupport extends EditingSupport {

  private final TableCreateColumnColumn colonne;
  private final TableViewer viewer;
  private TableCreateColumn table;
  private static final String TRUE = "true";
  private static final String FALSE = "false";
  private static final String INTEGER = "Integer";
  private static final String FLOAT = "Float";
  private static final String DOUBLE = "Double";
  private static final String BYTE = "Byte";
  private static final String STRING = "String";
  private static final String DATE = "Date";
  private static final String IMAGE = "Image";

  /**
   * @param viewer
   */
  public TableCreateColumnEditingSupport(ColumnViewer viewer, TableCreateColumnColumn colonne) {
    super(viewer);
    this.table = (TableCreateColumn) viewer;
    this.viewer = (TableViewer) viewer;
    this.colonne = colonne;
  }

  /*
   * (non-Javadoc)
   * @see org.eclipse.jface.viewers.EditingSupport#getCellEditor(java.lang.Object)
   */
  @Override
  protected CellEditor getCellEditor(Object element) {
    if (this.colonne.equals(TableCreateColumnColumn.RESIZABLE)
        || this.colonne.equals(TableCreateColumnColumn.MASQUABLE)) {
      return new ComboBoxCellEditor(viewer.getTable(), new String[] {TRUE, FALSE});
    } else if (this.colonne.equals(TableCreateColumnColumn.TYPE)) {
      return new ComboBoxCellEditor(viewer.getTable(), new String[] {INTEGER, FLOAT, DOUBLE, BYTE, STRING, DATE, IMAGE});
    } else if (this.colonne.equals((TableCreateColumnColumn.EDITABLE))) {
      return new CheckboxCellEditor(viewer.getTable(), SWT.CHECK);
    }
    return new TextCellEditor(viewer.getTable());

  }

  /*
   * (non-Javadoc)
   * @see org.eclipse.jface.viewers.EditingSupport#canEdit(java.lang.Object)
   */
  @Override
  protected boolean canEdit(Object element) {
    return true;
  }

  /*
   * (non-Javadoc)
   * @see org.eclipse.jface.viewers.EditingSupport#getValue(java.lang.Object)
   */
  @Override
  protected Object getValue(Object element) {
    if (element instanceof Column) {
      switch (colonne) {
        case NAME:
          return ((Column) element).getNameColumn();
        case SIZE:
          return ((Column) element).getSizeColumn();
        case TYPE:
          switch (((Column) element).getTypeColumn()) {
            case INTEGER:
              return 0;
            case FLOAT:
              return 1;
            case DOUBLE:
              return 2;
            case BYTE:
              return 3;
            case STRING:
              return 4;
            case DATE:
              return 5;
            case IMAGE:
              return 6;
            default:
          }
          return ((Column) element).getTypeColumn();
        case RESIZABLE:
          return ((Column) element).getResizableColumn().equals(TRUE) ? 0 : 1;
        case MASQUABLE:
          return ((Column) element).getMasquableColumn().equals(TRUE) ? 0 : 1;
        case EDITABLE:
          return ((Column) element).isEditable();
      }
    }
    return null;
  }

  /*
   * (non-Javadoc)
   * @see org.eclipse.jface.viewers.EditingSupport#setValue(java.lang.Object, java.lang.Object)
   */
  @Override
  protected void setValue(Object element, Object value) {
    if ((element instanceof Column)) {
      Column col = (Column) element;
      switch (colonne) {
        case NAME:
          col.setNameColumn(value.toString());
          break;
        case SIZE:
          if (NumberUtils.isNumber(value.toString().trim())) {
            col.setSizeColumn(value.toString());
          } else {
            col.setSizeColumn("");
          }
          table.getColumnCreationWizardPage().validatePage();
          break;
        case TYPE:
          switch (value.toString()) {
            case "0":
              col.setTypeColumn(INTEGER);
              break;
            case "1":
              col.setTypeColumn(FLOAT);
              break;
            case "2":
              col.setTypeColumn(DOUBLE);
              break;
            case "3":
              col.setTypeColumn(BYTE);
              break;
            case "4":
              col.setTypeColumn(STRING);
              break;
            case "5":
              col.setTypeColumn(DATE);
              break;
            case "6":
              col.setTypeColumn(IMAGE);
              break;
            default:
          }
          break;
        case RESIZABLE:
          col.setResizableColumn(value.toString().equals("1") ? FALSE : TRUE);
          break;
        case MASQUABLE:
          col.setMasquableColumn(value.toString().equals("1") ? FALSE : TRUE);
          break;
        case EDITABLE:
          col.setEditable((boolean) value);
      }
      table.getColumnCreationWizardPage().validatePage();
      viewer.update(element, null);
    }
  }

}

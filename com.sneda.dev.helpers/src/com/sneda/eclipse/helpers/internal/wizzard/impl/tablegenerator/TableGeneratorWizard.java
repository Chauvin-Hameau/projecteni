package com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator;

import java.util.List;
import java.util.Map;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;
import com.sneda.eclipse.helpers.internal.cmd.jet.PartGenerator;
import com.sneda.eclipse.helpers.internal.cmd.jet.TableGenerator;
import com.sneda.eclipse.helpers.internal.wizzard.GenericGeneratorWizard;
import com.sneda.eclipse.helpers.internal.wizzard.impl.Messages;

public class TableGeneratorWizard extends GenericGeneratorWizard {

	public TableGeneratorWizard() {
		super("TableGenerator");
	}

	@Override
	protected Map<String, ValueObject> buildInput() {
		return new TableGenerator().buildInput();
	}

	@Override
	protected String getPageTitle() {
		return Messages.TableGeneratorWizard_1;
	}

	@Override
	protected String getPageDescription() {
		return Messages.TableGeneratorWizard_0;
	}

	@Override
	protected String getSettingsId() {
		 return "TABLE_GENERATOR_SETTINGS"; //$NON-NLS-1$
	}

	@Override
	protected boolean executeCmd(Object param) {
		new TableGenerator().execute((List<ValueObject>) param);
	    return true;
	}

}

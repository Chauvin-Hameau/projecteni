package com.sneda.eclipse.helpers.internal.wizzard;

import java.util.Collection;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;

/**
 * @author nio
 */
public class GenericWizardModelObject extends AbstractModelObject {

  private ValueObject valueObject;

  public GenericWizardModelObject(String msge, String help) {
    this(new ValueObject().put(ValueObject.PROP_MSGE, msge).put(ValueObject.PROP_HELP, help));
  }

  public GenericWizardModelObject(String msge, String help, Object value) {
    this(new ValueObject().put(ValueObject.PROP_MSGE, msge).put(ValueObject.PROP_HELP, help).setValue(value));
  }

  public GenericWizardModelObject(ValueObject valueObject) {
    this.valueObject = valueObject;
  }

  public String getMsge() {
    return valueObject.get(ValueObject.PROP_MSGE).toString();
  }

  public String getHelp() {
    return valueObject.get(ValueObject.PROP_HELP).toString();
  }

  public String getPath() {
    return valueObject.get(ValueObject.PROP_PATH).toString();
  }

  public Object getValue() {
    return valueObject.getValue();
  }

  public void setValue(Object value) {
    valueObject.setValue(value);
  }

  public void accept(Collection<ValueObject> visitor) {
    visitor.add(valueObject);
  }
}

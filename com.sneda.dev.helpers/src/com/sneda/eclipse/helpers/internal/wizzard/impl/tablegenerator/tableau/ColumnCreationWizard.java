package com.sneda.eclipse.helpers.internal.wizzard.impl.tablegenerator.tableau;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.wizard.Wizard;

import com.sneda.eclipse.helpers.internal.wizzard.GenericWizard;
import com.sneda.eclipse.helpers.internal.wizzard.GenericWizardModelObject;

/**
 * @author dimitric
 *
 */
public class ColumnCreationWizard extends Wizard {

  private GenericWizardModelObject column;
  private TableViewer tableViewer;
  private ColumnCreationWizardPage columnCreationWizardPage;
  private GenericWizard genericWizardPage;

  private List<Column> columns = new ArrayList<>();

  /**
   * @param elmt
   * @param tableViewer
   * @param genericWizard
   */
  public ColumnCreationWizard(GenericWizardModelObject elmt, TableViewer tableViewer, GenericWizard genericWizard) {
    this.genericWizardPage = genericWizard;
    this.column = elmt;
    this.tableViewer = tableViewer;
  }

  /*
   * (non-Javadoc)
   * @see org.eclipse.jface.wizard.Wizard#performFinish()
   */
  @Override
  public boolean performFinish() {
    getColumnCreationWizardPage().buildAllColumn();
    this.tableViewer.refresh();
    this.genericWizardPage.canFinish();
    return true;
  }

  @Override
  public boolean performCancel() {
    getColumnCreationWizardPage().setErrorMessage(null);
    return true;
  }

  /*
   * (non-Javadoc)
   * @see org.eclipse.jface.wizard.IWizard#addPages()
   */
  @Override
  public void addPages() {
    if (columnCreationWizardPage == null) {
      columnCreationWizardPage = new ColumnCreationWizardPage("Création des colonnes", column, this);
      addPage(columnCreationWizardPage);
    }
  }

  /**
   * @return the column
   */
  public GenericWizardModelObject getColumn() {
    return column;
  }

  /**
   * @param column the column to set
   */
  public void setColumn(GenericWizardModelObject column) {
    this.column = column;
  }

  /**
   * @return the columnCreationWizardPage
   */
  public ColumnCreationWizardPage getColumnCreationWizardPage() {
    return columnCreationWizardPage;
  }

  /**
   * @return the columns
   */
  public List<Column> getColumns() {
    return columns;
  }

  /**
   * @param columns the columns to set
   */
  public void setColumns(List<Column> columns) {
    this.columns = columns;
  }

}

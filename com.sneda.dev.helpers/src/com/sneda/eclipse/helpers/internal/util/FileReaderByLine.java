package com.sneda.eclipse.helpers.internal.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author nio
 */
public class FileReaderByLine {

  private final File file;

  private final IProcessor<String, ?> lineProcessor;

  public FileReaderByLine(File file, IProcessor<String, ?> lineProcessor) {
    this.file = file;
    this.lineProcessor = lineProcessor;
  }

  public boolean execute() {
    BufferedReader reader = null;

    try {
      FileInputStream fis = new FileInputStream(file);
      BufferedInputStream bis = new BufferedInputStream(fis);
      reader = new BufferedReader(new InputStreamReader(bis));

      String l = null;
      while ((l = reader.readLine()) != null) {
        if (lineProcessor.process(l) == null) {
          return false;
        }
      }

    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    finally {
      if (reader != null) {
        try {
          // dispose all the resources after using them.
          reader.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return true;
  }
}

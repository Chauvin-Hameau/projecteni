package com.sneda.eclipse.helpers.internal.util;

import java.io.File;
import java.io.IOException;

/**
 * @author nio
 */
public interface ICopier {

  void copy(File s, File t) throws IOException;
}
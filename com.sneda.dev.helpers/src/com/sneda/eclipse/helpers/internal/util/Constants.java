/*
 * Copyright SNEDA
 * (c) All rights reserved 2009.
 */
package com.sneda.eclipse.helpers.internal.util;

import com.sneda.eclipse.helpers.internal.lang.Messages;

/**
 * @author nio
 * 
 */
public abstract class Constants {

  // This could be a button accessible from the SnedaConsole
  public static final boolean DEBUG = false;

  public static final String NULL_TESTER_INFO = Messages.Constants_0;
  public static final String FEATURE_FILLER_INFO = Messages.Constants_1;
}

/*
 * Copyright SNEDA
 * (c) All rights reserved 2009.
 */
package com.sneda.eclipse.helpers.internal.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.swing.text.Segment;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import com.sneda.eclipse.helpers.internal.Activator;

/**
 * @author nio
 *
 */
public abstract class EclipsePluginTools {

	private static final ILog LOGGER = Activator.getDefault().getLog();
	private static final String SLASH_SEPARATOR = "\\";

	public static IResource getCurrentSelection() {
		// Get current part
		IWorkbench wb = PlatformUI.getWorkbench();
		IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
		IWorkbenchPage page = win.getActivePage();
		IWorkbenchPart part = page.getActivePart();

		if (part instanceof ViewPart && ((ViewPart) part).getSite() != null
				&& ((ViewPart) part).getSite().getSelectionProvider() != null) {
			ISelection sel = ((ViewPart) part).getSite().getSelectionProvider().getSelection();
			if (sel instanceof TreeSelection) {
				Object out = ((TreeSelection) sel).getFirstElement();
				if (out != null) {
					if (out instanceof IResource) {
						return (IResource) out;
					}
					if (out instanceof IJavaElement) {
						try {
							return ((IJavaElement) out).getCorrespondingResource();
						} catch (JavaModelException e) {
							return null;
						}
					}
				}
			}
		}
		return null;
	}

	public static String guessBundleSymbolicName(IResource resource) {
		if (resource == null) {
			return null;
		}
		String projectDir = resource.getProject().getLocation().toString().trim();
		try {
			File file = new File(projectDir, "META-INF/MANIFEST.MF");
			Manifest manifest = new Manifest();
			if (file.exists()) {
				InputStream in = new FileInputStream(file);
				try {
					manifest.read(in);
				} finally {
					in.close();
				}
				Attributes mainAttributes = manifest.getMainAttributes();
				String out = mainAttributes.getValue("Bundle-SymbolicName");
				if (out.contains(";")) {
					return out.substring(0, out.indexOf(";"));
				}
				return out;
			}
		} catch (Exception e) {
			LOGGER.log(new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e));
			// Keep with default
		}
		return null;
	}

	public static String guessSrcDir(IResource resource) {
		if (resource != null) {
			return chaines(resource.getFullPath().toString())[0];
		}
		return null;
	}

	public static String guessPackageName(IResource resource) {
		if (resource != null) {
			return chaines(resource.getFullPath().toString())[1];
		}
		return null;
	}

	public static String cleanUpPackageName(String packageName) {
		packageName = packageName.replaceAll("/", "\\.");
		packageName = packageName.replaceAll("\\\\", "\\.");
		if (packageName.endsWith(".")) {
			packageName = packageName.substring(0, packageName.length() - 1);
		}
		if (packageName.startsWith(".")) {
			packageName = packageName.substring(1, packageName.length());
		}
		return packageName;
	}

	public static int sum(int[] array) {
		double sum = 0;
		for (int value : array) {
			sum += value;
		}
		return (int) sum;
	}

	public static double sum(double[] array) {
		double sum = 0;
		for (double value : array) {
			sum += value;
		}
		return sum;
	}

	private static String[] createSrc(String[] splitChaine) {
		String[] chaineSrc = new String[2];
		StringBuilder sb = new StringBuilder();
		chaineSrc[0] = '\\' +splitChaine[0] + '\\' + splitChaine[1];

		for(int i = 2; i < splitChaine.length; i++) {
			if(i + 1 != splitChaine.length) {
				sb.append(splitChaine[i] + '\\');
			}else {
				sb.append(splitChaine[i]);
			}
		}
		chaineSrc[1] = sb.toString().replaceAll(java.util.regex.Matcher.quoteReplacement("\\"), ".");
		return chaineSrc;
 	}

	private static String[] createSrcMainJava(String[] splitChaine) {
		String[] chaineSrc = new String[2];
		StringBuilder sb = new StringBuilder();
		chaineSrc[0] = '\\' +splitChaine[0] + '\\' + splitChaine[1] + '\\' + splitChaine[2] + '\\' + splitChaine[3];

		for(int i = 4; i < splitChaine.length; i++) {
			if(i + 1 != splitChaine.length) {
				sb.append(splitChaine[i] + '\\');
			}else {
				sb.append(splitChaine[i]);
			}
		}
		chaineSrc[1] = sb.toString().replaceAll(java.util.regex.Matcher.quoteReplacement("\\"), ".");
		return chaineSrc;
	}

	public static String[] chaines(String chaine){
		String[] splitChaine = chaine.substring(1, chaine.length()).split("/");
		if(splitChaine.length >= 4) {
			if("src".equals(splitChaine[1]) && "main".equals(splitChaine[2]) && "java".equals(splitChaine[3])) {
				return createSrcMainJava(splitChaine);
			}else {
				return createSrc(splitChaine);
			}
		}else {
			return createSrc(splitChaine);
		}
	}
}

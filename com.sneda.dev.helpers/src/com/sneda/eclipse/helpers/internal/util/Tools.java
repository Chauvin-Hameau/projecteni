/*
 * Copyright SNEDA
 * (c) All rights reserved 2009.
 */
package com.sneda.eclipse.helpers.internal.util;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * @author nio
 * 
 */
public abstract class Tools {

  public static void silentlyClose(Closeable... closables) {
    for (Closeable each : closables) {
      if (each != null) {
        try {
          each.close();
        } catch (IOException e) {}
      }
    }
  }

  public static File getDepthFirstFileMatching(File directory, Pattern pattern) {
    File[] found = directory.listFiles();
    if (found != null) {
      for (int i = 0; i < found.length; i++) {
        if (found[i].isDirectory()) {
          return getDepthFirstFileMatching(found[i], pattern);
        } else {
          if (pattern.matcher(found[i].getName()).matches()) {
            return found[i];
          }
        }
      }
    }
    return null;
  }
}

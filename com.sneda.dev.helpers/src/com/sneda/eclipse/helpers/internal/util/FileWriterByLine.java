/*
 * Copyright SNEDA
 * (c) All rights reserved 2009.
 */
package com.sneda.eclipse.helpers.internal.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author nio
 */
public class FileWriterByLine {

  private final File in;

  private File out;

  private final IProcessor<String, String> lineProcessor;

  private boolean replaceInputFile;

  public FileWriterByLine(File in, IProcessor<String, String> lineProcessor) {
    this.in = in;
    this.out = null;
    try {
      this.out = File.createTempFile("old", "new.txt");
    } catch (IOException e) {
      throw new RuntimeException("Error creating temporary file", e);
    }
    out.deleteOnExit();
    this.lineProcessor = lineProcessor;
    replaceInputFile = true;
  }

  public FileWriterByLine(File in, File out, IProcessor<String, String> lineProcessor) {
    this.in = in;
    this.out = out;
    this.lineProcessor = lineProcessor;
  }

  private void replace() {
    // Rename temporary file back to original file
    if (out.length() != 0) {
      if (!in.delete()) {
        System.err.println("Cannot delete original file: " + in.getPath());
        String str = in.getPath() + "-up";
        System.err.println("Rename temporary file to: " + str);

        File ff = new File(str);
        if (ff.exists()) {
          if (!ff.delete()) {
            throw new RuntimeException("Manually file removal needed: " + ff.getPath());
          }
        }
        out.renameTo(ff);
      } else if (!out.renameTo(in)) {
        throw new RuntimeException("Cannot rename temporary file");
      }
    }
  }

  public void execute() throws IOException {
    FileReader fr = new FileReader(in);
    BufferedReader br = new BufferedReader(fr);

    FileWriter fw = new FileWriter(out.getPath());
    BufferedWriter bw = new BufferedWriter(fw);

    String l = br.readLine();
    while (l != null) {
      boolean wroteLine = false;
      if (lineProcessor == null || (l = lineProcessor.process(l)) != null) {
        bw.write(l);
        wroteLine = true;
      }
      l = br.readLine();
      if (l != null && wroteLine) {
        bw.newLine();
      }
    }

    br.close();
    bw.close();

    if (replaceInputFile) {
      replace();
    }
  }
}

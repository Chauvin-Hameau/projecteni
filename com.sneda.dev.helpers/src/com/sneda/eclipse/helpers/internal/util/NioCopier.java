package com.sneda.eclipse.helpers.internal.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * @author nio
 */
public class NioCopier implements ICopier {

  public void copy(File s, File t) throws IOException {
    FileChannel in = (new FileInputStream(s)).getChannel();
    FileChannel out = (new FileOutputStream(t)).getChannel();
    in.transferTo(0, s.length(), out);
    in.close();
    out.close();
  }
}

package com.sneda.eclipse.helpers.internal.util;

/**
 * @author nio
 */
public interface ICommand<T> {

  Object execute(T arg);

  public interface NullArg {}

  public interface ICommandNullArg extends ICommand<NullArg> {}
}

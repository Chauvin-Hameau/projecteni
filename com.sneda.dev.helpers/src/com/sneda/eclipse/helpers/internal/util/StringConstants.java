package com.sneda.eclipse.helpers.internal.util;

public enum StringConstants {
  PROJECT4DEV("com.sneda.eclipse.tools");

  private String key;

  private StringConstants(String key) {
    this.key = key;
  }

  public String asString() {
    return key;
  }
}

package com.sneda.eclipse.helpers.internal.util;

/**
 * @author nio
 */
public interface LineProcessor extends IProcessor<String, String> {

  String process(String line);
}

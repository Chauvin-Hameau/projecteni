/*
 * Copyright SNEDA
 * (c) All rights reserved 2009.
 */
package com.sneda.eclipse.helpers.internal.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author nio
 */
public abstract class FileTools {

  // Process all files and directories under dir
  public static void visitAllDirsAndFiles(File dir, IProcessor<File, ?> cmd) {
    cmd.process(dir);

    if (dir.isDirectory()) {
      String[] children = dir.list();
      for (int i = 0; i < children.length; i++) {
        visitAllDirsAndFiles(new File(dir, children[i]), cmd);
      }
    }
  }

  // Process only directories under dir
  public static void visitAllDirs(File dir, IProcessor<File, ?> cmd) {
    if (dir.isDirectory()) {
      cmd.process(dir);

      String[] children = dir.list();
      for (int i = 0; i < children.length; i++) {
        visitAllDirs(new File(dir, children[i]), cmd);
      }
    }
  }

  // Process only files under dir
  public static void visitAllFiles(File dir, IProcessor<File, ?> cmd) {
    if (dir.isDirectory()) {
      String[] children = dir.list();
      for (int i = 0; i < children.length; i++) {
        visitAllFiles(new File(dir, children[i]), cmd);
      }
    } else {
      cmd.process(dir);
    }
  }

  // Deletes all files and subdirectories under dir.
  // Returns true if all deletions were successful.
  // If a deletion fails, the method stops attempting to delete and returns false.
  public static boolean deleteDir(File dir) {
    if (dir.isDirectory()) {
      String[] children = dir.list();
      for (int i = 0; i < children.length; i++) {
        boolean success = deleteDir(new File(dir, children[i]));
        if (!success) {
          return false;
        }
      }
    }

    // The directory is now empty so delete it
    return dir.delete();
  }

  // Tests if the given file is empty or not
  public static boolean isEmpty(File file) throws IOException {
    FileInputStream fis = null;
    try {
      fis = new FileInputStream(file);
      int b = fis.read();
      return (b == -1);
    } catch (IOException e) {
      throw e;
    }
    finally {
      if (fis != null) {
        fis.close();
      }
    }

  }

  // Append line to the given file
  public static void appendLine(File file, String line) throws IOException {
    BufferedWriter out = null;
    try {
      FileWriter fstream = new FileWriter(file, true);
      out = new BufferedWriter(fstream);
      if (!isEmpty(file)) {
        out.newLine();
      }
      out.write(line);
    }
    finally {
      if (out != null) {
        out.close();
      }
    }
  }
}

package com.sneda.eclipse.helpers.internal.util;

/**
 * @author nio
 */
public interface IProcessor<T, O> {

  O process(T obj);
}

/*
 * Copyright SNEDA
 * (c) All rights reserved 2009.
 */
package com.sneda.eclipse.helpers.internal.util;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.graphics.Rectangle;

/**
 * @author nio
 * 
 */
public abstract class DialogTools {

  public static int DEFAULT_MIN_WIDTH = 800;
  public static int DEFAULT_MIN_HEIGHT = 600;

  public static void setMinSize(Dialog dialog, int minWidth, int minHeight) {
    Rectangle boundsShell = dialog.getShell().getParent().getBounds();
    Rectangle bounds = new Rectangle(boundsShell.x + (boundsShell.width - minWidth) / 2, boundsShell.y
        + (boundsShell.height - minHeight) / 2, minWidth, minHeight);
    dialog.getShell().setBounds(bounds);
    dialog.getShell().setMinimumSize(minWidth, minHeight);
  }

  public static int createAndOpen(WizardDialog dialog) {
    dialog.setHelpAvailable(false);
    dialog.create();
    DialogTools.setMinSize(dialog, DialogTools.DEFAULT_MIN_WIDTH, DialogTools.DEFAULT_MIN_HEIGHT);
    return dialog.open();
  }
}

package com.sneda.eclipse.helpers.internal;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.swt.widgets.Display;

import com.sneda.eclipse.helpers.handlers.PropathBuilderHandler;
import com.sneda.eclipse.helpers.internal.console.ConsoleSupport;

public class PropathAutoBuilderListener implements IResourceChangeListener {

  private final ConsoleSupport console = new ConsoleSupport();

  private void doRebuildPropath() {
    Display.getDefault().asyncExec(new Runnable() {

      public void run() {
        try {
          console.debug("Project closed/opened, rebuilding propath...");
          new PropathBuilderHandler().execute(null);
        } catch (ExecutionException e) {
          console.printStackTrace(e);
        }
      }
    });
  }

  private void delayRebuildPropath() {
    MyRun myrun = new MyRun();
    new Thread(myrun).start();
  }

  public synchronized void resourceChanged(IResourceChangeEvent event) {
    if (event.getType() == IResourceChangeEvent.POST_CHANGE) {
      IResourceDelta delta = event.getDelta();
      if (delta != null) {
        IResourceDelta[] projDeltas = delta.getAffectedChildren(IResourceDelta.CHANGED);
        for (int i = 0; i < projDeltas.length; ++i) {
          IResourceDelta projDelta = projDeltas[i];
          if ((projDelta.getFlags() & IResourceDelta.OPEN) != 0) {
            delayRebuildPropath();
            return;
          }
        }
      }
    }
  }

  private class MyRun implements Runnable {

    public void run() {
      try {
        for (int i = 0; i < 10; i++) {
          if (ResourcesPlugin.getWorkspace().isTreeLocked()) {
            System.out.println("Sleeping...");
            Thread.sleep(1000L);
          } else {
            doRebuildPropath();
            break;
          }
        }
      } catch (InterruptedException e) {
        console.printStackTrace(e);
      }
    }
  }

}

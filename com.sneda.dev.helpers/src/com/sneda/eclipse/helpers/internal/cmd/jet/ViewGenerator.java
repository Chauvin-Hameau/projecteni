package com.sneda.eclipse.helpers.internal.cmd.jet;

import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;

/**
 * @author nio
 */
public class ViewGenerator extends AbstractJetGenerator {

  private static final String PATH_PACKAGE = "/input/elmt/view/package"; //$NON-NLS-1$
  private static final String PATH_NAME = "/input/elmt/view/name"; //$NON-NLS-1$

  @Override
  public Map<String, ValueObject> buildInput() {
    Map<String, ValueObject> inputs = super.buildInput();

    ValueObject vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_PACKAGE).put(ValueObject.PROP_HELP,
        Messages.AbstractGenerator_HelpPackage).put(ValueObject.PROP_PATH, PATH_PACKAGE);
    inputs.put(PATH_PACKAGE, vo);

    vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_NAME).put(ValueObject.PROP_HELP,
        Messages.AbstractGenerator_HelpName).put(ValueObject.PROP_PATH, PATH_NAME);
    inputs.put(PATH_NAME, vo);

    return inputs;
  }

  @Override
  protected Document convertToDocument(Map<String, ValueObject> valuesByPath) throws ParserConfigurationException {
    Document doc = super.convertToDocument(valuesByPath);
    Node input = doc.getDocumentElement();

    Element elmt = doc.createElement("elmt");
    input.appendChild(elmt);

    Element view = doc.createElement("view");
    elmt.appendChild(view);

    Element pakage = doc.createElement("package");
    pakage.appendChild(doc.createTextNode(valuesByPath.get(PATH_PACKAGE).getValue().toString()));
    view.appendChild(pakage);

    Element name = doc.createElement("name");
    name.appendChild(doc.createTextNode(valuesByPath.get(PATH_NAME).getValue().toString()));
    view.appendChild(name);

    return doc;
  }

  @Override
  protected void printPostComment(Map<String, ValueObject> valuesByPath) {
    getConsoleSupport().println(Messages.ViewGenerator_Stars);
    getConsoleSupport().println(Messages.ViewGenerator_Rem1);
    getConsoleSupport().println(Messages.ViewGenerator_Stars);
  }
}

/*
 * Copyright SNEDA
 * (c) All rights reserved 2009.
 */
package com.sneda.eclipse.helpers.internal.cmd.jet;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;
import com.sneda.eclipse.helpers.tools.ReadModifyProperties;
import com.sun.xml.internal.ws.util.StringUtils;

/**
 * @author nio
 */
public class EditorGenerator extends AbstractJetGenerator {

	private static final String PATH_PACKAGE = "/input/elmt/editor/package"; //$NON-NLS-1$
	private static final String PATH_NAME = "/input/elmt/editor/name"; //$NON-NLS-1$
	private static final String PATH_NUMOFTABS = "/input/elmt/editor/numOfTabs"; //$NON-NLS-1$
	private static final String PATH_IHMETENDUE = "/input/elmt/editor/ihmEtendue"; //$NON-NLS-1$

	public static final String MSGE_NUMOFTABS = Messages.EditorGenerator_KeyNumOfTab;
	public static final String MSGE_IHMETENDUE = Messages.EditorGenerator_KeyIhmEtendue;

	@Override
	public Map<String, ValueObject> buildInput() {
		Map<String, ValueObject> inputs = super.buildInput();

		ValueObject vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_PACKAGE)
				.put(ValueObject.PROP_HELP, Messages.AbstractGenerator_HelpPackage)
				.put(ValueObject.PROP_PATH, PATH_PACKAGE);
		inputs.put(PATH_PACKAGE, vo);

		vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_NAME)
				.put(ValueObject.PROP_HELP, Messages.AbstractGenerator_HelpName).put(ValueObject.PROP_PATH, PATH_NAME);
		inputs.put(PATH_NAME, vo);

		vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_NUMOFTABS)
				.put(ValueObject.PROP_HELP, Messages.EditorGenerator_HelpNumOfTabs)
				.put(ValueObject.PROP_PATH, PATH_NUMOFTABS);
		inputs.put(PATH_NUMOFTABS, vo);

		vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_IHMETENDUE)
				.put(ValueObject.PROP_HELP, Messages.EditorGenerator_HelpIhmEtendue)
				.put(ValueObject.PROP_PATH, PATH_IHMETENDUE);
		inputs.put(PATH_IHMETENDUE, vo);

		return inputs;
	}

	@Override
	protected Document convertToDocument(Map<String, ValueObject> valuesByPath) throws ParserConfigurationException {
		Document doc = super.convertToDocument(valuesByPath);
		Node input = doc.getDocumentElement();

		Element elmt = doc.createElement("elmt");
		input.appendChild(elmt);

		Element editor = doc.createElement("editor");
		elmt.appendChild(editor);

		Element pakage = doc.createElement("package");
		pakage.appendChild(doc.createTextNode(valuesByPath.get(PATH_PACKAGE).getValue().toString()));
		editor.appendChild(pakage);

		Element name = doc.createElement("name");
		name.appendChild(doc.createTextNode(valuesByPath.get(PATH_NAME).getValue().toString()));
		editor.appendChild(name);

		// Manage tabs
		ValueObject value = valuesByPath.get(PATH_NUMOFTABS);
		if (value != null) {
			int n = Integer.parseInt((String) value.getValue());
			for (int i = 0; i < n; i++) {
				Element tab = doc.createElement("tab");
				editor.appendChild(tab);

				Element id = doc.createElement("id");
				id.appendChild(doc.createTextNode("" + (i + 1)));
				tab.appendChild(id);
			}
		}

		List<String> parametresIhmEtendue = describeParametre(valuesByPath.get(PATH_IHMETENDUE).getValue().toString());

		Element parametresEditorIhmEtendue = doc.createElement("parametresEditorIhmEtendue");
		editor.appendChild(parametresEditorIhmEtendue);

		Element isIhmEtendue = doc.createElement("isIhmEtendue");
		isIhmEtendue.appendChild(doc.createTextNode(parametresIhmEtendue.get(0)));
		parametresEditorIhmEtendue.appendChild(isIhmEtendue);

		if ("ihmEtendue".equals(parametresIhmEtendue.get(0))) {

			Element nomEntiteObjet = doc.createElement("nomEntiteObjet");
			nomEntiteObjet.appendChild(doc.createTextNode(StringUtils.capitalize(parametresIhmEtendue.get(1))));
			parametresEditorIhmEtendue.appendChild(nomEntiteObjet);

			Element nomEntiteVariable = doc.createElement("nomEntiteVariable");
			nomEntiteVariable.appendChild(doc.createTextNode(StringUtils.decapitalize(parametresIhmEtendue.get(1))));
			parametresEditorIhmEtendue.appendChild(nomEntiteVariable);

			Element srcFolderCheminDAO = doc.createElement("srcFolderCheminDAO");
			srcFolderCheminDAO.appendChild(doc.createTextNode(parametresIhmEtendue.get(2)));
			parametresEditorIhmEtendue.appendChild(srcFolderCheminDAO);

			Element packageCheminDAO = doc.createElement("packageCheminDAO");
			packageCheminDAO.appendChild(doc.createTextNode(parametresIhmEtendue.get(3)));
			parametresEditorIhmEtendue.appendChild(packageCheminDAO);

			Element srcFolderCheminNMCMapper = doc.createElement("srcFolderCheminNMCMapper");
			srcFolderCheminNMCMapper.appendChild(doc.createTextNode(parametresIhmEtendue.get(4)));
			parametresEditorIhmEtendue.appendChild(srcFolderCheminNMCMapper);

			Element packageCheminNMCMapper = doc.createElement("packageCheminNMCMapper");
			packageCheminNMCMapper.appendChild(doc.createTextNode(parametresIhmEtendue.get(5)));
			parametresEditorIhmEtendue.appendChild(packageCheminNMCMapper);

			Element srcFolderCheminInputCreatorCharger = doc.createElement("srcFolderCheminInputCreatorCharger");
			srcFolderCheminInputCreatorCharger.appendChild(doc.createTextNode(parametresIhmEtendue.get(6)));
			parametresEditorIhmEtendue.appendChild(srcFolderCheminInputCreatorCharger);

			Element packageCheminInputCreatorCharger = doc.createElement("packageCheminInputCreatorCharger");
			packageCheminInputCreatorCharger.appendChild(doc.createTextNode(parametresIhmEtendue.get(7)));
			parametresEditorIhmEtendue.appendChild(packageCheminInputCreatorCharger);

			Element srcFolderCheminInputCreatorCreerModifier = doc
					.createElement("srcFolderCheminInputCreatorCreerModifier");
			srcFolderCheminInputCreatorCreerModifier.appendChild(doc.createTextNode(parametresIhmEtendue.get(8)));
			parametresEditorIhmEtendue.appendChild(srcFolderCheminInputCreatorCreerModifier);

			Element packageCheminInputCreatorCreerModifier = doc
					.createElement("packageCheminInputCreatorCreerModifier");
			packageCheminInputCreatorCreerModifier.appendChild(doc.createTextNode(parametresIhmEtendue.get(9)));
			parametresEditorIhmEtendue.appendChild(packageCheminInputCreatorCreerModifier);

			Element libelleParametre = doc.createElement("libelleParametre");
			libelleParametre.appendChild(doc.createTextNode(parametresIhmEtendue.get(11)));
			parametresEditorIhmEtendue.appendChild(libelleParametre);

			ReadModifyProperties readModifyProperties = new ReadModifyProperties(parametresIhmEtendue.get(10));
			try {
				readModifyProperties.usingBufferedWritter(parametresIhmEtendue.get(12), (parametresIhmEtendue.get(11)));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return doc;
	}

	public List<String> describeParametre(String column) {
		return Arrays.asList(column.split(";"));
	}

	@Override
	protected void printPostComment(Map<String, ValueObject> valuesByPath) {
		getConsoleSupport().println(Messages.EditorGenerator_Stars);
		getConsoleSupport().println(Messages.EditorGenerator_Rem1);
		ValueObject value = valuesByPath.get(PATH_NUMOFTABS);
		if (value != null) {
			if (Integer.parseInt((String) value.getValue()) > 0) {
				getConsoleSupport().println(Messages.EditorGenerator_Rem2);
			}
		}
		getConsoleSupport().println(Messages.EditorGenerator_Rem3);
		getConsoleSupport().println(Messages.EditorGenerator_Stars);
	}
}

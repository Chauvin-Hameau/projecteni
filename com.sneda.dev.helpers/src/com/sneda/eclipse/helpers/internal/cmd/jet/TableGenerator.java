package com.sneda.eclipse.helpers.internal.cmd.jet;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;
import com.sun.xml.internal.ws.util.StringUtils;

public class TableGenerator extends AbstractJetGenerator {

  private static final String PATH_PACKAGE = "/input/elmt/table/package"; //$NON-NLS-1$
  private static final String PATH_NAME = "/input/elmt/table/name"; //$NON-NLS-1$
  private static final String PATH_CREATECOLUMN = "/input/elmt/table/createColumn";
  private static final String PATH_ENTITYNAME = "/input/elmt/table/entityName";

  public static final String MSGE_CREATECOLUMN = Messages.TableGenerator_KeyCreateOfColumn;
  public static final String MSGE_ENTITYNAME = Messages.TableGenerator_KeyEntityName;

  private static final String STYLE_CENTER = "SWT.CENTER";
  private static final String STYLE_LEFT = "SWT.LEFT";
  private static final String STYLE_RIGHT = "SWT.RIGHT";

  private static final Map<String, String> mapStyles;
  static {
    mapStyles = new HashMap<>();
    mapStyles.put("Integer", STYLE_RIGHT);
    mapStyles.put("Float", STYLE_RIGHT);
    mapStyles.put("Double", STYLE_RIGHT);
    mapStyles.put("Byte", STYLE_RIGHT);
    mapStyles.put("String", STYLE_LEFT);
    mapStyles.put("Date", STYLE_CENTER);
    mapStyles.put("Image", STYLE_CENTER);
  }

  @Override
  public Map<String, ValueObject> buildInput() {
    Map<String, ValueObject> inputs = super.buildInput();

    ValueObject vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_PACKAGE).put(ValueObject.PROP_HELP,
        Messages.AbstractGenerator_HelpPackage).put(ValueObject.PROP_PATH, PATH_PACKAGE);
    inputs.put(PATH_PACKAGE, vo);

    vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_NAME).put(ValueObject.PROP_HELP,
        Messages.AbstractGenerator_HelpName).put(ValueObject.PROP_PATH, PATH_NAME);
    inputs.put(PATH_NAME, vo);

    vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_CREATECOLUMN).put(ValueObject.PROP_HELP,
        Messages.TableGenerator_CreateColumnHelp).put(ValueObject.PROP_PATH, PATH_CREATECOLUMN);
    inputs.put(PATH_CREATECOLUMN, vo);

    vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_ENTITYNAME).put(ValueObject.PROP_HELP,
            Messages.TableGenerator_EditableColumnHelp).put(ValueObject.PROP_PATH, PATH_ENTITYNAME);
        inputs.put(PATH_ENTITYNAME, vo);

    return inputs;
  }

  @Override
  protected Document convertToDocument(Map<String, ValueObject> valuesByPath) throws ParserConfigurationException {
    Document doc = super.convertToDocument(valuesByPath);
    Node input = doc.getDocumentElement();

    Element elmt = doc.createElement("elmt");
    input.appendChild(elmt);

    Element table = doc.createElement("table");
    elmt.appendChild(table);

    Element pakage = doc.createElement("package");
    pakage.appendChild(doc.createTextNode(valuesByPath.get(PATH_PACKAGE).getValue().toString()));
    table.appendChild(pakage);

    // Element UowerCase first letter
    Element entityUpperCase = doc.createElement("Entity");
    entityUpperCase.appendChild(doc.createTextNode(StringUtils.capitalize(valuesByPath.get(PATH_ENTITYNAME).getValue().toString().trim())));
    table.appendChild(entityUpperCase);

    // Element lowerCase first letter
    Element entityLowerCase = doc.createElement("entity");
    entityLowerCase.appendChild(doc.createTextNode(StringUtils.decapitalize(valuesByPath.get(PATH_ENTITYNAME).getValue().toString().trim())));
    table.appendChild(entityLowerCase);

    Element name = doc.createElement("name");
    name.appendChild(doc.createTextNode(valuesByPath.get(PATH_NAME).getValue().toString()));
    table.appendChild(name);

    List<String> columns = getColumns(valuesByPath.get(PATH_CREATECOLUMN).getValue().toString());
    // création d'autant de noeuds que du nombre de colonnes (nombre déduit par la
    // taille de la liste).
    if (!columns.isEmpty()) {
      for (String column : columns) {
        List<String> describeColumns = describeColumns(column);

        Element columnTable = doc.createElement("columnTable");
        table.appendChild(columnTable);

        Element nameColumn = doc.createElement("nameColumn");
        nameColumn.appendChild(doc.createTextNode(describeColumns.get(0).toUpperCase()));
        columnTable.appendChild(nameColumn);

        Element sizeColumn = doc.createElement("sizeColumn");
        sizeColumn.appendChild(doc.createTextNode(describeColumns.get(1)));
        columnTable.appendChild(sizeColumn);

        Element typeColumn = doc.createElement("typeColumn");
        typeColumn.appendChild(doc.createTextNode(getStyle(describeColumns.get(2))));
        columnTable.appendChild(typeColumn);

        Element resizableColumn = doc.createElement("resizableColumn");
        resizableColumn.appendChild(doc.createTextNode(describeColumns.get(3)));
        columnTable.appendChild(resizableColumn);

        Element masquableColumn = doc.createElement("masquableColumn");
        masquableColumn.appendChild(doc.createTextNode(describeColumns.get(4)));
        columnTable.appendChild(masquableColumn);

        Element editableColumn = doc.createElement("editableColumn");
        editableColumn.appendChild(doc.createTextNode(describeColumns.get(5)));
        columnTable.appendChild(editableColumn);
      }
    }

    return doc;
  }

  public List<String> getColumns(String chaineColumn) {
    String chaineColumns = chaineColumn.trim();
    return Arrays.asList(chaineColumns.split("//"));
  }

  public List<String> describeColumns(String column) {
    return Arrays.asList(column.split(";"));
  }

  public String getStyle(String type) {
    if (mapStyles.containsKey(type)) {
      return mapStyles.get(type);
    }
    return null;
  }
}

package com.sneda.eclipse.helpers.internal.cmd;

import java.util.HashMap;
import java.util.Map;

public class ValueObject {

  public static final String PROP_MSGE = "prop_msge"; //$NON-NLS-1$
  public static final String PROP_HELP = "prop_help"; //$NON-NLS-1$
  public static final String PROP_PATH = "prop_path"; //$NON-NLS-1$

  private Object value;

  private Map<String, Object> clientProperties;

  public ValueObject() {}

  public ValueObject(Object value) {
    this.value = value;
  }

  public Object get(String key) {
    if (clientProperties != null) {
      return clientProperties.get(key);
    }
    return null;
  }

  public ValueObject put(String key, Object property) {
    if (clientProperties == null) {
      clientProperties = new HashMap<String, Object>();
    }
    clientProperties.put(key, property);
    return this;
  }

  public Object remove(String key) {
    if (clientProperties != null) {
      clientProperties.remove(key);
      if (clientProperties.isEmpty()) {
        clientProperties = null;
      }
    }
    return this;
  }

  public Object getValue() {
    return value;
  }

  public ValueObject setValue(Object value) {
    this.value = value;
    return this;
  }
}

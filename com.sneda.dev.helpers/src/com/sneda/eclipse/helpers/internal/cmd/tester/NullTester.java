package com.sneda.eclipse.helpers.internal.cmd.tester;

/**
 * @author jot
 */
public class NullTester {

  /**
   * Utilitaire pour g�n�rer le code des tests de non nullit�. Transforme :
   * 
   * <pre>
   * a().b().c();
   * </pre>
   * 
   * en
   * 
   * <pre>
   * a() != null 
   * a().b() != null
   * a().b().c() != null
   * </pre>
   */
  public String cutUp(String strToCutUp) {
    strToCutUp = strToCutUp.trim();
    if (strToCutUp.endsWith("!= null)")) {
      strToCutUp = strToCutUp.replace("!= null)", "").trim();
    }
    StringBuilder stringBuilder = new StringBuilder();

    String[] tab = strToCutUp.split("\\.");

    for (int i = 0; i < tab.length; i++) {

      for (int j = 0; j < i + 1; j++) {
        if (j != 0) {
          stringBuilder.append(".");
        }
        stringBuilder.append(tab[j]);
      }

      if (i != tab.length - 1) {
        stringBuilder.append(" != null && ");
      }

      if (i == tab.length - 1) {
        stringBuilder.append(" != null");
      }
    }
    return stringBuilder.toString();
  }
}

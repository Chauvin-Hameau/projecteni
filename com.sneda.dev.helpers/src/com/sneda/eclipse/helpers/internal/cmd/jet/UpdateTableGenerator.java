package com.sneda.eclipse.helpers.internal.cmd.jet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;
import com.sun.xml.internal.ws.util.StringUtils;

/**
 * @author dchauvinhameau
 *
 */
public class UpdateTableGenerator extends AbstractJetGenerator {

  private static final String PATH_PACKAGE = "/input/elmt/table/package"; //$NON-NLS-1$
  private static final String PATH_NAME = "/input/elmt/table/name"; //$NON-NLS-1$
  private static final String PATH_CREATECOLUMN = "/input/elmt/table/createColumn";
  private static final String PATH_ENTITYNAME = "/input/elmt/table/entityName";
  private static final String PATH_SETTINGS_UPDATE = "/input/elmt/table/settingsUpdate";

  public static final String MSGE_CREATECOLUMN = Messages.TableGenerator_KeyCreateOfColumn;
  public static final String MSGE_SETTINGS_UPDATABLE = Messages.UpdateTableGenerator_KeySettingsUpdate;
  public static final String MSGE_ENTITYNAME = Messages.TableGenerator_KeyEntityName;

  private static final String STYLE_CENTER = "SWT.CENTER";
  private static final String STYLE_LEFT = "SWT.LEFT";
  private static final String STYLE_RIGHT = "SWT.RIGHT";

  private static final Map<String, String> mapStyles;
  static {
    mapStyles = new HashMap<>();
    mapStyles.put("int", STYLE_RIGHT);
    mapStyles.put("float", STYLE_RIGHT);
    mapStyles.put("double", STYLE_RIGHT);
    mapStyles.put("byte", STYLE_RIGHT);
    mapStyles.put("String", STYLE_LEFT);
    mapStyles.put("date", STYLE_CENTER);
    mapStyles.put("image", STYLE_CENTER);
  }

  @Override
  public Map<String, ValueObject> buildInput() {
    Map<String, ValueObject> inputs = super.buildInput();

    ValueObject vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_PACKAGE).put(ValueObject.PROP_HELP,
        Messages.AbstractGenerator_HelpPackage).put(ValueObject.PROP_PATH, PATH_PACKAGE);
    inputs.put(PATH_PACKAGE, vo);

    vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_NAME).put(ValueObject.PROP_HELP,
        Messages.AbstractGenerator_HelpName).put(ValueObject.PROP_PATH, PATH_NAME);
    inputs.put(PATH_NAME, vo);

    vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_CREATECOLUMN).put(ValueObject.PROP_HELP,
        Messages.TableGenerator_CreateColumnHelp).put(ValueObject.PROP_PATH, PATH_CREATECOLUMN);
    inputs.put(PATH_CREATECOLUMN, vo);

    vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_SETTINGS_UPDATABLE).put(ValueObject.PROP_HELP,
        Messages.UpdateTableGenerator_SettingsUpdateHelp).put(ValueObject.PROP_PATH, PATH_SETTINGS_UPDATE);
    inputs.put(PATH_SETTINGS_UPDATE, vo);

    vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_ENTITYNAME).put(ValueObject.PROP_HELP,
        Messages.TableGenerator_EditableColumnHelp).put(ValueObject.PROP_PATH, PATH_ENTITYNAME);
    inputs.put(PATH_ENTITYNAME, vo);

    return inputs;
  }

  @Override
  protected Document convertToDocument(Map<String, ValueObject> valuesByPath) throws ParserConfigurationException {
    Document doc = super.convertToDocument(valuesByPath);
    Node input = doc.getDocumentElement();

    Element elmt = doc.createElement("elmt");
    input.appendChild(elmt);

    Element updateTable = doc.createElement("updateTable");
    elmt.appendChild(updateTable);

    Element pakage = doc.createElement("package");
    pakage.appendChild(doc.createTextNode(valuesByPath.get(PATH_PACKAGE).getValue().toString()));
    updateTable.appendChild(pakage);

    // Element UowerCase first letter
    Element entityUpperCase = doc.createElement("Entity");
    entityUpperCase.appendChild(doc.createTextNode(StringUtils.capitalize(valuesByPath.get(PATH_ENTITYNAME).getValue().toString().trim())));
    updateTable.appendChild(entityUpperCase);

    // Element lowerCase first letter
    Element entityLowerCase = doc.createElement("entity");
    entityLowerCase.appendChild(doc.createTextNode(StringUtils.decapitalize(valuesByPath.get(PATH_ENTITYNAME).getValue().toString().trim())));
    updateTable.appendChild(entityLowerCase);

    Element name = doc.createElement("name");
    name.appendChild(doc.createTextNode(valuesByPath.get(PATH_NAME).getValue().toString()));
    updateTable.appendChild(name);

    List<String> columns = getColumns(valuesByPath.get(PATH_CREATECOLUMN).getValue().toString());
    List<String> numColumnEditables = new ArrayList<>();
    // création d'autant de noeuds que du nombre de colonnes (nombre déduit par la
    // taille de la liste).
    if (!columns.isEmpty()) {
      for (String column : columns) {
        List<String> describeColumns = describeColumns(column);

        Element columnTable = doc.createElement("columnTable");
        updateTable.appendChild(columnTable);

        Element nameColumn = doc.createElement("nameColumn");
        nameColumn.appendChild(doc.createTextNode(describeColumns.get(0).toUpperCase()));
        columnTable.appendChild(nameColumn);

        Element sizeColumn = doc.createElement("sizeColumn");
        sizeColumn.appendChild(doc.createTextNode(describeColumns.get(1)));
        columnTable.appendChild(sizeColumn);

        Element typeColumn = doc.createElement("typeColumn");
        typeColumn.appendChild(doc.createTextNode(getStyle(describeColumns.get(2))));
        columnTable.appendChild(typeColumn);

        Element resizableColumn = doc.createElement("resizableColumn");
        resizableColumn.appendChild(doc.createTextNode(describeColumns.get(3)));
        columnTable.appendChild(resizableColumn);

        Element masquableColumn = doc.createElement("masquableColumn");
        masquableColumn.appendChild(doc.createTextNode(describeColumns.get(4)));
        columnTable.appendChild(masquableColumn);

        Element editableColumn = doc.createElement("editableColumn");
        editableColumn.appendChild(doc.createTextNode(describeColumns.get(5)));
        columnTable.appendChild(editableColumn);

        if (describeColumns.get(5).equals("true")) {
          numColumnEditables.add(describeColumns.get(6));
        }

      }
    }

    Element editableColumnIndex = doc.createElement("editableColumnIndex");
    updateTable.appendChild(editableColumnIndex);

    Element sizeIndexColumnEditables = doc.createElement("sizeIndexColumnEditables");
    sizeIndexColumnEditables.appendChild(doc.createTextNode(String.valueOf(numColumnEditables.size())));
    updateTable.appendChild(sizeIndexColumnEditables);

    for (String indexColumn : numColumnEditables) {
      Element indexColumnEditable = doc.createElement("indexColumn");
      indexColumnEditable.appendChild(doc.createTextNode(indexColumn));
      editableColumnIndex.appendChild(indexColumnEditable);
    }

    List<String> settingsTables = describeColumns(valuesByPath.get(PATH_SETTINGS_UPDATE).getValue().toString());

    Element settingsTable = doc.createElement("settingsTable");
    updateTable.appendChild(settingsTable);

    Element isAssistant = doc.createElement("isAssistant");
    isAssistant.appendChild(doc.createTextNode(settingsTables.get(0)));
    settingsTable.appendChild(isAssistant);

    Element isCell = doc.createElement("isCell");
    isCell.appendChild(doc.createTextNode(settingsTables.get(1)));
    settingsTable.appendChild(isCell);

    Element isCellAndAssistant = doc.createElement("isCellAndAssistant");
    isCellAndAssistant.appendChild(doc.createTextNode(settingsTables.get(2)));
    settingsTable.appendChild(isCellAndAssistant);

    Element isMonoSelection = doc.createElement("isMonoSelection");
    isMonoSelection.appendChild(doc.createTextNode(settingsTables.get(3)));
    settingsTable.appendChild(isMonoSelection);

    Element isMultiSelection = doc.createElement("isMultiSelection");
    isMultiSelection.appendChild(doc.createTextNode(settingsTables.get(4)));
    settingsTable.appendChild(isMultiSelection);

    Element isAddItem = doc.createElement("isAddItem");
    isAddItem.appendChild(doc.createTextNode(settingsTables.get(5)));
    settingsTable.appendChild(isAddItem);

    Element isUpdateItem = doc.createElement("isUpdateItem");
    isUpdateItem.appendChild(doc.createTextNode(settingsTables.get(6)));
    settingsTable.appendChild(isUpdateItem);

    Element isUpdateItems = doc.createElement("isUpdateItems");
    isUpdateItems.appendChild(doc.createTextNode(settingsTables.get(7)));
    settingsTable.appendChild(isUpdateItems);

    Element isDellItem = doc.createElement("isDellItem");
    isDellItem.appendChild(doc.createTextNode(settingsTables.get(8)));
    settingsTable.appendChild(isDellItem);

    Element isDuplicateItem = doc.createElement("isDuplicateItem");
    isDuplicateItem.appendChild(doc.createTextNode(settingsTables.get(9)));
    settingsTable.appendChild(isDuplicateItem);

    Element isMovingItem = doc.createElement("isMovingItem");
    isMovingItem.appendChild(doc.createTextNode(settingsTables.get(10)));
    settingsTable.appendChild(isMovingItem);

    return doc;
  }

  /**
   * @param chaineColumn
   * @return
   */
  public List<String> getColumns(String chaineColumn) {
    String chaineColumns = chaineColumn.trim();
    return Arrays.asList(chaineColumns.split("//"));
  }

  /**
   * @param column
   * @return
   */
  public List<String> describeColumns(String column) {
    return Arrays.asList(column.split(";"));
  }

  /**
   * @param type
   * @return
   */
  public String getStyle(String type) {
    if (type == "string") {
      type = StringUtils.capitalize(type);
    }
    if (mapStyles.containsKey(type)) {
      return mapStyles.get(type);
    }
    return STYLE_LEFT;
  }

}

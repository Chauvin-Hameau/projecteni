package com.sneda.eclipse.helpers.internal.cmd.jet;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jet.BufferedJET2Writer;
import org.eclipse.jet.JET2Context;
import org.eclipse.jet.JET2TemplateManager;
import org.eclipse.jet.JET2TemplateManager.ITemplateRunner;
import org.eclipse.jet.transform.TransformContextExtender;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;
import com.sneda.eclipse.helpers.internal.console.ConsoleSupport;
import com.sneda.eclipse.helpers.internal.util.Constants;
import com.sneda.eclipse.helpers.internal.util.ICommand;

public abstract class AbstractJetGenerator implements ICommand<Collection<ValueObject>> {

  private static final String PATH_BUNDLENAME = "/input/project/bundleName"; //$NON-NLS-1$
  private static final String PATH_SRCFOLDER = "/input/project/srcFolder"; //$NON-NLS-1$
  private static final String PATH_AUTHOR = "/input/project/author"; //$NON-NLS-1$

  public static final String MSGE_BUNDLENAME = Messages.AbstractGenerator_KeyBundleName;
  public static final String MSGE_SRCFOLDER = Messages.AbstractGenerator_KeyHome;
  public static final String MSGE_PACKAGE = Messages.AbstractGenerator_KeyPackage;
  public static final String MSGE_AUTHOR = Messages.AbstractGenerator_KeyUser;
  public static final String MSGE_NAME = Messages.AbstractGenerator_KeySuffix;

  private final ConsoleSupport support = new ConsoleSupport();

  public Map<String, ValueObject> buildInput() {
    Map<String, ValueObject> inputs = new LinkedHashMap<String, ValueObject>();

    ValueObject vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_BUNDLENAME).put(ValueObject.PROP_HELP,
        Messages.AbstractGenerator_HelpBundleName).put(ValueObject.PROP_PATH, PATH_BUNDLENAME);
    inputs.put(PATH_BUNDLENAME, vo);

    vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_SRCFOLDER).put(ValueObject.PROP_HELP,
        Messages.AbstractGenerator_HelpSrcFolder).put(ValueObject.PROP_PATH, PATH_SRCFOLDER);
    inputs.put(PATH_SRCFOLDER, vo);

    vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_AUTHOR).put(ValueObject.PROP_HELP,
        Messages.AbstractGenerator_HelpAuthor).put(ValueObject.PROP_PATH, PATH_AUTHOR);
    inputs.put(PATH_AUTHOR, vo);

    return inputs;
  }

  protected final ConsoleSupport getConsoleSupport() {
    return support;
  }

  protected Document convertToDocument(Map<String, ValueObject> valuesByPath) throws ParserConfigurationException {
    DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

    Document doc = docBuilder.newDocument();
    Element input = doc.createElement("input");
    doc.appendChild(input);

    Element project = doc.createElement("project");
    input.appendChild(project);

    Element bundleName = doc.createElement("bundleName");
    bundleName.appendChild(doc.createTextNode(valuesByPath.get(PATH_BUNDLENAME).getValue().toString()));
    project.appendChild(bundleName);

    Element srcFolder = doc.createElement("srcFolder");
    srcFolder.appendChild(doc.createTextNode(valuesByPath.get(PATH_SRCFOLDER).getValue().toString()));
    project.appendChild(srcFolder);

    Element author = doc.createElement("author");
    author.appendChild(doc.createTextNode(valuesByPath.get(PATH_AUTHOR).getValue().toString()));
    project.appendChild(author);

    return doc;
  }

  protected void printPostComment(Map<String, ValueObject> valuesByPath) {

  }

  @Override
  public Object execute(Collection<ValueObject> values) {
    Map<String, ValueObject> valuesByPath = new HashMap<String, ValueObject>();
    for (ValueObject each : values) {
      String key = each.get(ValueObject.PROP_PATH).toString();
      valuesByPath.put(key, each);
    }

    try {
      Document doc = convertToDocument(valuesByPath);

      if (Constants.DEBUG) {
        // Use a Transformer for output
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();

        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(System.out);
        transformer.transform(source, result);
      }

      final JET2Context jet2Context = JET2Helpers.createJETContext(doc, null);
      final BufferedJET2Writer jet2Writer = JET2Helpers.createJETWriter();

      JET2TemplateManager.run(new String[] {"com.sneda.dev.helpers.jet"}, new JET2TemplateManager.ITemplateOperation() {

        @Override
        public void run(ITemplateRunner templateRunner) {
          templateRunner.generate("templates/main.jet2", jet2Context, jet2Writer);
        }
      });

      TransformContextExtender ex = (TransformContextExtender) jet2Context.getPrivateData(TransformContextExtender.class.getName());
      if (ex != null) {
        ex.commit(new NullProgressMonitor());
      }

      printPostComment(valuesByPath);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

    return null;
  }
}
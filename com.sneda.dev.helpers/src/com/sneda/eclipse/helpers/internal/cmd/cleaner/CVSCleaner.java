/*
 * Copyright SNEDA
 * (c) All rights reserved 2009.
 */
package com.sneda.eclipse.helpers.internal.cmd.cleaner;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.sneda.eclipse.helpers.internal.console.ConsoleSupport;
import com.sneda.eclipse.helpers.internal.util.FileReaderByLine;
import com.sneda.eclipse.helpers.internal.util.FileTools;
import com.sneda.eclipse.helpers.internal.util.FileWriterByLine;
import com.sneda.eclipse.helpers.internal.util.IProcessor;
import com.sneda.eclipse.helpers.internal.util.LineProcessor;

/**
 * @author nio
 */
public class CVSCleaner {

  public static final String INPUT_ROOTDIR = "R�pertoire racine"; //$NON-NLS-N$
  public static final String INPUT_APPLICATION = "Nom du projet 'serveur'"; //$NON-NLS-N$
  public static final String INPUT_PRESENTATION = "Nom du projet 'presentation'"; //$NON-NLS-N$
  public static final String INPUT_COMPONENTS = "Nom du projet 'services'"; //$NON-NLS-N$
  public static final String INPUT_TEST_INTEGRATION = "Nom du projet 'test integration'"; //$NON-NLS-N$
  public static final String INPUT_DRY_RUN = "Dry run"; //$NON-NLS-N$

  private static final char SEP = File.separatorChar;

  private static final String[] DIRS_WANTED = {SEP + ".settings" + SEP, SEP + "classes" + SEP, SEP + "target" + SEP,
      SEP + "bin" + SEP};

  // private static final String[] DIRS_WANTED_EXCLUDED = { "enums" + SEP + "src" + SEP + "main" +
  // SEP + "java" + SEP + "com" + SEP + "sneda" + SEP + "pgi"
  // + SEP + "crd" + SEP + "classes" };

  private static final String[] ENTRIES_UNWANTED = {"D/.settings////", "D/classes////", "D/target////", "D/bin////"};

  private static final String[] CVSIGNORE = {".settings", "classes", "target", "bin"};

  private final boolean dryRun;

  private final ConsoleSupport support = new ConsoleSupport();

  public CVSCleaner(boolean dryRun) {
    this.dryRun = dryRun;
  }

  /**
   * Remove CVS directories from DIRS_WANTED directories for the given full paths.
   */
  public void cleanDirCVS(String... fullPaths) {
    if (dryRun) {
      support.println("** You are in dry-run mode i.e. changes will not be performed **");
    }

    for (String eachFullPath : fullPaths) {
      File directory = new File(eachFullPath);
      if (directory.exists() == false) {
        support.println("Cannot find: " + directory.getPath());
        return;
      }

      support.println("> Looking for wrong CVS directories in " + directory.getName() + ". Please wait...");

      final List<File> dirs = new ArrayList<File>();
      FileTools.visitAllDirs(directory, new IProcessor<File, Boolean>() {

        public Boolean process(File fileOrDir) {
          // for (String each : DIRS_WANTED_EXCLUDED)
          // {
          // if (fileOrDir.getAbsolutePath().contains(each))
          // {
          // // excluded!
          // return false;
          // }
          // }
          for (String each : DIRS_WANTED) {
            if (fileOrDir.getAbsolutePath().contains(each) && "CVS".equals(fileOrDir.getName())) {
              return dirs.add(fileOrDir);
            }
          }
          return false;
        }
      });

      for (File each : dirs) {
        support.println("....deleting " + each.getAbsolutePath());
        if (!dryRun) {
          FileTools.deleteDir(each);
        }
      }

      support.println("Done!");
    }
  }

  /**
   * Clean CVS Entries files
   */
  public void cleanFileEntries(String... fullPaths) {
    if (dryRun) {
      System.out.println("** You are in dry-run mode ie. changes will not be performed **");
    }

    for (String eachFullPath : fullPaths) {
      File directory = new File(eachFullPath);
      if (directory.exists() == false) {
        support.println("Cannot find: " + directory.getPath());
        return;
      }

      support.println("> Looking for CVS Entries files in " + directory.getName() + ". Please wait...");

      final List<File> entries = new ArrayList<File>();
      FileTools.visitAllFiles(directory, new IProcessor<File, Boolean>() {

        public Boolean process(final File fileOrDir) {
          if ("Entries".equals(fileOrDir.getName())) {
            // Read file
            FileReaderByLine reader = new FileReaderByLine(fileOrDir, new LineProcessor() {

              public String process(String line) {
                for (String each : ENTRIES_UNWANTED) {
                  if (line.trim().equals(each)) {
                    entries.add(fileOrDir);
                    // break
                    return null;
                  }
                }
                return line;
              }
            });
            reader.execute();
          }
          return false;
        }
      });

      for (File each : entries) {
        support.println("....cleaning " + each.getAbsolutePath());
        if (!dryRun) {
          try {
            FileWriterByLine modifier = new FileWriterByLine(each, new LineProcessor() {

              public String process(String line) {
                for (String each : ENTRIES_UNWANTED) {
                  if (line.trim().equals(each)) {
                    return null;
                  }
                }
                return line;
              }
            });
            modifier.execute();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
      support.println("Done!");
    }
  }

  /**
   * Check .cvsignore files
   */
  public void checkIgnoreFiles(String... fullPaths) {
    if (dryRun) {
      System.out.println("** You are in dry-run mode ie. changes will not be performed **");
    }

    for (String eachFullPath : fullPaths) {
      File directory = new File(eachFullPath);
      if (directory.exists() == false) {
        support.println("Cannot find: " + directory.getPath());
        return;
      }

      support.println("> Looking for .cvsignore files in " + directory.getName() + ". Please wait...");

      final List<File> pomFiles = new ArrayList<File>();
      FileTools.visitAllFiles(directory, new IProcessor<File, Boolean>() {

        public Boolean process(File file) {
          if ("pom.xml".equals(file.getName())) {
            return pomFiles.add(file);
          }
          return true;
        }
      });
      for (File pom : pomFiles) {
        File cvsignore = new File(pom.getParent(), ".cvsignore");

        // Check existence
        if (!cvsignore.exists()) {
          support.println("....file: " + cvsignore.getAbsolutePath() + " should exist!");

          if (!dryRun) {
            try {
              cvsignore.createNewFile();
            } catch (IOException e) {
              e.printStackTrace();
              continue;
            }
          } else {
            continue;
          }
        }

        // Check content
        final Set<String> content = new HashSet<String>();
        FileReaderByLine reader = new FileReaderByLine(cvsignore, new LineProcessor() {

          public String process(String line) {
            content.add(line.trim());
            return line;
          }
        });
        reader.execute();
        for (String each : CVSIGNORE) {
          if (!content.contains(each)) {
            support.println("....file: " + cvsignore.getAbsolutePath() + " should contain '" + each + "' entry!");
            if (!dryRun) {
              try {
                FileTools.appendLine(cvsignore, each);
              } catch (IOException e) {
                e.printStackTrace();
                continue;
              }
            }
          }
        }
      }
      support.println("Done!");
    }
  }

  public void cleanIgnoreFiles(String... fullPaths) {
    for (String eachFullPath : fullPaths) {
      File directory = new File(eachFullPath);
      if (directory.exists() == false) {
        support.println("Cannot find: " + directory.getPath());
        return;
      }

      support.println("> Looking for .cvsignore files in " + directory.getName() + ". Please wait...");

      final List<File> files = new ArrayList<File>();
      FileTools.visitAllFiles(directory, new IProcessor<File, Boolean>() {

        public Boolean process(File file) {
          if (".cvsignore".equals(file.getName())) {
            return files.add(file);
          }
          return true;
        }
      });

      for (final File each : files) {
        if (!dryRun) {
          try {
            FileWriterByLine modifier = new FileWriterByLine(each, new LineProcessor() {

              public String process(String line) {
                if (line.trim().length() == 0) {
                  System.out.println("....file: " + each.getAbsolutePath() + " contains empty lines");
                  return null;
                }
                return line;
              }
            });
            modifier.execute();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
      support.println("Done!");
    }
  }

  public static void main(String[] args) {
    // Modifiez les valeurs de ces variables
    // N'oubliez pas les slashs finaux
    String root = "C:/DEVGI/nio/workspace-dev/";
    String application = root + "pgi-application/";
    String components = root + "pgi-components/";
    String presentation = root + "pgi-presentation/";
    boolean dryRun = false;
    // -------------------------------------

    if (dryRun) {
      System.out.println("** You are in dry-run mode ie. changes will not be performed");
    }

    new CVSCleaner(dryRun).cleanDirCVS(application, components, presentation);
    new CVSCleaner(dryRun).cleanFileEntries(application, components, presentation);

    // Attention avec extra car CVS trouve ensuite des changements sur tous les .cvsignore
    boolean extra = false;
    if (extra) {
      new CVSCleaner(dryRun).checkIgnoreFiles(application, components, presentation);
      new CVSCleaner(dryRun).cleanIgnoreFiles(application, components, presentation);
    }

    System.out.println();
    System.out.println("That's all folks!");
  }
}

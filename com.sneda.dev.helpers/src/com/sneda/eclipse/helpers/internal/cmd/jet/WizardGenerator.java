package com.sneda.eclipse.helpers.internal.cmd.jet;

import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;

/**
 * @author nio
 */
public class WizardGenerator extends AbstractJetGenerator {

  private static final String PATH_PACKAGE = "/input/elmt/wizard/package"; //$NON-NLS-1$
  private static final String PATH_NAME = "/input/elmt/wizard/names"; //$NON-NLS-1$
  private static final String PATH_NUMOFPAGES = "/input/elmt/wizard/numOfPages"; //$NON-NLS-1$
  private static final String PATH_ENTITY = "/input/elmt/wizard/entity"; //$NON-NLS-1$

  public static final String MSGE_NUMOFPAGES = Messages.WizardGenerator_KeyNumOfPage;
  public static final String MSGE_ENTITY = Messages.WizardGenerator_KeyEntity;

  @Override
  public Map<String, ValueObject> buildInput() {
    Map<String, ValueObject> inputs = super.buildInput();

    ValueObject vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_PACKAGE).put(ValueObject.PROP_HELP,
        Messages.AbstractGenerator_HelpPackage).put(ValueObject.PROP_PATH, PATH_PACKAGE);
    inputs.put(PATH_PACKAGE, vo);

    vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_NAME).put(ValueObject.PROP_HELP,
        Messages.AbstractGenerator_HelpName).put(ValueObject.PROP_PATH, PATH_NAME);
    inputs.put(PATH_NAME, vo);

    vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_NUMOFPAGES).put(ValueObject.PROP_HELP,
        Messages.WizardGenerator_HelpNumOfPages).put(ValueObject.PROP_PATH, PATH_NUMOFPAGES);
    inputs.put(PATH_NUMOFPAGES, vo);

    vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_ENTITY).put(ValueObject.PROP_HELP,
        Messages.WizardGenerator_HelpCreation).put(ValueObject.PROP_PATH, PATH_ENTITY);
    inputs.put(PATH_ENTITY, vo);

    return inputs;
  }

  @Override
  protected Document convertToDocument(Map<String, ValueObject> valuesByPath) throws ParserConfigurationException {
    Document doc = super.convertToDocument(valuesByPath);
    Node input = doc.getDocumentElement();

    Element elmt = doc.createElement("elmt");
    input.appendChild(elmt);

    Element wizard = doc.createElement("wizard");
    elmt.appendChild(wizard);

    Element pakage = doc.createElement("package");
    pakage.appendChild(doc.createTextNode(valuesByPath.get(PATH_PACKAGE).getValue().toString()));
    wizard.appendChild(pakage);

    Element name = doc.createElement("name");
    name.appendChild(doc.createTextNode(valuesByPath.get(PATH_NAME).getValue().toString()));
    wizard.appendChild(name);

    Element entity = doc.createElement("entity");
    if (Messages.WizardGenerator_Oui.equals(valuesByPath.get(PATH_ENTITY).getValue())) {
      entity.appendChild(doc.createTextNode(Boolean.TRUE.toString()));
    } else {
      entity.appendChild(doc.createTextNode(Boolean.FALSE.toString()));
    }
    wizard.appendChild(entity);

    // Manage pages
    ValueObject value = valuesByPath.get(PATH_NUMOFPAGES);
    if (value != null) {
      int n = Integer.parseInt((String) value.getValue());
      for (int i = 0; i < n; i++) {
        Element page = doc.createElement("page");
        wizard.appendChild(page);

        Element id = doc.createElement("id");
        id.appendChild(doc.createTextNode("" + (i + 1)));
        page.appendChild(id);
      }
    }

    return doc;
  }

  @Override
  protected void printPostComment(Map<String, ValueObject> valuesByPath) {
    getConsoleSupport().println(Messages.WizardGenerator_Stars);
    if (Messages.WizardGenerator_Oui.equals(valuesByPath.get(PATH_ENTITY))) {
      getConsoleSupport().println(Messages.WizardGenerator_Rem1);
    }
    if (Messages.WizardGenerator_Non.equals(valuesByPath.get(PATH_ENTITY))) {
      getConsoleSupport().println(Messages.WizardGenerator_Rem2);
    }
    getConsoleSupport().println(Messages.WizardGenerator_Stars);
  }
}

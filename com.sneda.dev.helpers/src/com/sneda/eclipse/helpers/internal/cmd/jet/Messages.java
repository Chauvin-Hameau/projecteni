package com.sneda.eclipse.helpers.internal.cmd.jet;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

  private static final String BUNDLE_NAME = "com.sneda.eclipse.helpers.internal.cmd.jet.messages"; //$NON-NLS-1$

  public static String AbstractGenerator_HelpName;
  public static String AbstractGenerator_HelpBundleName;
  public static String AbstractGenerator_HelpSrcFolder;
  public static String AbstractGenerator_HelpPackage;
  public static String AbstractGenerator_HelpAuthor;
  public static String AbstractGenerator_ExistingFile1;
  public static String AbstractGenerator_ExistingFile2;
  public static String AbstractGenerator_KeyBundleName;
  public static String AbstractGenerator_KeyHome;
  public static String AbstractGenerator_KeyPackage;
  public static String AbstractGenerator_KeySuffix;
  public static String AbstractGenerator_KeyUser;
  public static String AbstractGenerator_Running;
  public static String EditorGenerator_HelpNumOfTabs;
  public static String EditorGenerator_HelpIhmEtendue;
  public static String EditorGenerator_KeyNumOfTab;
  public static String EditorGenerator_KeyIhmEtendue;
  public static String EditorGenerator_Rem1;
  public static String EditorGenerator_Rem2;
  public static String EditorGenerator_Rem3;
  public static String EditorGenerator_Stars;
  public static String PartGenerator_Ctx1;
  public static String PartGenerator_Ctx2;
  public static String PartGenerator_Ctx3;
  public static String PartGenerator_Ctx4;
  public static String PartGenerator_Ctx5;
  public static String ViewGenerator_Rem1;
  public static String ViewGenerator_Stars;
  public static String WidgetContentProposalGenerator_nom_qualifie_AOE;

  public static String WidgetContentProposalGenerator_nom_qualifie_AOE_exemple;

  public static String WidgetContentProposalGenerator_partieVariable;

  public static String WidgetContentProposalGenerator_recuperer_le_reqAO;

  public static String WidgetLazyLoadingGenerator_aide_partie_variable;

  public static String WidgetLazyLoadingGenerator_nom_qualifie_aoe;

  public static String WidgetLazyLoadingGenerator_nom_qualifie_aoe_exemple;

  public static String WizardGenerator_Oui;
  public static String WizardGenerator_Non;
  public static String WizardGenerator_KeyNumOfPage;
  public static String WizardGenerator_KeyEntity;
  public static String WizardGenerator_HelpNumOfPages;
  public static String WizardGenerator_HelpCreation;
  public static String WizardGenerator_Stars;
  public static String WizardGenerator_Rem1;
  public static String WizardGenerator_Rem2;

  public static String TableGenerator_NamePageHelp;
  public static String TableGenerator_CreateColumnHelp;
  public static String TableGenerator_EditableColumnHelp;

  public static String TableGenerator_KeyNameOfPage;
  public static String TableGenerator_KeyCreateOfColumn;
  public static String TableGenerator_KeyEntityName;

  public static String UpdateTableGenerator_NamePageHelp;
  public static String UpdateTableGenerator_SettingsUpdateHelp;
  public static String UpdateTableGenerator_KeyNameOfPage;
  public static String UpdateTableGenerator_KeySettingsUpdate;

  static {
    // initialize resource bundle
    NLS.initializeMessages(BUNDLE_NAME, Messages.class);
  }

  private Messages() {}
}

package com.sneda.eclipse.helpers.internal.cmd.jet;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jet.BodyContentWriter;
import org.eclipse.jet.BufferedJET2Writer;
import org.eclipse.jet.JET2Context;
import org.eclipse.jet.transform.TransformContextExtender;

import com.sneda.dev.helpers.jet.compiled._jet_transformation;

public abstract class JET2Helpers {

  /**
   * Create a JET context suitable individual template execution
   * 
   * @param modelRoot the model root (corresponds to XPath /). May be <code>null</code>
   * @param variables predefined JET variables. May be <code>null</code>.
   * @return the JET execution context
   */
  public static JET2Context createJETContext(Object modelRoot, final Map<String, ?> variables) {

    Map<String, Object> copiedVariables = new HashMap<String, Object>(variables != null ? variables
        : Collections.<String, Object>emptyMap());

    // ensure that c:iterate can set the XPath context object
    copiedVariables.put("org.eclipse.jet.taglib.control.iterateSetsContext", Boolean.TRUE);

    final JET2Context context = new JET2Context(modelRoot, copiedVariables);
    TransformContextExtender ex = TransformContextExtender.getInstance(context);
    ex.setLoader(new _jet_transformation());

    return context;
  }

  /**
   * Create a writer suitable for individual template execution. Use
   * {@link BufferedJET2Writer#getContent()} to retrieve the writer's contents
   * 
   * @return a JET writer
   */
  public static BufferedJET2Writer createJETWriter() {
    return new BodyContentWriter();
  }

}

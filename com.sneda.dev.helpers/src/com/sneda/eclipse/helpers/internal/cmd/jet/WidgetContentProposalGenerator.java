package com.sneda.eclipse.helpers.internal.cmd.jet;

import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.sneda.eclipse.helpers.internal.cmd.ValueObject;

/**
 * @author daf
 */
public class WidgetContentProposalGenerator extends AbstractJetGenerator {

  private static final String PATH_PACKAGE = "/input/elmt/widgetContentProposal/package"; //$NON-NLS-1$
  private static final String PATH_NAME = "/input/elmt/widgetContentProposal/name"; //$NON-NLS-1$
  private static final String PATH_AOE_QN = "/input/elmt/widgetContentProposal/aoeQN"; //$NON-NLS-1$

  @Override
  public Map<String, ValueObject> buildInput() {
    Map<String, ValueObject> inputs = super.buildInput();

    ValueObject vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_PACKAGE).put(ValueObject.PROP_HELP,
        Messages.AbstractGenerator_HelpPackage).put(ValueObject.PROP_PATH, PATH_PACKAGE);
    inputs.put(PATH_PACKAGE, vo);

    vo = new ValueObject().put(ValueObject.PROP_MSGE, MSGE_NAME).put(ValueObject.PROP_HELP,
        Messages.WidgetContentProposalGenerator_partieVariable).put(ValueObject.PROP_PATH, PATH_NAME);
    inputs.put(PATH_NAME, vo);

    vo = new ValueObject().put(ValueObject.PROP_MSGE, Messages.WidgetContentProposalGenerator_nom_qualifie_AOE).put(
        ValueObject.PROP_HELP, Messages.WidgetContentProposalGenerator_nom_qualifie_AOE_exemple).put(
        ValueObject.PROP_PATH, PATH_AOE_QN);
    inputs.put(PATH_AOE_QN, vo);

    return inputs;
  }

  @Override
  protected Document convertToDocument(Map<String, ValueObject> valuesByPath) throws ParserConfigurationException {
    Document doc = super.convertToDocument(valuesByPath);
    Node input = doc.getDocumentElement();

    Element elmt = doc.createElement("elmt"); //$NON-NLS-1$
    input.appendChild(elmt);

    Element wizard = doc.createElement("widgetContentProposal"); //$NON-NLS-1$
    elmt.appendChild(wizard);

    Element pakage = doc.createElement("package"); //$NON-NLS-1$
    pakage.appendChild(doc.createTextNode(valuesByPath.get(PATH_PACKAGE).getValue().toString()));
    wizard.appendChild(pakage);

    Element name = doc.createElement("name"); //$NON-NLS-1$
    name.appendChild(doc.createTextNode(valuesByPath.get(PATH_NAME).getValue().toString()));
    wizard.appendChild(name);

    Element aoeQN = doc.createElement("aoeQN"); //$NON-NLS-1$
    Object aoeQnValue = valuesByPath.get(PATH_AOE_QN).getValue();
    aoeQN.appendChild(doc.createTextNode(aoeQnValue.toString()));
    wizard.appendChild(aoeQN);

    Element aoe = doc.createElement("aoe"); //$NON-NLS-1$
    String aoeClassName = aoeQnValue.toString().substring(aoeQnValue.toString().lastIndexOf('.') + 1);

    aoe.appendChild(doc.createTextNode(aoeClassName));
    wizard.appendChild(aoe);

    return doc;
  }

  @Override
  protected void printPostComment(Map<String, ValueObject> valuesByPath) {
    getConsoleSupport().println(Messages.WidgetContentProposalGenerator_recuperer_le_reqAO);
  }
}

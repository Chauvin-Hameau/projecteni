package com.sneda.eclipse.helpers.internal.lang;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

  private static final String BUNDLE_NAME = "com.sneda.eclipse.helpers.internal.lang.messages"; //$NON-NLS-1$
  public static String Constants_0;
  public static String Constants_1;
  static {
    // initialize resource bundle
    NLS.initializeMessages(BUNDLE_NAME, Messages.class);
  }

  private Messages() {}
}

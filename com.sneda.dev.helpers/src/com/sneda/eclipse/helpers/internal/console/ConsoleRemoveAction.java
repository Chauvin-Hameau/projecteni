/*
 * Copyright SNEDA
 * (c) All rights reserved 2009.
 */
package com.sneda.eclipse.helpers.internal.console;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.internal.console.ConsolePluginImages;
import org.eclipse.ui.internal.console.IInternalConsoleConstants;

import com.sneda.eclipse.helpers.console.SnedaOutputConsole;

/**
 * @author nio
 * 
 */
@SuppressWarnings("restriction")
public class ConsoleRemoveAction extends Action {

  public ConsoleRemoveAction() {
    setToolTipText("Close SNEDA Console");
    setDisabledImageDescriptor(ConsolePluginImages.getImageRegistry().getDescriptor(
        IInternalConsoleConstants.IMG_DLCL_CLOSE));
    setImageDescriptor(ConsolePluginImages.getImageRegistry().getDescriptor(IInternalConsoleConstants.IMG_ELCL_CLOSE));
  }

  @Override
  public void run() {
    IConsoleManager manager = ConsolePlugin.getDefault().getConsoleManager();
    IConsole snedaConsole = null;
    for (IConsole console : manager.getConsoles()) {
      if (SnedaOutputConsole.CONSOLE_ID.equals(console.getName())) {
        snedaConsole = console;
        break;
      }
    }
    if (snedaConsole != null) {
      manager.removeConsoles(new IConsole[] {snedaConsole});
    }
  }
}

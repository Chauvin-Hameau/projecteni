package com.sneda.eclipse.helpers.internal.console;

import java.io.IOException;
import java.io.PrintStream;

import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

import com.sneda.eclipse.helpers.internal.Activator;
import com.sneda.eclipse.helpers.internal.util.Constants;

/**
 * @author nio
 */
public class ConsoleSupport {

  public void touch() {
    MessageConsole console = Activator.getDefault().getConsole();
    console.activate();
  }

  private void doPrintln(final String message) {
    MessageConsole console = Activator.getDefault().getConsole();
    MessageConsoleStream stream = console.newMessageStream();
    stream.println(message);
    try {
      stream.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @SuppressWarnings("unused")
  public void debug(final String message) {
    if (!Constants.DEBUG) {
      return;
    }
    final String messageWithPRefix = "<DEBUG> " + message;
    Display.getDefault().asyncExec(new Runnable() {

      public void run() {
        doPrintln("<DEBUG> " + message);
      }
    });
  }

  public void println(final String message) {
    Display.getDefault().asyncExec(new Runnable() {

      public void run() {
        doPrintln(message);
      }
    });
  }

  private void doPrintStackTrace(final Throwable t) {
    MessageConsole console = Activator.getDefault().getConsole();
    MessageConsoleStream stream = console.newMessageStream();
    PrintStream pstream = new PrintStream(stream);
    t.printStackTrace(pstream);
    pstream.close();
  }

  public void printStackTrace(final Throwable t) {
    Display.getDefault().asyncExec(new Runnable() {

      public void run() {
        doPrintStackTrace(t);
      }
    });
  }
}

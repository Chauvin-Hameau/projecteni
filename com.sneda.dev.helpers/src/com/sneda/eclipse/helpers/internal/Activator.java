package com.sneda.eclipse.helpers.internal;

import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import com.sneda.eclipse.helpers.console.SnedaOutputConsole;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

  // The plug-in ID
  public static final String PLUGIN_ID = "com.sneda.eclipse.helper";

  // The shared instance
  private static Activator plugin;

  private SnedaOutputConsole console;

  private IResourceChangeListener listener = new PropathAutoBuilderListener();

  /*
   * (non-Javadoc)
   * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
   */
  @Override
  public void start(BundleContext context) throws Exception {
    super.start(context);
    plugin = this;
    console = new SnedaOutputConsole();

    // ICommandService cmdService = (ICommandService)
    // PlatformUI.getWorkbench().getService(ICommandService.class);
    // Command cmd = cmdService.getCommand("com.sneda.eclipse.helpers.commands.propathAutoRebuild");
    // State state = cmd.getState("org.eclipse.ui.commands.toggleState");
    // if (state.getValue() instanceof Boolean) {
    // if (true == (Boolean) state.getValue()) {
    // ResourcesPlugin.getWorkspace().addResourceChangeListener(listener);
    // }
    // }
  }

  /*
   * (non-Javadoc)
   * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
   */
  @Override
  public void stop(BundleContext context) throws Exception {
    plugin = null;
    try {
      if (console != null) {
        console.shutdown();
      }
      ResourcesPlugin.getWorkspace().removeResourceChangeListener(listener);
    }
    finally {
      super.stop(context);
    }
  }

  /**
   * Returns the shared instance
   * 
   * @return the shared instance
   */
  public static Activator getDefault() {
    return plugin;
  }

  /**
   * Returns an image descriptor for the image file at the given plug-in relative path
   * 
   * @param path the path
   * @return the image descriptor
   */
  public static ImageDescriptor getImageDescriptor(String path) {
    return imageDescriptorFromPlugin(PLUGIN_ID, path);
  }

  public SnedaOutputConsole getConsole() {
    return console;
  }

  public void enableProgressAutoBuilder(boolean b) {
    ResourcesPlugin.getWorkspace().removeResourceChangeListener(listener);
    if (b == true) {
      ResourcesPlugin.getWorkspace().addResourceChangeListener(listener);
    } else {

    }
  }
}

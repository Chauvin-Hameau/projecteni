package com.sneda.eclipse.helpers.tools;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class ReadModifyProperties {

	final Properties prop = new Properties();

	private InputStream input = null;
	private OutputStream output = null;

	private String inputFile;
	private String[] properties = null;
	private String propertie = null;

	/**
	 * <b>Constructeur 1</b>
	 * <p>
	 * Prend en compte que le chemin du fichier properties
	 * </p>
	 *
	 * @param inputFile
	 *            Chemin du fichier properties à lire
	 */
	public ReadModifyProperties(String inputFile) {
		setInputFile(inputFile);
	}

	/**
	 * <b>Constructeur 2</b>
	 * <p>
	 * Prend en compte le chemin du fichier properties et un tableau de properties
	 * </p>
	 *
	 * @param inputFile
	 *            Chemin du fichier properties à lire
	 * @param properties
	 *            Clé des différentes properties à lire
	 */
	public ReadModifyProperties(String inputFile, String[] properties) {
		setInputFile(inputFile);
		setProperties(properties);
	}

	public ReadModifyProperties(String inputFile, String propertie) {
		setInputFile(inputFile);
		setPropertie(propertie);
	}

	/**
	 * <b>readProperties()</b>
	 * <p>
	 * Permet de lire toutes les valeurs de propriété du fichier renseigné en
	 * fonction des valeur données dans le tableau de propriété
	 * </p>
	 */
	public void readProperties() {
		if (properties != null && properties.length > 0) {

			try {

				input = new FileInputStream(inputFile);
				prop.load(input);

				for (String currentPropertie : properties) {
					System.out.println(prop.getProperty(currentPropertie));
				}

			} catch (final IOException ex) {

			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (final IOException ex) {
						ex.printStackTrace();
					}
				}
			}
		}
	}

	public String readPropertie() {
		try {
			input = new FileInputStream(inputFile);
			prop.load(input);
			return prop.getProperty(propertie);
		} catch (final IOException ex) {

		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (final IOException ex) {
					ex.printStackTrace();
				}
			}
		}
		return null;
	}

	/**
	 * <b>addProperties(String keyProperties, String valueProperties)</b>
	 * <p>
	 * Permet d'ajouter une propriété et une valeur dans le fichier properties
	 * </p>
	 *
	 * @param keyProperties
	 *            Clé de la properties
	 * @param ValueProperties
	 *            Valeur associée à la clé de la properties
	 */
	public void addProperties(String keyProperties, String valueProperties) {
		try {
			output = new FileOutputStream(inputFile);

			prop.setProperty(keyProperties, valueProperties);

			prop.store(output, null);
		} catch (Exception io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * <b>addProperties(HashMap<String, String> properties)</b>
	 * <p>
	 * Permet d'ajouter plusieurs propriétés et leur valeur dans le fichier
	 * properties
	 * </p>
	 *
	 * @param properties
	 *            HashMap qui contient un ensemble de clé/Valeur correspondant à la
	 *            clé du propertie et sa valeur
	 */
	public void addProperties(HashMap<String, String> properties) {

		try {
			output = new FileOutputStream(inputFile);

			for (Map.Entry<String, String> entry : properties.entrySet()) {
				prop.setProperty(entry.getKey(), entry.getValue());
			}

			prop.store(output, null);
		} catch (Exception io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void usingBufferedWritter(String keyProperties, String valueProperties) throws IOException {
		prop.setProperty(keyProperties, valueProperties);
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(inputFile, true));
			writer.newLine(); // Add new line
			writer.write(keyProperties + "=" + valueProperties);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}

	public InputStream getInput() {
		return input;
	}

	public void setInput(InputStream input) {
		this.input = input;
	}

	public String getInputFile() {
		return inputFile;
	}

	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}

	public String[] getProperties() {
		return properties;
	}

	public void setProperties(String[] properties) {
		this.properties = properties;
	}

	public Properties getProp() {
		return prop;
	}

	public String getPropertie() {
		return propertie;
	}

	public void setPropertie(String propertie) {
		this.propertie = propertie;
	}

}

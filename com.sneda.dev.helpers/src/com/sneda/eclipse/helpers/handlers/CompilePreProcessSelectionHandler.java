package com.sneda.eclipse.helpers.handlers;

import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.internal.WorkbenchPage;
import org.eclipse.ui.part.FileEditorInput;

import com.sneda.eclipse.helpers.internal.console.ConsoleSupport;
import com.sneda.eclipse.helpers.internal.util.EclipsePluginTools;
import com.sneda.eclipse.helpers.internal.util.StringConstants;

@SuppressWarnings("restriction")
public class CompilePreProcessSelectionHandler extends AbstractHandler {

  private static final AtomicReference<Object> LOCK = new AtomicReference<Object>();

  private static final String PROJECT_DEV = StringConstants.PROJECT4DEV.asString();

  // TODO Linux !
  private static final String SCRIPT = "/script/compile.bat";
  private static final String POINTP = "/progress/compilePreProcess.p";
  private static final String TARGET = "/output/out.p";

  private final ConsoleSupport console = new ConsoleSupport();

  public Object execute(ExecutionEvent event) throws ExecutionException {
    console.touch();

    if (LOCK.get() != null) {
      // Already running, exit
      console.println("Already running...");
      return null;
    }

    IWorkspace workspace = ResourcesPlugin.getWorkspace();

    IProject devProject = workspace.getRoot().getProject(StringConstants.PROJECT4DEV.asString());
    if (devProject == null || !devProject.exists()) {
      console.println("Please, first open the project '" + StringConstants.PROJECT4DEV.asString()
          + "' in your workspace.");
      return null;
    }

    IResource resource = EclipsePluginTools.getCurrentSelection();
    if (resource == null || !resource.exists()) {
      console.println("Please, first select the file you want to compile.");
      return null;
    }

    try {
      LOCK.set(new Object());

      // Check target

      IFile output = workspace.getRoot().getFile(new Path(PROJECT_DEV + TARGET));
      if (output.exists()) {
        try {
          output.delete(true, null);
        } catch (CoreException e) {
          console.printStackTrace(e);
          return null;
        }
      }

      // Launch script

      String resourceSrcName = resource.getName();
      String resourceSrcLocation = resource.getLocation().toOSString();
      String resourceTargetLocation = output.getLocation().toOSString();
      String scriptFile = workspace.getRoot().getFile(new Path(PROJECT_DEV + SCRIPT)).getLocation().toOSString();
      String progressFile = workspace.getRoot().getFile(new Path(PROJECT_DEV + POINTP)).getLocation().toOSString();
      String projectLocation = resource.getProject().getLocation().toOSString();

      CommandLine commandLine = new CommandLine(scriptFile);
      commandLine.addArgument(resourceSrcName);
      commandLine.addArgument(resourceSrcLocation);
      commandLine.addArgument(progressFile);
      commandLine.addArgument(resourceTargetLocation);
      commandLine.addArgument(projectLocation);

      console.debug("Invoking script: " + scriptFile + " with the following arguments");
      console.debug("  - $1 " + resourceSrcName);
      console.debug("  - $2 " + resourceSrcLocation);
      console.debug("  - $3 " + progressFile);
      console.debug("  - $4 " + resourceTargetLocation);
      console.debug("  - $5 " + projectLocation);

      DefaultExecutor executor = new DefaultExecutor();
      Integer exitValue = null;
      try {
        exitValue = executor.execute(commandLine);
      } catch (Exception e) {
        console.printStackTrace(e);
        return null;
      }

      // Refresh and Open file

      try {
        console.debug("Refreshing WS");
        devProject.refreshLocal(2, null);
      } catch (CoreException e) {
        console.printStackTrace(e);
        return null;
      }

      if (exitValue != null && !executor.isFailure(exitValue)) {
        output = workspace.getRoot().getFile(new Path(PROJECT_DEV + TARGET));
        if (output == null || !output.exists()) {
          console.println("Cannot launch Editor: resource " + output.getLocation().toOSString() + " does not exist.");
          return null;
        }

        IEditorDescriptor preferredEditor = IDE.getDefaultEditor((IFile) resource);
        if (preferredEditor == null) {
          console.println("Unable to open default editor");
          return null;
        }

        IWorkbench wb = PlatformUI.getWorkbench();
        IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
        IWorkbenchPage page = win.getActivePage();
        try {
          console.debug("Opening editor from descriptor...");
          ((WorkbenchPage) page).openEditorFromDescriptor(new FileEditorInput(output), preferredEditor, true, null);
        } catch (PartInitException e) {
          console.printStackTrace(e);
          return null;
        }
      }
    }
    finally {
      LOCK.set(null);
    }

    return null;
  }
}
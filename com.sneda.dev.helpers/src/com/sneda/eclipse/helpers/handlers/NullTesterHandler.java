package com.sneda.eclipse.helpers.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.EditorPart;

import com.sneda.eclipse.helpers.internal.cmd.tester.NullTester;
import com.sneda.eclipse.helpers.internal.console.ConsoleSupport;
import com.sneda.eclipse.helpers.internal.util.Constants;

/**
 * @author nio
 */
public class NullTesterHandler extends AbstractHandler {

  private final ConsoleSupport console = new ConsoleSupport();

  public Object execute(ExecutionEvent event) throws ExecutionException {
    // Get current part
    IWorkbench wb = PlatformUI.getWorkbench();
    IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
    IWorkbenchPage page = win.getActivePage();
    IWorkbenchPart part = page.getActivePart();

    if (part instanceof EditorPart) {
      ISelection sel = ((EditorPart) part).getSite().getSelectionProvider().getSelection();
      if (sel instanceof TextSelection) {
        String str = ((TextSelection) sel).getText().trim();
        if (str != null && !str.isEmpty()) {
          String s = new NullTester().cutUp(str);
          console.println(s);
        } else {
          console.println(Constants.NULL_TESTER_INFO);
        }
      }
    } else {
      console.println(Constants.NULL_TESTER_INFO);
    }

    return null;
  }
}

/*
 * Copyright SNEDA
 * (c) All rights reserved 2009.
 */
package com.sneda.eclipse.helpers.handlers;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkingSet;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.wizards.datatransfer.ExternalProjectImportWizard;

import com.sneda.eclipse.helpers.internal.console.ConsoleSupport;
import com.sneda.eclipse.helpers.internal.util.EclipsePluginTools;

/**
 * @author nio
 */
public class RcpProjectsImporterHandler extends AbstractHandler {

  private final ConsoleSupport console = new ConsoleSupport();

  private static String getPrlProjectName() {
    IWorkspace workspace = ResourcesPlugin.getWorkspace();
    IProject project = workspace.getRoot().getProject("pgi-presentation");
    if (project.exists()) {
      return "pgi-presentation";
    }
    project = workspace.getRoot().getProject("pgi-presentation-parent");
    if (project.exists()) {
      return "pgi-presentation-parent";
    }
    IResource resource = EclipsePluginTools.getCurrentSelection();
    return (resource != null) ? resource.getName().trim() : null;
  }

  public Object execute(ExecutionEvent event) throws ExecutionException {
    // Get current part
    IWorkbench wb = PlatformUI.getWorkbench();
    IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
    IWorkbenchPage page = win.getActivePage();
    IWorkbenchPart part = page.getActivePart();

    try {
      // Create WSet if they don't exist
      if (wb.getWorkingSetManager().getWorkingSet("SgsServices") == null) {
        console.println("Creating WorkingSet: SgsServices");
        IWorkingSet workingSet = wb.getWorkingSetManager().createWorkingSet("SgsServices", new IAdaptable[0]);
        workingSet.setId("org.eclipse.jdt.ui.JavaWorkingSetPage");
        wb.getWorkingSetManager().addWorkingSet(workingSet);
      }
      if (wb.getWorkingSetManager().getWorkingSet("SgsClients") == null) {
        console.println("Creating WorkingSet: SgsClients");
        IWorkingSet workingSet = wb.getWorkingSetManager().createWorkingSet("SgsClients", new IAdaptable[0]);
        workingSet.setId("org.eclipse.jdt.ui.JavaWorkingSetPage");
        wb.getWorkingSetManager().addWorkingSet(workingSet);
      }

      IWorkspace workspace = ResourcesPlugin.getWorkspace();
      IProject project = workspace.getRoot().getProject(getPrlProjectName());

      IFile projectFile = project.getFile(".project");
      IFile projectOldFile = project.getFile("project.old");

      // Preliminaries
      if (!projectFile.exists() && projectOldFile.exists()) {
        IFile file = project.getFile("project.old");
        file.move(projectFile.getFullPath(), true, null);
        project.refreshLocal(IResource.DEPTH_ONE, null);
      }
      if (project.getFile("project.old").exists()) {
        project.getFile("project.old").delete(true, null);
        project.refreshLocal(IResource.DEPTH_ONE, null);
      }

      // Mv .project file in pgi-presentation
      IFile file = project.getFile(".project");
      // support.println("Moving " + file.getFullPath() + " to " + projectOldFile.getFullPath());
      file.move(projectOldFile.getFullPath(), true, null);

      // Refresh WS
      project.refreshLocal(IResource.DEPTH_ONE, null);

      // Launch Wizard
      String initPath = workspace.getRoot().getLocation().toOSString() + File.separator + getPrlProjectName();
      ExternalProjectImportWizard wizard = new ExternalProjectImportWizard(initPath);
      wizard.init(part.getSite().getWorkbenchWindow().getWorkbench(), null);

      // Instantiates the wizard container with the wizard and opens it
      WizardDialog dialog = new WizardDialog(PlatformUI.getWorkbench().getDisplay().getActiveShell(), wizard);
      dialog.create();
      dialog.open();

      // Refresh WS
      project.refreshLocal(IResource.DEPTH_ONE, null);

      // Mv back .project file
      if (project.getFile(".project").exists()) {
        project.getFile(".project").delete(true, null);
      }

      file = project.getFile("project.old");
      // support.println("Moving back " + file.getFullPath() + " to " + projectFile.getFullPath());
      file.move(projectFile.getFullPath(), true, null);

      // Refresh WS
      project.refreshLocal(IResource.DEPTH_ONE, null);
    } catch (CoreException e) {
      console.printStackTrace(e);
    }

    return null;
  }
}

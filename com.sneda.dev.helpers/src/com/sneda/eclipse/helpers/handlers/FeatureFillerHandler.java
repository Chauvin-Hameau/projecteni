package com.sneda.eclipse.helpers.handlers;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sneda.eclipse.helpers.internal.console.ConsoleSupport;
import com.sneda.eclipse.helpers.internal.util.Constants;
import com.sneda.eclipse.helpers.internal.util.EclipsePluginTools;
import com.sneda.eclipse.helpers.internal.util.FileWriterByLine;
import com.sneda.eclipse.helpers.internal.util.IProcessor;
import com.sneda.eclipse.helpers.internal.util.NioCopier;

/**
 * @deprecated
 */
@Deprecated
public class FeatureFillerHandler extends AbstractHandler {

  // Exclusions as part of the 'framework' feature
  private static final Set<String> EXCLUSIONS = new HashSet<String>();
  static {
    EXCLUSIONS.add("com.sneda.pgi.enums"); //$NON-NLS-N$
  }

  private static final String LS = System.getProperty("line.separator"); //$NON-NLS-N$

  private final ConsoleSupport support = new ConsoleSupport();

  public Object execute(ExecutionEvent event) throws ExecutionException {
    IResource resource = EclipsePluginTools.getCurrentSelection();
    if (resource == null) {
      support.println(Constants.FEATURE_FILLER_INFO);
      return null;
    }

    File file = new File(resource.getRawLocation().toOSString());
    if (!file.getName().equals("feature.xml")) { //$NON-NLS-N$
      support.println(Constants.FEATURE_FILLER_INFO);
      return null;
    }

    /*
     * Look for dependencies.
     */

    final Set<String> dependencies = new HashSet<String>();
    final Set<String> pluginsAlreadyThere = new HashSet<String>();

    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    DocumentBuilder db;
    Document doc;
    try {
      db = dbf.newDocumentBuilder();
      doc = db.parse(file);
    } catch (Exception e) {
      support.printStackTrace(e);
      return null;
    }

    doc.getDocumentElement().normalize();

    NodeList nodeImportLst = doc.getElementsByTagName("import"); //$NON-NLS-N$
    for (int s = 0; s < nodeImportLst.getLength(); s++) {
      Node fstNode = nodeImportLst.item(s);
      if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
        Element fstElmnt = (Element) fstNode;
        String str = fstElmnt.getAttribute("plugin"); //$NON-NLS-N$
        if (str != null && !str.trim().isEmpty()) {
          dependencies.add(str);
        }
      }
    }

    NodeList nodePluginLst = doc.getElementsByTagName("plugin"); //$NON-NLS-N$
    for (int s = 0; s < nodePluginLst.getLength(); s++) {
      Node fstNode = nodePluginLst.item(s);
      if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
        Element fstElmnt = (Element) fstNode;
        String str = fstElmnt.getAttribute("id"); //$NON-NLS-N$
        if (str != null && !str.trim().isEmpty()) {
          pluginsAlreadyThere.add(str);
        }
      }
    }

    final Set<String> plugins = new HashSet<String>();

    for (String each : dependencies) {
      if (each.startsWith("com.sneda.pgi")) { //$NON-NLS-N$
        if (!EXCLUSIONS.contains(each)) {
          if (!pluginsAlreadyThere.contains(each)) {
            plugins.add(each);
          } else {
            support.println("Plugin " + each + " already there, ignore it"); //$NON-NLS-N$
          }
        }
      }
    }

    /*
     * Transform dependencies as packaging plugins.
     */

    File destFile = new File(resource.getRawLocation().toOSString() + "~"); //$NON-NLS-N$
    if (destFile.exists()) {
      destFile.delete();
    }

    FileWriterByLine writer = new FileWriterByLine(file, destFile, new IProcessor<String, String>() {

      public String process(String line) {
        if (line.trim().equals("</feature>")) { //$NON-NLS-N$
          // Append plugins
          StringBuilder sb = new StringBuilder();
          for (String each : plugins) {
            support.println("Add plugin: " + each); //$NON-NLS-N$

            sb.append(LS);
            sb.append("   <plugin").append(LS); //$NON-NLS-N$
            sb.append("         id=\"" + each + "\"").append(LS); //$NON-NLS-N$
            sb.append("         download-size=\"0\"").append(LS); //$NON-NLS-N$
            sb.append("         install-size=\"0\"").append(LS); //$NON-NLS-N$
            sb.append("         version=\"0.0.0\"").append(LS); //$NON-NLS-N$
            sb.append("         unpack=\"false\"/>").append(LS); //$NON-NLS-N$
          }
          sb.append(LS);
          sb.append("</feature>"); //$NON-NLS-N$
          return sb.toString();
        }
        return line;
      }
    });

    try {
      writer.execute();
    } catch (IOException e) {
      support.printStackTrace(e);
      return null;
    }

    /*
     * Refresh feature.xml file
     */

    try {
      String oldName = getOldNameForFeature(file);
      new NioCopier().copy(file, new File(file.getParentFile(), oldName));
      file.delete();

      new NioCopier().copy(destFile, new File(file.getParentFile(), "feature.xml")); //$NON-NLS-N$
      destFile.delete();

      IWorkspace workspace = ResourcesPlugin.getWorkspace();
      IProject project = workspace.getRoot().getProject(resource.getParent().getName());
      project.refreshLocal(IResource.DEPTH_ONE, null);
    } catch (Exception e) {
      support.printStackTrace(e);
      return null;
    }

    support.println("Done for " + resource.getParent().getName() + ", " + plugins.size() + " plugins added"); //$NON-NLS-N$

    return null;
  }

  private String getOldNameForFeature(File feature) {
    int cpt = 1;
    while (new File(feature.getParentFile(), "feature.xml.old" + cpt).exists()) { //$NON-NLS-N$
      cpt++;
    }
    return "feature.xml.old" + cpt; //$NON-NLS-N$
  }
}

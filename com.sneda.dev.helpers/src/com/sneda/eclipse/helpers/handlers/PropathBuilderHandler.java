package com.sneda.eclipse.helpers.handlers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.sneda.eclipse.helpers.internal.console.ConsoleSupport;
import com.sneda.eclipse.helpers.internal.util.StringConstants;
import com.sneda.eclipse.helpers.internal.util.Tools;

public class PropathBuilderHandler extends AbstractHandler {

  private static final AtomicReference<Object> LOCK = new AtomicReference<Object>();

  private static final String SEP = File.separator;
  private static final String PROJECT_DEV = StringConstants.PROJECT4DEV.asString();
  private static final String TARGET = "/output/ws.propath";

  private static final Pattern PATTERN = Pattern.compile(".*\\.p|.*\\.i");

  private static final String[] PGMX_PATHS = new String[] { // br
  "pgmx" + SEP + "inv" + SEP + "commun" + SEP + "inclv", // br
      "pgmx" + SEP + "commun" + SEP + "inclv", // br
      "pgmx" + SEP + "inv" + SEP + "commun" + SEP + "incli", // br
      "pgmx" + SEP + "commun" + SEP + "incli", // br
      "pgmx" + SEP + "inv" + SEP + "commun" + SEP + "gpar", // br
      "pgmx" + SEP + "commun" + SEP + "gpar", // br
      "pgmx" + SEP + "inv" + SEP + "commun" + SEP + "outils", // br
      "pgmx" + SEP + "commun" + SEP + "outils", // br
      "pgmx" + SEP + "commun" + SEP + "imprim", // br
      "pgmx" + SEP + "commun" + SEP + "iloc", // br
      "pgmx" + SEP + "inv" + SEP + "commun" + SEP + "iloc" // br
  };

  private final ConsoleSupport console = new ConsoleSupport();

  private Collection<IProject> getL4gProjects(IProject[] projects) {
    Collection<IProject> out = new ArrayList<IProject>(20);

    for (IProject project : projects) {
      if (!project.isOpen()) {
        continue;
      }
      IFile file = project.getFile("pom.xml");
      if (!file.exists()) {
        continue;
      }

      InputStream is = null;
      try {
        is = file.getContents();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(is);

        Element rootElement = document.getDocumentElement();
        NodeList list = rootElement.getElementsByTagName("packaging");
        if (list.getLength() > 0) {
          if ("oear".equals(list.item(0).getTextContent())) {
            console.debug("Found L4G project: " + project.getName());
            out.add(project);
          }
        }
      } catch (Exception e) {
        console.printStackTrace(e);
      }
      finally {
        Tools.silentlyClose(is);
      }
    }

    return out;
  }

  private Collection<IProject> getEstiaProjects(IProject[] projects) {
    Collection<IProject> out = new ArrayList<IProject>(20);

    for (IProject project : projects) {
      if (!project.isOpen()) {
        continue;
      }
      IFile pomFile = project.getFile("pom.xml");
      if (pomFile.exists()) {
        // Estia projects do not have pom file
        continue;
      }
      IFile projectFile = project.getFile(".project");
      if (!projectFile.exists()) {
        continue;
      }

      InputStream is = null;
      try {
        is = projectFile.getContents();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(is);

        Element rootElement = document.getDocumentElement();
        NodeList list = rootElement.getElementsByTagName("nature");
        for (int i = 0; i < list.getLength(); i++) {
          if ("com.openedge.pdt.text.progressNature".equals(list.item(i).getTextContent())) {
            console.debug("Found Estia project: " + project.getName());
            out.add(project);
          }
        }
      } catch (Exception e) {
        console.printStackTrace(e);
      }
      finally {
        Tools.silentlyClose(is);
      }
    }

    return out;
  }

  private String getSrcPathForL4g(IProject project) {
    return "src" + SEP + "main" + SEP + "progress";
  }

  private String getSrcPathForEstia(IProject project) {
    String str = project.getLocation().toOSString();
    if (new File(str + SEP + "src").exists()) {
      // Only flat structures for Estia but should be breadth first
      String srcPath = Tools.getDepthFirstFileMatching(new File(str + SEP + "src"), PATTERN).getAbsolutePath();
      if (srcPath != null) {
        String parent = new File(srcPath).getParentFile().getAbsolutePath();
        return parent.substring(new File(str).getAbsolutePath().length() + 1);
      }
    }
    return null;
  }

  private boolean containsFile(File file) {
    for (File f : file.listFiles()) {
      if (!f.isDirectory()) {
        return true;
      }
    }
    return false;
  }

  private List<String> getEstiaFullPaths() {
    File rootPath = null;

    String variable = System.getenv("SNEDA_USER_NETDIR");
    if (variable != null) {
      // Use it!
      if (!new File(variable).exists()) {
        console.println("Please set the variable SNEDA_USER_NETDIR in order to scan directory");
        return Collections.emptyList();
      }
      rootPath = new File(variable, "cvs" + SEP + "estia");
      if (!rootPath.exists()) {
        console.println("Oops, directory: " + rootPath.getAbsolutePath() + " does not exist!");
        return Collections.emptyList();
      }
    } else {
      rootPath = new File("T:" + SEP + "cvs" + SEP + "estia");
      if (!rootPath.exists()) {
        console.println("Oops, directory: " + rootPath.getAbsolutePath() + " does not exist!");
        return Collections.emptyList();
      }
    }

    console.debug("Using rootpath: " + rootPath.getAbsolutePath());

    List<String> out = new ArrayList<String>();
    for (String each : PGMX_PATHS) {
      File f = new File(rootPath, each);
      if (f.exists()) {
        if (containsFile(f)) {
          out.add(f.getAbsolutePath());
        }
      }
    }
    return out;
  }

  public Object execute(ExecutionEvent event) throws ExecutionException {
    console.touch();

    if (LOCK.get() != null) {
      // Already running, exit
      console.println("Already running...");
      return null;
    }

    IWorkspace workspace = ResourcesPlugin.getWorkspace();

    IProject devProject = workspace.getRoot().getProject(PROJECT_DEV);
    if (devProject == null || !devProject.exists()) {
      console.println("Please, first open the project '" + PROJECT_DEV + "' in your workspace.");
      return null;
    }

    try {
      LOCK.set(new Object());

      String path = PROJECT_DEV + TARGET;
      IFile output = workspace.getRoot().getFile(new Path(path));
      // if (output.exists()) {
      // console.debug("Deleting " + output.getLocation().toOSString());
      // try {
      // output.delete(true, null);
      // } catch (CoreException e) {
      // console.printStackTrace(e);
      // return null;
      // }
      // }

      String outputStr = output.getLocation().toOSString();
      console.debug("Generating file: " + outputStr);
      FileOutputStream fout = null;
      try {
        fout = new FileOutputStream(outputStr);
        PrintStream ps = new PrintStream(fout);

        for (IProject l4gProject : getL4gProjects(workspace.getRoot().getProjects())) {
          String srcPath = getSrcPathForL4g(l4gProject);
          if (srcPath == null) {
            console.println("Cannot find any source for prject: " + l4gProject.getLocation().toOSString());
            continue;
          }
          String projectLoc = l4gProject.getLocation().toOSString();
          String line = projectLoc + SEP + srcPath;
          console.debug("Adding line to propath file: " + line);
          ps.println(line);
        }

        for (IProject estiaProject : getEstiaProjects(workspace.getRoot().getProjects())) {
          String srcPath = getSrcPathForEstia(estiaProject);
          if (srcPath == null) {
            console.println("Cannot find any source for prject: " + estiaProject.getLocation().toOSString());
            continue;
          }
          String projectLoc = estiaProject.getLocation().toOSString();
          String line = projectLoc + SEP + srcPath;
          console.debug("Adding line to propath file: " + line);
          ps.println(line);
        }

        for (String line : getEstiaFullPaths()) {
          console.debug("Adding line to propath file: " + line);
          ps.println(line);
        }

      } catch (IOException e) {
        console.printStackTrace(e);
        return null;
      }
      finally {
        Tools.silentlyClose(fout);
      }

      try {
        console.debug("Refreshing WS");
        devProject.refreshLocal(2, null);
      } catch (CoreException e) {
        console.printStackTrace(e);
        return null;
      }

      console.println("Generated file: " + outputStr);
    }
    finally {
      LOCK.set(null);
    }

    return null;
  }
}

package com.sneda.eclipse.helpers.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.MenuItem;

import com.sneda.eclipse.helpers.internal.Activator;
import com.sneda.eclipse.helpers.internal.console.ConsoleSupport;

public class PropathAutoBuilderHandler extends AbstractHandler {

  private final ConsoleSupport console = new ConsoleSupport();

  public Object execute(ExecutionEvent event) throws ExecutionException {
    if (event.getTrigger() instanceof Event) {
      if (((Event) event.getTrigger()).widget instanceof MenuItem) {
        // To handle persistent state between sessions
        // boolean oldValue = HandlerUtil.toggleCommandState(event.getCommand());

        MenuItem item = (MenuItem) ((Event) event.getTrigger()).widget;
        boolean oldValue = !item.getSelection();

        String msge = !oldValue ? "Enabling " : "Disabling ";
        msge = msge + "auto rebuild for propath.";
        console.debug(msge);

        Activator.getDefault().enableProgressAutoBuilder(!oldValue);
      }
    }

    return null;
  }
}

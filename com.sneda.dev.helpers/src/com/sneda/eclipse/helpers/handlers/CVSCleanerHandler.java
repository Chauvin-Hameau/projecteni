package com.sneda.eclipse.helpers.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.sneda.eclipse.helpers.internal.util.DialogTools;
import com.sneda.eclipse.helpers.internal.wizzard.impl.CVSCleanerWizard;

/**
 * @author nio
 */
public class CVSCleanerHandler extends AbstractHandler {

  public Object execute(ExecutionEvent event) throws ExecutionException {
    // Get current part
    IWorkbench wb = PlatformUI.getWorkbench();
    IWorkbenchWindow win = wb.getActiveWorkbenchWindow();

    // Instantiates and initializes the wizard
    CVSCleanerWizard wizard = new CVSCleanerWizard();
    // wizard.init(part.getSite().getWorkbenchWindow().getWorkbench(), null);

    // Instantiates the wizard container with the wizard and opens it
    WizardDialog dialog = new WizardDialog(win.getShell(), wizard);
    DialogTools.createAndOpen(dialog);

    return null;
  }
}

package com.sneda.dev.helpers.jet.compiled;

import org.eclipse.jet.JET2Context;
import org.eclipse.jet.JET2Template;
import org.eclipse.jet.JET2Writer;
import org.eclipse.jet.taglib.RuntimeTagElement;
import org.eclipse.jet.taglib.TagInfo;

public class _jet_TableauModifiableMyNameColumnEnumjava implements JET2Template {
    private static final String _jetns_c = "org.eclipse.jet.controlTags"; //$NON-NLS-1$

    public _jet_TableauModifiableMyNameColumnEnumjava() {
        super();
    }

    private static final String NL = System.getProperty("line.separator"); //$NON-NLS-1$
    
    private static final TagInfo _td_c_get_1_9 = new TagInfo("c:get", //$NON-NLS-1$
            1, 9,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/package", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_4_12 = new TagInfo("c:get", //$NON-NLS-1$
            4, 12,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "/input/project/author", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_10_21 = new TagInfo("c:get", //$NON-NLS-1$
            10, 21,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_iterate_11_4 = new TagInfo("c:iterate", //$NON-NLS-1$
            11, 4,
            new String[] {
                "select", //$NON-NLS-1$
                "var", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/columnTable", //$NON-NLS-1$
                "columnTable", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_12_5 = new TagInfo("c:get", //$NON-NLS-1$
            12, 5,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/nameColumn", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_13_10 = new TagInfo("c:get", //$NON-NLS-1$
            13, 10,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/nameColumn", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_14_10 = new TagInfo("c:get", //$NON-NLS-1$
            14, 10,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/nameColumn", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_21_18 = new TagInfo("c:get", //$NON-NLS-1$
            21, 18,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );

    public void generate(final JET2Context context, final JET2Writer __out) {
        JET2Writer out = __out;
        out.write("package ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_1_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_1_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_1_9.setRuntimeParent(null);
        _jettag_c_get_1_9.setTagInfo(_td_c_get_1_9);
        _jettag_c_get_1_9.doStart(context, out);
        _jettag_c_get_1_9.doEnd();
        out.write(";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("/**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * @author ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_4_12 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_4_12); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_4_12.setRuntimeParent(null);
        _jettag_c_get_4_12.setTagInfo(_td_c_get_4_12);
        _jettag_c_get_4_12.doStart(context, out);
        _jettag_c_get_4_12.doEnd();
        out.write(NL);         
        out.write(" *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" *\tCette page contient tous les noms et tooltips de chaque colonnes.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write(" public enum Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_10_21 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_10_21); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_10_21.setRuntimeParent(null);
        _jettag_c_get_10_21.setTagInfo(_td_c_get_10_21);
        _jettag_c_get_10_21.doStart(context, out);
        _jettag_c_get_10_21.doEnd();
        out.write("ColumnEnum {");  //$NON-NLS-1$        
        out.write(NL);         
        RuntimeTagElement _jettag_c_iterate_11_4 = context.getTagFactory().createRuntimeTag(_jetns_c, "iterate", "c:iterate", _td_c_iterate_11_4); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_iterate_11_4.setRuntimeParent(null);
        _jettag_c_iterate_11_4.setTagInfo(_td_c_iterate_11_4);
        _jettag_c_iterate_11_4.doStart(context, out);
        while (_jettag_c_iterate_11_4.okToProcessBody()) {
            out.write("  \t\t");  //$NON-NLS-1$        
            RuntimeTagElement _jettag_c_get_12_5 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_12_5); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_get_12_5.setRuntimeParent(_jettag_c_iterate_11_4);
            _jettag_c_get_12_5.setTagInfo(_td_c_get_12_5);
            _jettag_c_get_12_5.doStart(context, out);
            _jettag_c_get_12_5.doEnd();
            out.write("(");  //$NON-NLS-1$        
            out.write(NL);         
            out.write("      \t\t\"");  //$NON-NLS-1$        
            RuntimeTagElement _jettag_c_get_13_10 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_13_10); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_get_13_10.setRuntimeParent(_jettag_c_iterate_11_4);
            _jettag_c_get_13_10.setTagInfo(_td_c_get_13_10);
            _jettag_c_get_13_10.doStart(context, out);
            _jettag_c_get_13_10.doEnd();
            out.write("\",");  //$NON-NLS-1$        
            out.write(NL);         
            out.write("      \t\t\"");  //$NON-NLS-1$        
            RuntimeTagElement _jettag_c_get_14_10 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_14_10); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_get_14_10.setRuntimeParent(_jettag_c_iterate_11_4);
            _jettag_c_get_14_10.setTagInfo(_td_c_get_14_10);
            _jettag_c_get_14_10.doStart(context, out);
            _jettag_c_get_14_10.doEnd();
            out.write("\"),");  //$NON-NLS-1$        
            out.write(NL);         
            _jettag_c_iterate_11_4.handleBodyContent(out);
        }
        _jettag_c_iterate_11_4.doEnd();
        out.write(";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  private String libelle;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  private String tooltipText;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  private Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_21_18 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_21_18); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_21_18.setRuntimeParent(null);
        _jettag_c_get_21_18.setTagInfo(_td_c_get_21_18);
        _jettag_c_get_21_18.doStart(context, out);
        _jettag_c_get_21_18.doEnd();
        out.write("ColumnEnum(String libelle, String tooltipText) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    this.libelle = libelle;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    this.tooltipText = tooltipText;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  public String getLibelle() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return libelle;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  public String getTooltipText() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return tooltipText;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("}");  //$NON-NLS-1$        
    }
}

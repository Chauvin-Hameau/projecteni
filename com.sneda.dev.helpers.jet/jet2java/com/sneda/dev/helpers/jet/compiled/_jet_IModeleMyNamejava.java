package com.sneda.dev.helpers.jet.compiled;

import org.eclipse.jet.JET2Context;
import org.eclipse.jet.JET2Template;
import org.eclipse.jet.JET2Writer;
import org.eclipse.jet.taglib.RuntimeTagElement;
import org.eclipse.jet.taglib.TagInfo;

public class _jet_IModeleMyNamejava implements JET2Template {
    private static final String _jetns_c = "org.eclipse.jet.controlTags"; //$NON-NLS-1$

    public _jet_IModeleMyNamejava() {
        super();
    }

    private static final String NL = System.getProperty("line.separator"); //$NON-NLS-1$
    
    private static final TagInfo _td_c_get_1_9 = new TagInfo("c:get", //$NON-NLS-1$
            1, 9,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currPart/package", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_6_12 = new TagInfo("c:get", //$NON-NLS-1$
            6, 12,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "/input/project/author", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_8_25 = new TagInfo("c:get", //$NON-NLS-1$
            8, 25,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currPart/name", //$NON-NLS-1$
            } );

    public void generate(final JET2Context context, final JET2Writer __out) {
        JET2Writer out = __out;
        out.write("package ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_1_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_1_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_1_9.setRuntimeParent(null);
        _jettag_c_get_1_9.setTagInfo(_td_c_get_1_9);
        _jettag_c_get_1_9.doStart(context, out);
        _jettag_c_get_1_9.doEnd();
        out.write(";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("/**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * Le modèle du Part.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * @author ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_6_12 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_6_12); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_6_12.setRuntimeParent(null);
        _jettag_c_get_6_12.setTagInfo(_td_c_get_6_12);
        _jettag_c_get_6_12.doStart(context, out);
        _jettag_c_get_6_12.doEnd();
        out.write(NL);         
        out.write(" */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("public interface IModele");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_8_25 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_8_25); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_8_25.setRuntimeParent(null);
        _jettag_c_get_8_25.setTagInfo(_td_c_get_8_25);
        _jettag_c_get_8_25.doStart(context, out);
        _jettag_c_get_8_25.doEnd();
        out.write(" {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("}");  //$NON-NLS-1$        
        out.write(NL);         
    }
}

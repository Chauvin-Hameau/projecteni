package com.sneda.dev.helpers.jet.compiled;

import org.eclipse.jet.JET2Context;
import org.eclipse.jet.JET2Template;
import org.eclipse.jet.JET2Writer;
import org.eclipse.jet.taglib.RuntimeTagElement;
import org.eclipse.jet.taglib.TagInfo;

public class _jet_MyNameDAOjava implements JET2Template {
    private static final String _jetns_c = "org.eclipse.jet.controlTags"; //$NON-NLS-1$

    public _jet_MyNameDAOjava() {
        super();
    }

    private static final String NL = System.getProperty("line.separator"); //$NON-NLS-1$
    
    private static final TagInfo _td_c_get_1_9 = new TagInfo("c:get", //$NON-NLS-1$
            1, 9,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/packageCheminDAO", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_17_12 = new TagInfo("c:get", //$NON-NLS-1$
            17, 12,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "/input/project/author", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_20_14 = new TagInfo("c:get", //$NON-NLS-1$
            20, 14,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_21_14 = new TagInfo("c:get", //$NON-NLS-1$
            21, 14,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_21_71 = new TagInfo("c:get", //$NON-NLS-1$
            21, 71,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_23_17 = new TagInfo("c:get", //$NON-NLS-1$
            23, 17,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_32_33 = new TagInfo("c:get", //$NON-NLS-1$
            32, 33,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_41_21 = new TagInfo("c:get", //$NON-NLS-1$
            41, 21,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_50_21 = new TagInfo("c:get", //$NON-NLS-1$
            50, 21,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_54_22 = new TagInfo("c:get", //$NON-NLS-1$
            54, 22,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_56_44 = new TagInfo("c:get", //$NON-NLS-1$
            56, 44,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_59_13 = new TagInfo("c:get", //$NON-NLS-1$
            59, 13,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_67_15 = new TagInfo("c:get", //$NON-NLS-1$
            67, 15,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_77_21 = new TagInfo("c:get", //$NON-NLS-1$
            77, 21,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_86_10 = new TagInfo("c:get", //$NON-NLS-1$
            86, 10,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_98_21 = new TagInfo("c:get", //$NON-NLS-1$
            98, 21,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_99_18 = new TagInfo("c:get", //$NON-NLS-1$
            99, 18,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_100_42 = new TagInfo("c:get", //$NON-NLS-1$
            100, 42,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_110_16 = new TagInfo("c:get", //$NON-NLS-1$
            110, 16,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_120_36 = new TagInfo("c:get", //$NON-NLS-1$
            120, 36,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );

    public void generate(final JET2Context context, final JET2Writer __out) {
        JET2Writer out = __out;
        out.write("package ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_1_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_1_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_1_9.setRuntimeParent(null);
        _jettag_c_get_1_9.setTagInfo(_td_c_get_1_9);
        _jettag_c_get_1_9.doStart(context, out);
        _jettag_c_get_1_9.doEnd();
        out.write(";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import java.util.Collection;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import java.util.Collections;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import java.util.List;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import org.springframework.stereotype.Service;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import com.sneda.applicativearchitecture.domains.IFiltreEnum;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.appserver.core.AppServerTemplate;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.appserver.core.ProDataGraphCreator;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.appserver.datasource.InteractionFactoryUtils;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.appserver.datasource.InteractionSpec;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.pgi.bpr.bsl.dao.intf.INMCDao;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("/**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * @author ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_17_12 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_17_12); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_17_12.setRuntimeParent(null);
        _jettag_c_get_17_12.setTagInfo(_td_c_get_17_12);
        _jettag_c_get_17_12.doStart(context, out);
        _jettag_c_get_17_12.doEnd();
        out.write(NL);         
        out.write(" *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("@Service(\"nmc");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_20_14 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_20_14); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_20_14.setRuntimeParent(null);
        _jettag_c_get_20_14.setTagInfo(_td_c_get_20_14);
        _jettag_c_get_20_14.doStart(context, out);
        _jettag_c_get_20_14.doEnd();
        out.write("\")");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("public class ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_21_14 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_21_14); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_21_14.setRuntimeParent(null);
        _jettag_c_get_21_14.setTagInfo(_td_c_get_21_14);
        _jettag_c_get_21_14.doStart(context, out);
        _jettag_c_get_21_14.doEnd();
        out.write("DAO implements INMCDao<");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_21_71 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_21_71); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_21_71.setRuntimeParent(null);
        _jettag_c_get_21_71.setTagInfo(_td_c_get_21_71);
        _jettag_c_get_21_71.doStart(context, out);
        _jettag_c_get_21_71.doEnd();
        out.write("> {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  private Class<");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_23_17 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_23_17); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_23_17.setRuntimeParent(null);
        _jettag_c_get_23_17.setTagInfo(_td_c_get_23_17);
        _jettag_c_get_23_17.doStart(context, out);
        _jettag_c_get_23_17.doEnd();
        out.write("> nmcClass;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  private static final String PATH = \"ptm/nomenclatures/\";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  // TODO à compléter avec le nom du fichier progress");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  private static final InteractionSpec chargerNomenclatureSpec = InteractionFactoryUtils.getInteractionSpec(");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      PATH.concat(\"charger.p\"), \"charger\");");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  //TODO à compléter avec le nom du fichier progress");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  private static final InteractionSpec creerOuModifierNomenclatureSpec = InteractionFactoryUtils.getInteractionSpec(");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      PATH.concat(\"creerModifier.p\"), \"creerModifier\");");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  public void setNmcClass(Class<");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_32_33 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_32_33); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_32_33.setRuntimeParent(null);
        _jettag_c_get_32_33.setTagInfo(_td_c_get_32_33);
        _jettag_c_get_32_33.doStart(context, out);
        _jettag_c_get_32_33.doEnd();
        out.write("> nmcClass) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    this.nmcClass = nmcClass;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  /*");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * (non-Javadoc)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @see com.sneda.pgi.bpr.bsl.dao.intf.INMCDao#charger()");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public Collection<");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_41_21 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_41_21); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_41_21.setRuntimeParent(null);
        _jettag_c_get_41_21.setTagInfo(_td_c_get_41_21);
        _jettag_c_get_41_21.doStart(context, out);
        _jettag_c_get_41_21.doEnd();
        out.write("> charger() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return Collections.emptyList();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  /*");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * (non-Javadoc)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @see com.sneda.pgi.bpr.bsl.dao.intf.INMCDao#charger(java.lang.Object)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public Collection<");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_50_21 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_50_21); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_50_21.setRuntimeParent(null);
        _jettag_c_get_50_21.setTagInfo(_td_c_get_50_21);
        _jettag_c_get_50_21.doStart(context, out);
        _jettag_c_get_50_21.doEnd();
        out.write("> charger(Object template) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return chargerNMC(template, false);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  private Collection<");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_54_22 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_54_22); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_54_22.setRuntimeParent(null);
        _jettag_c_get_54_22.setTagInfo(_td_c_get_54_22);
        _jettag_c_get_54_22.doStart(context, out);
        _jettag_c_get_54_22.doEnd();
        out.write("> chargerNMC(Object template, boolean chargerTout) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    // Définir le prodatagraph qui mappe les parametres reçus en entree");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    ProDataGraphCreator inputCreator = new ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_56_44 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_56_44); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_56_44.setRuntimeParent(null);
        _jettag_c_get_56_44.setTagInfo(_td_c_get_56_44);
        _jettag_c_get_56_44.doStart(context, out);
        _jettag_c_get_56_44.doEnd();
        out.write("InputCreatorCharger(true);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("    return ((AppServerTemplate) template).queryForList(chargerNomenclatureSpec, inputCreator,");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        new ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_59_13 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_59_13); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_59_13.setRuntimeParent(null);
        _jettag_c_get_59_13.setTagInfo(_td_c_get_59_13);
        _jettag_c_get_59_13.doStart(context, out);
        _jettag_c_get_59_13.doEnd();
        out.write("NMCMapper());");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  /*");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * (non-Javadoc)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @see com.sneda.pgi.bpr.bsl.dao.intf.INMCDao#chargerParCritere(java.lang.Object, java.util.List)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public List<");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_67_15 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_67_15); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_67_15.setRuntimeParent(null);
        _jettag_c_get_67_15.setTagInfo(_td_c_get_67_15);
        _jettag_c_get_67_15.doStart(context, out);
        _jettag_c_get_67_15.doEnd();
        out.write("> chargerParCritere(Object arg0, List<IFiltreEnum> arg1) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("    return Collections.emptyList();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  /*");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * (non-Javadoc)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @see com.sneda.pgi.bpr.bsl.dao.intf.INMCDao#chargerTout(java.lang.Object)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public Collection<");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_77_21 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_77_21); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_77_21.setRuntimeParent(null);
        _jettag_c_get_77_21.setTagInfo(_td_c_get_77_21);
        _jettag_c_get_77_21.doStart(context, out);
        _jettag_c_get_77_21.doEnd();
        out.write("> chargerTout(Object template) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return chargerNMC(template, true);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  /*");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * (non-Javadoc)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @see com.sneda.pgi.bpr.bsl.dao.intf.INMCDao#chargerParCode(java.lang.Object, java.lang.String)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_86_10 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_86_10); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_86_10.setRuntimeParent(null);
        _jettag_c_get_86_10.setTagInfo(_td_c_get_86_10);
        _jettag_c_get_86_10.doStart(context, out);
        _jettag_c_get_86_10.doEnd();
        out.write(" chargerParCode(Object appServerTemplate, String code) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    // TODO Auto-generated method stub");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        out.write("  /*");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * (non-Javadoc)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @see com.sneda.pgi.bpr.bsl.dao.intf.INMCDao#creerOuModifier(java.util.Collection,");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * java.lang.Object)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public Collection<");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_98_21 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_98_21); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_98_21.setRuntimeParent(null);
        _jettag_c_get_98_21.setTagInfo(_td_c_get_98_21);
        _jettag_c_get_98_21.doStart(context, out);
        _jettag_c_get_98_21.doEnd();
        out.write("> creerOuModifier(");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      Collection<");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_99_18 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_99_18); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_99_18.setRuntimeParent(null);
        _jettag_c_get_99_18.setTagInfo(_td_c_get_99_18);
        _jettag_c_get_99_18.doStart(context, out);
        _jettag_c_get_99_18.doEnd();
        out.write("> listNomenclature, Object template) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    ProDataGraphCreator pdgCreator = new ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_100_42 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_100_42); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_100_42.setRuntimeParent(null);
        _jettag_c_get_100_42.setTagInfo(_td_c_get_100_42);
        _jettag_c_get_100_42.doStart(context, out);
        _jettag_c_get_100_42.doEnd();
        out.write("InputCreatorCreerModifier(listNomenclature);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    ((AppServerTemplate) template).execute(creerOuModifierNomenclatureSpec, pdgCreator);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return charger(template);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  /*");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * (non-Javadoc)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @see com.sneda.pgi.bpr.bsl.dao.intf.INMCDao#getNMCClass()");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public Class<");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_110_16 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_110_16); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_110_16.setRuntimeParent(null);
        _jettag_c_get_110_16.setTagInfo(_td_c_get_110_16);
        _jettag_c_get_110_16.doStart(context, out);
        _jettag_c_get_110_16.doEnd();
        out.write("> getNMCClass() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("    return nmcClass;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  /*");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * (non-Javadoc)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @see com.sneda.pgi.bpr.bsl.dao.intf.INMCDao#supprimer(java.util.Collection, java.lang.Object)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public void supprimer(Collection<");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_120_36 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_120_36); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_120_36.setRuntimeParent(null);
        _jettag_c_get_120_36.setTagInfo(_td_c_get_120_36);
        _jettag_c_get_120_36.doStart(context, out);
        _jettag_c_get_120_36.doEnd();
        out.write("> listASupprimer, Object template) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    // nothing");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("}");  //$NON-NLS-1$        
        out.write(NL);         
    }
}

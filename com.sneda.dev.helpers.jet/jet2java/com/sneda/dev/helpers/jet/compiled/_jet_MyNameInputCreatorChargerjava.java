package com.sneda.dev.helpers.jet.compiled;

import org.eclipse.jet.JET2Context;
import org.eclipse.jet.JET2Template;
import org.eclipse.jet.JET2Writer;
import org.eclipse.jet.taglib.RuntimeTagElement;
import org.eclipse.jet.taglib.TagInfo;

public class _jet_MyNameInputCreatorChargerjava implements JET2Template {
    private static final String _jetns_c = "org.eclipse.jet.controlTags"; //$NON-NLS-1$

    public _jet_MyNameInputCreatorChargerjava() {
        super();
    }

    private static final String NL = System.getProperty("line.separator"); //$NON-NLS-1$
    
    private static final TagInfo _td_c_get_1_9 = new TagInfo("c:get", //$NON-NLS-1$
            1, 9,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/packageCheminInputCreatorCharger", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_11_12 = new TagInfo("c:get", //$NON-NLS-1$
            11, 12,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "/input/project/author", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_14_14 = new TagInfo("c:get", //$NON-NLS-1$
            14, 14,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_18_10 = new TagInfo("c:get", //$NON-NLS-1$
            18, 10,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_24_76 = new TagInfo("c:get", //$NON-NLS-1$
            24, 76,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_32_37 = new TagInfo("c:get", //$NON-NLS-1$
            32, 37,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );

    public void generate(final JET2Context context, final JET2Writer __out) {
        JET2Writer out = __out;
        out.write("package ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_1_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_1_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_1_9.setRuntimeParent(null);
        _jettag_c_get_1_9.setTagInfo(_td_c_get_1_9);
        _jettag_c_get_1_9.doStart(context, out);
        _jettag_c_get_1_9.doEnd();
        out.write(";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import com.progress.common.exception.ProException;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.progress.open4gl.ProDataGraph;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.progress.open4gl.ProDataGraphMetaData;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.appserver.core.ProDataGraphCreator;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.appserver.core.ProDataGraphMetaDataCreator;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.appserver.util.ProDataGraphParameterUtils;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("/**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * @author ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_11_12 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_11_12); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_11_12.setRuntimeParent(null);
        _jettag_c_get_11_12.setTagInfo(_td_c_get_11_12);
        _jettag_c_get_11_12.doStart(context, out);
        _jettag_c_get_11_12.doEnd();
        out.write(NL);         
        out.write(" *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("public class ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_14_14 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_14_14); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_14_14.setRuntimeParent(null);
        _jettag_c_get_14_14.setTagInfo(_td_c_get_14_14);
        _jettag_c_get_14_14.doStart(context, out);
        _jettag_c_get_14_14.doEnd();
        out.write("InputCreatorCharger implements ProDataGraphCreator {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  private boolean chargerTout = false;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  public ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_18_10 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_18_10); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_18_10.setRuntimeParent(null);
        _jettag_c_get_18_10.setTagInfo(_td_c_get_18_10);
        _jettag_c_get_18_10.doStart(context, out);
        _jettag_c_get_18_10.doEnd();
        out.write("InputCreatorCharger(boolean chargerTout) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    this.chargerTout = chargerTout;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public ProDataGraph createProDataGraph() throws Exception {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    ProDataGraph dataGraph = ProDataGraphParameterUtils.createProDataGraph(");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_24_76 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_24_76); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_24_76.setRuntimeParent(null);
        _jettag_c_get_24_76.setTagInfo(_td_c_get_24_76);
        _jettag_c_get_24_76.doStart(context, out);
        _jettag_c_get_24_76.doEnd();
        out.write("InputMDCreator);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("    if (chargerTout) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      ProDataGraphParameterUtils.addBooleanParameter(dataGraph, \"ChargerTout\", true);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return dataGraph;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  final ProDataGraphMetaDataCreator ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_32_37 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_32_37); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_32_37.setRuntimeParent(null);
        _jettag_c_get_32_37.setTagInfo(_td_c_get_32_37);
        _jettag_c_get_32_37.doStart(context, out);
        _jettag_c_get_32_37.doEnd();
        out.write("InputMDCreator = new ProDataGraphMetaDataCreator() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("    @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    public ProDataGraphMetaData createProDataGraphMetaData() throws ProException {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      return ProDataGraphParameterUtils.getProDataGraphMetaDataForStandardParameters();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  };");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("}");  //$NON-NLS-1$        
        out.write(NL);         
    }
}

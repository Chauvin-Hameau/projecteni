package com.sneda.dev.helpers.jet.compiled;

import org.eclipse.jet.JET2Context;
import org.eclipse.jet.JET2Template;
import org.eclipse.jet.JET2Writer;
import org.eclipse.jet.taglib.RuntimeTagElement;
import org.eclipse.jet.taglib.TagInfo;

public class _jet_TableauMyNameStyledLabelProviderjava implements JET2Template {
    private static final String _jetns_c = "org.eclipse.jet.controlTags"; //$NON-NLS-1$

    public _jet_TableauMyNameStyledLabelProviderjava() {
        super();
    }

    private static final String NL = System.getProperty("line.separator"); //$NON-NLS-1$
    
    private static final TagInfo _td_c_get_1_9 = new TagInfo("c:get", //$NON-NLS-1$
            1, 9,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currTable/package", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_22_12 = new TagInfo("c:get", //$NON-NLS-1$
            22, 12,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "/input/project/author", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_25_21 = new TagInfo("c:get", //$NON-NLS-1$
            25, 21,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_28_24 = new TagInfo("c:get", //$NON-NLS-1$
            28, 24,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_30_17 = new TagInfo("c:get", //$NON-NLS-1$
            30, 17,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_30_77 = new TagInfo("c:get", //$NON-NLS-1$
            30, 77,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_iterate_48_9 = new TagInfo("c:iterate", //$NON-NLS-1$
            48, 9,
            new String[] {
                "select", //$NON-NLS-1$
                "var", //$NON-NLS-1$
            },
            new String[] {
                "$currTable/columnTable", //$NON-NLS-1$
                "columnTable", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_49_14 = new TagInfo("c:get", //$NON-NLS-1$
            49, 14,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/nameColumn", //$NON-NLS-1$
            } );

    public void generate(final JET2Context context, final JET2Writer __out) {
        JET2Writer out = __out;
        out.write("package ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_1_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_1_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_1_9.setRuntimeParent(null);
        _jettag_c_get_1_9.setTagInfo(_td_c_get_1_9);
        _jettag_c_get_1_9.doStart(context, out);
        _jettag_c_get_1_9.doEnd();
        out.write(";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import org.apache.commons.lang3.StringUtils;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.resource.ImageDescriptor;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.viewers.StyledCellLabelProvider;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.viewers.ViewerCell;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.swt.graphics.Font;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.swt.graphics.Image;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.swt.graphics.RGB;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.swt.graphics.Rectangle;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.swt.widgets.Event;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.swt.widgets.TableItem;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import com.sneda.toolkit.external.ISharedColorsToolKit;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.external.ISharedImagesToolKit;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.external.SharedColorsToolKit;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.external.SharedImagesToolKit;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.table.ISpreadsheetExportProvider;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.updatableviewer.UpdatableItem;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("/**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * @author ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_22_12 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_22_12); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_22_12.setRuntimeParent(null);
        _jettag_c_get_22_12.setTagInfo(_td_c_get_22_12);
        _jettag_c_get_22_12.doStart(context, out);
        _jettag_c_get_22_12.doEnd();
        out.write(NL);         
        out.write(" *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("public class Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_25_21 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_25_21); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_25_21.setRuntimeParent(null);
        _jettag_c_get_25_21.setTagInfo(_td_c_get_25_21);
        _jettag_c_get_25_21.doStart(context, out);
        _jettag_c_get_25_21.doEnd();
        out.write("StyledLabelProvider extends StyledCellLabelProvider implements");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    ISpreadsheetExportProvider {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  private final Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_28_24 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_28_24); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_28_24.setRuntimeParent(null);
        _jettag_c_get_28_24.setTagInfo(_td_c_get_28_24);
        _jettag_c_get_28_24.doStart(context, out);
        _jettag_c_get_28_24.doEnd();
        out.write("ColumnEnum colonne;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  public Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_30_17 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_30_17); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_30_17.setRuntimeParent(null);
        _jettag_c_get_30_17.setTagInfo(_td_c_get_30_17);
        _jettag_c_get_30_17.doStart(context, out);
        _jettag_c_get_30_17.doEnd();
        out.write("StyledLabelProvider(Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_30_77 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_30_77); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_30_77.setRuntimeParent(null);
        _jettag_c_get_30_77.setTagInfo(_td_c_get_30_77);
        _jettag_c_get_30_77.doStart(context, out);
        _jettag_c_get_30_77.doEnd();
        out.write("ColumnEnum colonne) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    super();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    this.colonne = colonne;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public Object getValue(Object element, int columnIndex) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    String text = StringUtils.EMPTY;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return text;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public void update(ViewerCell cell) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    if (cell != null && cell.getElement() != null) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      String texte = StringUtils.EMPTY;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      //TODO : Créer une condition pour vérifier que cell.getElement() est bien une instance de votre objet. Cf WIKI");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        switch (colonne) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        //TODO Le texte est la valeur que la colonne doit afficher");  //$NON-NLS-1$        
        out.write(NL);         
        RuntimeTagElement _jettag_c_iterate_48_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "iterate", "c:iterate", _td_c_iterate_48_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_iterate_48_9.setRuntimeParent(null);
        _jettag_c_iterate_48_9.setTagInfo(_td_c_iterate_48_9);
        _jettag_c_iterate_48_9.doStart(context, out);
        while (_jettag_c_iterate_48_9.okToProcessBody()) {
            out.write("\t      \tcase ");  //$NON-NLS-1$        
            RuntimeTagElement _jettag_c_get_49_14 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_49_14); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_get_49_14.setRuntimeParent(_jettag_c_iterate_48_9);
            _jettag_c_get_49_14.setTagInfo(_td_c_get_49_14);
            _jettag_c_get_49_14.doStart(context, out);
            _jettag_c_get_49_14.doEnd();
            out.write(":");  //$NON-NLS-1$        
            out.write(NL);         
            out.write("                texte = ;");  //$NON-NLS-1$        
            out.write(NL);         
            out.write("            break;");  //$NON-NLS-1$        
            out.write(NL);         
            _jettag_c_iterate_48_9.handleBodyContent(out);
        }
        _jettag_c_iterate_48_9.doEnd();
        out.write("          default:");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("            break;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      cell.setImage();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      cell.setText(texte);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        out.write("  /**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * Affiche l'image au sein de la cellule");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @param event");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @param image //");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  protected void afficheImage(Event event, Image image) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    if (image != null) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      Rectangle bounds = ((TableItem) event.item).getBounds(event.index);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      Rectangle imgBounds = image.getBounds();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      bounds.width /= 2;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      bounds.width -= imgBounds.width / 2;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      bounds.height /= 2;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      bounds.height -= imgBounds.height / 2;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("      int x = bounds.width > 0 ? bounds.x + bounds.width : bounds.x;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      int y = bounds.height > 0 ? bounds.y + bounds.height : bounds.y;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("      event.gc.drawImage(image, x, y - 1);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  protected void paint(Event event, Object element) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    Image image = null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public EDataType getDataType(Object element, int columnIndex) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public String getDataFormat(Object element, int columnIndex) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public Font getFont(Object element, int columnIndex) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public RGB getBackground(Object element, int columnIndex) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public RGB getForeground(Object element, int columnIndex) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("}");  //$NON-NLS-1$        
    }
}

package com.sneda.dev.helpers.jet.compiled;

import org.eclipse.jet.JET2Context;
import org.eclipse.jet.JET2Template;
import org.eclipse.jet.JET2Writer;
import org.eclipse.jet.taglib.RuntimeTagElement;
import org.eclipse.jet.taglib.TagInfo;

public class _jet_TableauModifiableMyNameEditingSupportjava implements JET2Template {
    private static final String _jetns_c = "org.eclipse.jet.controlTags"; //$NON-NLS-1$

    public _jet_TableauModifiableMyNameEditingSupportjava() {
        super();
    }

    private static final String NL = System.getProperty("line.separator"); //$NON-NLS-1$
    
    private static final TagInfo _td_c_get_1_9 = new TagInfo("c:get", //$NON-NLS-1$
            1, 9,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/package", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_23_12 = new TagInfo("c:get", //$NON-NLS-1$
            23, 12,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "/input/project/author", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_26_21 = new TagInfo("c:get", //$NON-NLS-1$
            26, 21,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_32_24 = new TagInfo("c:get", //$NON-NLS-1$
            32, 24,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_43_17 = new TagInfo("c:get", //$NON-NLS-1$
            43, 17,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_44_14 = new TagInfo("c:get", //$NON-NLS-1$
            44, 14,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_iterate_64_5 = new TagInfo("c:iterate", //$NON-NLS-1$
            64, 5,
            new String[] {
                "select", //$NON-NLS-1$
                "var", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/columnTable", //$NON-NLS-1$
                "columnTable", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_65_7 = new TagInfo("c:get", //$NON-NLS-1$
            65, 7,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/nameColumn", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_iterate_83_4 = new TagInfo("c:iterate", //$NON-NLS-1$
            83, 4,
            new String[] {
                "select", //$NON-NLS-1$
                "var", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/columnTable", //$NON-NLS-1$
                "columnTable", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_84_7 = new TagInfo("c:get", //$NON-NLS-1$
            84, 7,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/nameColumn", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_choose_85_2 = new TagInfo("c:choose", //$NON-NLS-1$
            85, 2,
            new String[] {
            },
            new String[] {
            } );
    private static final TagInfo _td_c_when_86_3 = new TagInfo("c:when", //$NON-NLS-1$
            86, 3,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/editableColumn = 'true'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_otherwise_89_3 = new TagInfo("c:otherwise", //$NON-NLS-1$
            89, 3,
            new String[] {
            },
            new String[] {
            } );
    private static final TagInfo _td_c_get_113_37 = new TagInfo("c:get", //$NON-NLS-1$
            113, 37,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_114_9 = new TagInfo("c:get", //$NON-NLS-1$
            114, 9,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_114_51 = new TagInfo("c:get", //$NON-NLS-1$
            114, 51,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_114_96 = new TagInfo("c:get", //$NON-NLS-1$
            114, 96,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_iterate_117_9 = new TagInfo("c:iterate", //$NON-NLS-1$
            117, 9,
            new String[] {
                "select", //$NON-NLS-1$
                "var", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/columnTable", //$NON-NLS-1$
                "columnTable", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_118_17 = new TagInfo("c:get", //$NON-NLS-1$
            118, 17,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/nameColumn", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_141_37 = new TagInfo("c:get", //$NON-NLS-1$
            141, 37,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_142_9 = new TagInfo("c:get", //$NON-NLS-1$
            142, 9,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_142_51 = new TagInfo("c:get", //$NON-NLS-1$
            142, 51,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_142_96 = new TagInfo("c:get", //$NON-NLS-1$
            142, 96,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_iterate_145_9 = new TagInfo("c:iterate", //$NON-NLS-1$
            145, 9,
            new String[] {
                "select", //$NON-NLS-1$
                "var", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/columnTable", //$NON-NLS-1$
                "columnTable", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_146_17 = new TagInfo("c:get", //$NON-NLS-1$
            146, 17,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/nameColumn", //$NON-NLS-1$
            } );

    public void generate(final JET2Context context, final JET2Writer __out) {
        JET2Writer out = __out;
        out.write("package ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_1_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_1_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_1_9.setRuntimeParent(null);
        _jettag_c_get_1_9.setTagInfo(_td_c_get_1_9);
        _jettag_c_get_1_9.doStart(context, out);
        _jettag_c_get_1_9.doEnd();
        out.write(";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import org.apache.commons.lang3.ObjectUtils;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.apache.commons.lang3.StringUtils;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.viewers.CellEditor;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.viewers.ColumnViewer;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.viewers.EditingSupport;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.viewers.LabelProvider;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.viewers.StructuredSelection;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.viewers.TableViewer;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.swt.widgets.Text;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.celleditor.TextCellEditor;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.updatableviewer.UpdatableItem;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.factory.CellEditorsFactory;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.updatableviewer.UpdatableItem;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("/**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * <p>");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * Permet de gerer les cellules modifiables du tableau modifiable.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * </p>");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * @author ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_23_12 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_23_12); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_23_12.setRuntimeParent(null);
        _jettag_c_get_23_12.setTagInfo(_td_c_get_23_12);
        _jettag_c_get_23_12.doStart(context, out);
        _jettag_c_get_23_12.doEnd();
        out.write(NL);         
        out.write(" *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("public class Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_26_21 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_26_21); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_26_21.setRuntimeParent(null);
        _jettag_c_get_26_21.setTagInfo(_td_c_get_26_21);
        _jettag_c_get_26_21.doStart(context, out);
        _jettag_c_get_26_21.doEnd();
        out.write("EditingSupport extends EditingSupport {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  private CellEditor cellEditor;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  private final TableViewer tableau;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  private final Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_32_24 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_32_24); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_32_24.setRuntimeParent(null);
        _jettag_c_get_32_24.setTagInfo(_td_c_get_32_24);
        _jettag_c_get_32_24.doStart(context, out);
        _jettag_c_get_32_24.doEnd();
        out.write("ColumnEnum column;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write(" /**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * <p>");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * Constructeur.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * </p>");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @param viewer Instance de colunmViewer");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @param controleur Le controleur du use case");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @param column La colonne à gérer.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_43_17 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_43_17); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_43_17.setRuntimeParent(null);
        _jettag_c_get_43_17.setTagInfo(_td_c_get_43_17);
        _jettag_c_get_43_17.doStart(context, out);
        _jettag_c_get_43_17.doEnd();
        out.write("EditingSupport(ColumnViewer viewer,");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_44_14 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_44_14); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_44_14.setRuntimeParent(null);
        _jettag_c_get_44_14.setTagInfo(_td_c_get_44_14);
        _jettag_c_get_44_14.doStart(context, out);
        _jettag_c_get_44_14.doEnd();
        out.write("ColumnEnum column) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    super(viewer);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    this.column = column;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    this.tableau = (TableViewer) viewer;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        out.write(" /**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * <p>");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * Méthode permettant de définir le type de composant contenu dans la cellule du tableau");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * modifiable en cas de modification.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * </p>");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @param element Cellule du tableau.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @return Type du composant.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  protected CellEditor getCellEditor(Object element) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  switch (column){");  //$NON-NLS-1$        
        out.write(NL);         
        RuntimeTagElement _jettag_c_iterate_64_5 = context.getTagFactory().createRuntimeTag(_jetns_c, "iterate", "c:iterate", _td_c_iterate_64_5); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_iterate_64_5.setRuntimeParent(null);
        _jettag_c_iterate_64_5.setTagInfo(_td_c_iterate_64_5);
        _jettag_c_iterate_64_5.doStart(context, out);
        while (_jettag_c_iterate_64_5.okToProcessBody()) {
            out.write("\tcase ");  //$NON-NLS-1$        
            RuntimeTagElement _jettag_c_get_65_7 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_65_7); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_get_65_7.setRuntimeParent(_jettag_c_iterate_64_5);
            _jettag_c_get_65_7.setTagInfo(_td_c_get_65_7);
            _jettag_c_get_65_7.doStart(context, out);
            _jettag_c_get_65_7.doEnd();
            out.write(":");  //$NON-NLS-1$        
            out.write(NL);         
            out.write("\t\treturn new TextCellEditor(tableau.getTable());");  //$NON-NLS-1$        
            out.write(NL);         
            _jettag_c_iterate_64_5.handleBodyContent(out);
        }
        _jettag_c_iterate_64_5.doEnd();
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  return cellEditor;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write(" /**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * <p>");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * Méthode permettant de savoir si la cellule est éditable.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * </p>");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @param element Cellule à modifier.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @return Cellule modifiable ?");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  protected boolean canEdit(Object element) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  switch (column){");  //$NON-NLS-1$        
        out.write(NL);         
        RuntimeTagElement _jettag_c_iterate_83_4 = context.getTagFactory().createRuntimeTag(_jetns_c, "iterate", "c:iterate", _td_c_iterate_83_4); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_iterate_83_4.setRuntimeParent(null);
        _jettag_c_iterate_83_4.setTagInfo(_td_c_iterate_83_4);
        _jettag_c_iterate_83_4.doStart(context, out);
        while (_jettag_c_iterate_83_4.okToProcessBody()) {
            out.write("\tcase ");  //$NON-NLS-1$        
            RuntimeTagElement _jettag_c_get_84_7 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_84_7); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_get_84_7.setRuntimeParent(_jettag_c_iterate_83_4);
            _jettag_c_get_84_7.setTagInfo(_td_c_get_84_7);
            _jettag_c_get_84_7.doStart(context, out);
            _jettag_c_get_84_7.doEnd();
            out.write(":");  //$NON-NLS-1$        
            out.write(NL);         
            RuntimeTagElement _jettag_c_choose_85_2 = context.getTagFactory().createRuntimeTag(_jetns_c, "choose", "c:choose", _td_c_choose_85_2); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_choose_85_2.setRuntimeParent(_jettag_c_iterate_83_4);
            _jettag_c_choose_85_2.setTagInfo(_td_c_choose_85_2);
            _jettag_c_choose_85_2.doStart(context, out);
            JET2Writer _jettag_c_choose_85_2_saved_out = out;
            while (_jettag_c_choose_85_2.okToProcessBody()) {
                out = out.newNestedContentWriter();
                RuntimeTagElement _jettag_c_when_86_3 = context.getTagFactory().createRuntimeTag(_jetns_c, "when", "c:when", _td_c_when_86_3); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_when_86_3.setRuntimeParent(_jettag_c_choose_85_2);
                _jettag_c_when_86_3.setTagInfo(_td_c_when_86_3);
                _jettag_c_when_86_3.doStart(context, out);
                JET2Writer _jettag_c_when_86_3_saved_out = out;
                while (_jettag_c_when_86_3.okToProcessBody()) {
                    out = out.newNestedContentWriter();
                    out.write("\t\t    return true;");  //$NON-NLS-1$        
                    out.write(NL);         
                    _jettag_c_when_86_3.handleBodyContent(out);
                }
                out = _jettag_c_when_86_3_saved_out;
                _jettag_c_when_86_3.doEnd();
                RuntimeTagElement _jettag_c_otherwise_89_3 = context.getTagFactory().createRuntimeTag(_jetns_c, "otherwise", "c:otherwise", _td_c_otherwise_89_3); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_otherwise_89_3.setRuntimeParent(_jettag_c_choose_85_2);
                _jettag_c_otherwise_89_3.setTagInfo(_td_c_otherwise_89_3);
                _jettag_c_otherwise_89_3.doStart(context, out);
                JET2Writer _jettag_c_otherwise_89_3_saved_out = out;
                while (_jettag_c_otherwise_89_3.okToProcessBody()) {
                    out = out.newNestedContentWriter();
                    out.write("\t        return false;");  //$NON-NLS-1$        
                    out.write(NL);         
                    _jettag_c_otherwise_89_3.handleBodyContent(out);
                }
                out = _jettag_c_otherwise_89_3_saved_out;
                _jettag_c_otherwise_89_3.doEnd();
                _jettag_c_choose_85_2.handleBodyContent(out);
            }
            out = _jettag_c_choose_85_2_saved_out;
            _jettag_c_choose_85_2.doEnd();
            _jettag_c_iterate_83_4.handleBodyContent(out);
        }
        _jettag_c_iterate_83_4.doEnd();
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  return false;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write(" /**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * <p>");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * Méthode permettant d'initialiser la cellule lors du focus de celle-ci.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * </p>");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @param element Cellule à modifier.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @return Valeur de la cellule.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  protected Object getValue(Object element) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  \tif (element instanceof UpdatableItem) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      UpdatableItem item = (UpdatableItem) element;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      if (item.getItem() instanceof ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_113_37 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_113_37); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_113_37.setRuntimeParent(null);
        _jettag_c_get_113_37.setTagInfo(_td_c_get_113_37);
        _jettag_c_get_113_37.doStart(context, out);
        _jettag_c_get_113_37.doEnd();
        out.write(") {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_114_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_114_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_114_9.setRuntimeParent(null);
        _jettag_c_get_114_9.setTagInfo(_td_c_get_114_9);
        _jettag_c_get_114_9.doStart(context, out);
        _jettag_c_get_114_9.doEnd();
        out.write(" ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_114_51 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_114_51); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_114_51.setRuntimeParent(null);
        _jettag_c_get_114_51.setTagInfo(_td_c_get_114_51);
        _jettag_c_get_114_51.doStart(context, out);
        _jettag_c_get_114_51.doEnd();
        out.write(" = (");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_114_96 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_114_96); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_114_96.setRuntimeParent(null);
        _jettag_c_get_114_96.setTagInfo(_td_c_get_114_96);
        _jettag_c_get_114_96.doStart(context, out);
        _jettag_c_get_114_96.doEnd();
        out.write(") item.getItem();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        switch (column) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        //TODO Compléter le return");  //$NON-NLS-1$        
        out.write(NL);         
        RuntimeTagElement _jettag_c_iterate_117_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "iterate", "c:iterate", _td_c_iterate_117_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_iterate_117_9.setRuntimeParent(null);
        _jettag_c_iterate_117_9.setTagInfo(_td_c_iterate_117_9);
        _jettag_c_iterate_117_9.doStart(context, out);
        while (_jettag_c_iterate_117_9.okToProcessBody()) {
            out.write("          \tcase ");  //$NON-NLS-1$        
            RuntimeTagElement _jettag_c_get_118_17 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_118_17); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_get_118_17.setRuntimeParent(_jettag_c_iterate_117_9);
            _jettag_c_get_118_17.setTagInfo(_td_c_get_118_17);
            _jettag_c_get_118_17.doStart(context, out);
            _jettag_c_get_118_17.doEnd();
            out.write(":");  //$NON-NLS-1$        
            out.write(NL);         
            out.write("          \t\treturn ;");  //$NON-NLS-1$        
            out.write(NL);         
            _jettag_c_iterate_117_9.handleBodyContent(out);
        }
        _jettag_c_iterate_117_9.doEnd();
        out.write("            default:");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("              break;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("   /**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * <p>");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * Méthode permettant de valider le contenu de la cellule après la modification de celle-ci.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * </p>");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @param element Cellule modifiée.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @param value Valeur de la cellule modifiée.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  protected void setValue(Object element, Object value) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    if (element instanceof UpdatableItem) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      UpdatableItem item = (UpdatableItem) element;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      if (item.getItem() instanceof ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_141_37 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_141_37); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_141_37.setRuntimeParent(null);
        _jettag_c_get_141_37.setTagInfo(_td_c_get_141_37);
        _jettag_c_get_141_37.doStart(context, out);
        _jettag_c_get_141_37.doEnd();
        out.write(") {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_142_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_142_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_142_9.setRuntimeParent(null);
        _jettag_c_get_142_9.setTagInfo(_td_c_get_142_9);
        _jettag_c_get_142_9.doStart(context, out);
        _jettag_c_get_142_9.doEnd();
        out.write(" ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_142_51 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_142_51); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_142_51.setRuntimeParent(null);
        _jettag_c_get_142_51.setTagInfo(_td_c_get_142_51);
        _jettag_c_get_142_51.doStart(context, out);
        _jettag_c_get_142_51.doEnd();
        out.write(" = (");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_142_96 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_142_96); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_142_96.setRuntimeParent(null);
        _jettag_c_get_142_96.setTagInfo(_td_c_get_142_96);
        _jettag_c_get_142_96.doStart(context, out);
        _jettag_c_get_142_96.doEnd();
        out.write(") item.getItem();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        switch (column) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        //TODO Compléter la méthode pour chaque cas");  //$NON-NLS-1$        
        out.write(NL);         
        RuntimeTagElement _jettag_c_iterate_145_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "iterate", "c:iterate", _td_c_iterate_145_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_iterate_145_9.setRuntimeParent(null);
        _jettag_c_iterate_145_9.setTagInfo(_td_c_iterate_145_9);
        _jettag_c_iterate_145_9.doStart(context, out);
        while (_jettag_c_iterate_145_9.okToProcessBody()) {
            out.write("          \tcase ");  //$NON-NLS-1$        
            RuntimeTagElement _jettag_c_get_146_17 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_146_17); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_get_146_17.setRuntimeParent(_jettag_c_iterate_145_9);
            _jettag_c_get_146_17.setTagInfo(_td_c_get_146_17);
            _jettag_c_get_146_17.doStart(context, out);
            _jettag_c_get_146_17.doEnd();
            out.write(":");  //$NON-NLS-1$        
            out.write(NL);         
            out.write("          \t\tbreak;");  //$NON-NLS-1$        
            out.write(NL);         
            _jettag_c_iterate_145_9.handleBodyContent(out);
        }
        _jettag_c_iterate_145_9.doEnd();
        out.write("            default:");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("              break;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      tableau.update(element, null);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      tableau.refresh();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
    }
}

package com.sneda.dev.helpers.jet.compiled;

import org.eclipse.jet.JET2Context;
import org.eclipse.jet.JET2Template;
import org.eclipse.jet.JET2Writer;
import org.eclipse.jet.taglib.RuntimeTagElement;
import org.eclipse.jet.taglib.TagInfo;

public class _jet_ControleurMyNamejava_2 implements JET2Template {
    private static final String _jetns_c = "org.eclipse.jet.controlTags"; //$NON-NLS-1$

    public _jet_ControleurMyNamejava_2() {
        super();
    }

    private static final String NL = System.getProperty("line.separator"); //$NON-NLS-1$
    
    private static final TagInfo _td_c_get_1_9 = new TagInfo("c:get", //$NON-NLS-1$
            1, 9,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/package", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_9_8 = new TagInfo("c:get", //$NON-NLS-1$
            9, 8,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/package", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_9_52 = new TagInfo("c:get", //$NON-NLS-1$
            9, 52,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_10_8 = new TagInfo("c:get", //$NON-NLS-1$
            10, 8,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/package", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_10_51 = new TagInfo("c:get", //$NON-NLS-1$
            10, 51,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_choose_12_1 = new TagInfo("c:choose", //$NON-NLS-1$
            12, 1,
            new String[] {
            },
            new String[] {
            } );
    private static final TagInfo _td_c_when_13_1 = new TagInfo("c:when", //$NON-NLS-1$
            13, 1,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/isIhmEtendue = 'ihmEtendue'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_21_12 = new TagInfo("c:get", //$NON-NLS-1$
            21, 12,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "/input/project/author", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_choose_23_1 = new TagInfo("c:choose", //$NON-NLS-1$
            23, 1,
            new String[] {
            },
            new String[] {
            } );
    private static final TagInfo _td_c_when_24_2 = new TagInfo("c:when", //$NON-NLS-1$
            24, 2,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/isIhmEtendue = 'ihmEtendue'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_25_24 = new TagInfo("c:get", //$NON-NLS-1$
            25, 24,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_25_71 = new TagInfo("c:get", //$NON-NLS-1$
            25, 71,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_otherwise_27_4 = new TagInfo("c:otherwise", //$NON-NLS-1$
            27, 4,
            new String[] {
            },
            new String[] {
            } );
    private static final TagInfo _td_c_get_28_24 = new TagInfo("c:get", //$NON-NLS-1$
            28, 24,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_28_71 = new TagInfo("c:get", //$NON-NLS-1$
            28, 71,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_32_23 = new TagInfo("c:get", //$NON-NLS-1$
            32, 23,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_34_18 = new TagInfo("c:get", //$NON-NLS-1$
            34, 18,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_choose_36_3 = new TagInfo("c:choose", //$NON-NLS-1$
            36, 3,
            new String[] {
            },
            new String[] {
            } );
    private static final TagInfo _td_c_when_37_2 = new TagInfo("c:when", //$NON-NLS-1$
            37, 2,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/isIhmEtendue = 'ihmEtendue'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_39_17 = new TagInfo("c:get", //$NON-NLS-1$
            39, 17,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_choose_42_3 = new TagInfo("c:choose", //$NON-NLS-1$
            42, 3,
            new String[] {
            },
            new String[] {
            } );
    private static final TagInfo _td_c_when_43_2 = new TagInfo("c:when", //$NON-NLS-1$
            43, 2,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/isIhmEtendue = 'ihmEtendue'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_44_19 = new TagInfo("c:get", //$NON-NLS-1$
            44, 19,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_44_54 = new TagInfo("c:get", //$NON-NLS-1$
            44, 54,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_48_26 = new TagInfo("c:get", //$NON-NLS-1$
            48, 26,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_otherwise_52_2 = new TagInfo("c:otherwise", //$NON-NLS-1$
            52, 2,
            new String[] {
            },
            new String[] {
            } );
    private static final TagInfo _td_c_get_53_21 = new TagInfo("c:get", //$NON-NLS-1$
            53, 21,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_53_56 = new TagInfo("c:get", //$NON-NLS-1$
            53, 56,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_63_16 = new TagInfo("c:get", //$NON-NLS-1$
            63, 16,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_67_20 = new TagInfo("c:get", //$NON-NLS-1$
            67, 20,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_71_33 = new TagInfo("c:get", //$NON-NLS-1$
            71, 33,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_79_25 = new TagInfo("c:get", //$NON-NLS-1$
            79, 25,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_79_100 = new TagInfo("c:get", //$NON-NLS-1$
            79, 100,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_choose_126_3 = new TagInfo("c:choose", //$NON-NLS-1$
            126, 3,
            new String[] {
            },
            new String[] {
            } );
    private static final TagInfo _td_c_when_127_2 = new TagInfo("c:when", //$NON-NLS-1$
            127, 2,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/isIhmEtendue = 'ihmEtendue'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_140_10 = new TagInfo("c:get", //$NON-NLS-1$
            140, 10,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );

    public void generate(final JET2Context context, final JET2Writer __out) {
        JET2Writer out = __out;
        out.write("package ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_1_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_1_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_1_9.setRuntimeParent(null);
        _jettag_c_get_1_9.setTagInfo(_td_c_get_1_9);
        _jettag_c_get_1_9.doStart(context, out);
        _jettag_c_get_1_9.doEnd();
        out.write(".impl;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import java.util.HashSet;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import java.util.Set;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.resource.ImageDescriptor;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.ui.IPersistableElement;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.ui.PartInitException;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_9_8 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_9_8); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_9_8.setRuntimeParent(null);
        _jettag_c_get_9_8.setTagInfo(_td_c_get_9_8);
        _jettag_c_get_9_8.doStart(context, out);
        _jettag_c_get_9_8.doEnd();
        out.write(".intf.I");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_9_52 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_9_52); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_9_52.setRuntimeParent(null);
        _jettag_c_get_9_52.setTagInfo(_td_c_get_9_52);
        _jettag_c_get_9_52.doStart(context, out);
        _jettag_c_get_9_52.doEnd();
        out.write("UC;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_10_8 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_10_8); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_10_8.setRuntimeParent(null);
        _jettag_c_get_10_8.setTagInfo(_td_c_get_10_8);
        _jettag_c_get_10_8.doStart(context, out);
        _jettag_c_get_10_8.doEnd();
        out.write(".intf.");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_10_51 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_10_51); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_10_51.setRuntimeParent(null);
        _jettag_c_get_10_51.setTagInfo(_td_c_get_10_51);
        _jettag_c_get_10_51.doStart(context, out);
        _jettag_c_get_10_51.doEnd();
        out.write("ReqAO;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.concepts.platform.PlatformHelper;");  //$NON-NLS-1$        
        out.write(NL);         
        RuntimeTagElement _jettag_c_choose_12_1 = context.getTagFactory().createRuntimeTag(_jetns_c, "choose", "c:choose", _td_c_choose_12_1); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_choose_12_1.setRuntimeParent(null);
        _jettag_c_choose_12_1.setTagInfo(_td_c_choose_12_1);
        _jettag_c_choose_12_1.doStart(context, out);
        JET2Writer _jettag_c_choose_12_1_saved_out = out;
        while (_jettag_c_choose_12_1.okToProcessBody()) {
            out = out.newNestedContentWriter();
            RuntimeTagElement _jettag_c_when_13_1 = context.getTagFactory().createRuntimeTag(_jetns_c, "when", "c:when", _td_c_when_13_1); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_when_13_1.setRuntimeParent(_jettag_c_choose_12_1);
            _jettag_c_when_13_1.setTagInfo(_td_c_when_13_1);
            _jettag_c_when_13_1.doStart(context, out);
            JET2Writer _jettag_c_when_13_1_saved_out = out;
            while (_jettag_c_when_13_1.okToProcessBody()) {
                out = out.newNestedContentWriter();
                out.write("import com.sneda.pgi.prm.prl.parameters.IParameterDescriptor;");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("import com.sneda.pgi.prm.prl.parameters.IParameterEditorInput;");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_when_13_1.handleBodyContent(out);
            }
            out = _jettag_c_when_13_1_saved_out;
            _jettag_c_when_13_1.doEnd();
            _jettag_c_choose_12_1.handleBodyContent(out);
        }
        out = _jettag_c_choose_12_1_saved_out;
        _jettag_c_choose_12_1.doEnd();
        out.write("/**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * Le controleur est l'{@code IEditorInput} de l'Editor.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * @author ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_21_12 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_21_12); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_21_12.setRuntimeParent(null);
        _jettag_c_get_21_12.setTagInfo(_td_c_get_21_12);
        _jettag_c_get_21_12.doStart(context, out);
        _jettag_c_get_21_12.doEnd();
        out.write(NL);         
        out.write(" */");  //$NON-NLS-1$        
        out.write(NL);         
        RuntimeTagElement _jettag_c_choose_23_1 = context.getTagFactory().createRuntimeTag(_jetns_c, "choose", "c:choose", _td_c_choose_23_1); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_choose_23_1.setRuntimeParent(null);
        _jettag_c_choose_23_1.setTagInfo(_td_c_choose_23_1);
        _jettag_c_choose_23_1.doStart(context, out);
        JET2Writer _jettag_c_choose_23_1_saved_out = out;
        while (_jettag_c_choose_23_1.okToProcessBody()) {
            out = out.newNestedContentWriter();
            RuntimeTagElement _jettag_c_when_24_2 = context.getTagFactory().createRuntimeTag(_jetns_c, "when", "c:when", _td_c_when_24_2); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_when_24_2.setRuntimeParent(_jettag_c_choose_23_1);
            _jettag_c_when_24_2.setTagInfo(_td_c_when_24_2);
            _jettag_c_when_24_2.doStart(context, out);
            JET2Writer _jettag_c_when_24_2_saved_out = out;
            while (_jettag_c_when_24_2.okToProcessBody()) {
                out = out.newNestedContentWriter();
                out.write("public class Controleur");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_25_24 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_25_24); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_25_24.setRuntimeParent(_jettag_c_when_24_2);
                _jettag_c_get_25_24.setTagInfo(_td_c_get_25_24);
                _jettag_c_get_25_24.doStart(context, out);
                _jettag_c_get_25_24.doEnd();
                out.write(" implements I");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_25_71 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_25_71); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_25_71.setRuntimeParent(_jettag_c_when_24_2);
                _jettag_c_get_25_71.setTagInfo(_td_c_get_25_71);
                _jettag_c_get_25_71.doStart(context, out);
                _jettag_c_get_25_71.doEnd();
                out.write("UC, IParameterEditorInput {");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_when_24_2.handleBodyContent(out);
            }
            out = _jettag_c_when_24_2_saved_out;
            _jettag_c_when_24_2.doEnd();
            RuntimeTagElement _jettag_c_otherwise_27_4 = context.getTagFactory().createRuntimeTag(_jetns_c, "otherwise", "c:otherwise", _td_c_otherwise_27_4); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_otherwise_27_4.setRuntimeParent(_jettag_c_choose_23_1);
            _jettag_c_otherwise_27_4.setTagInfo(_td_c_otherwise_27_4);
            _jettag_c_otherwise_27_4.doStart(context, out);
            JET2Writer _jettag_c_otherwise_27_4_saved_out = out;
            while (_jettag_c_otherwise_27_4.okToProcessBody()) {
                out = out.newNestedContentWriter();
                out.write("public class Controleur");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_28_24 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_28_24); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_28_24.setRuntimeParent(_jettag_c_otherwise_27_4);
                _jettag_c_get_28_24.setTagInfo(_td_c_get_28_24);
                _jettag_c_get_28_24.doStart(context, out);
                _jettag_c_get_28_24.doEnd();
                out.write(" implements I");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_28_71 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_28_71); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_28_71.setRuntimeParent(_jettag_c_otherwise_27_4);
                _jettag_c_get_28_71.setTagInfo(_td_c_get_28_71);
                _jettag_c_get_28_71.doStart(context, out);
                _jettag_c_get_28_71.doEnd();
                out.write("UC, IEditorInput {");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_otherwise_27_4.handleBodyContent(out);
            }
            out = _jettag_c_otherwise_27_4_saved_out;
            _jettag_c_otherwise_27_4.doEnd();
            _jettag_c_choose_23_1.handleBodyContent(out);
        }
        out = _jettag_c_choose_23_1_saved_out;
        _jettag_c_choose_23_1.doEnd();
        out.write(NL);         
        out.write("  private final Modele");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_32_23 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_32_23); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_32_23.setRuntimeParent(null);
        _jettag_c_get_32_23.setTagInfo(_td_c_get_32_23);
        _jettag_c_get_32_23.doStart(context, out);
        _jettag_c_get_32_23.doEnd();
        out.write(" modele;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  private Editeur");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_34_18 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_34_18); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_34_18.setRuntimeParent(null);
        _jettag_c_get_34_18.setTagInfo(_td_c_get_34_18);
        _jettag_c_get_34_18.doStart(context, out);
        _jettag_c_get_34_18.doEnd();
        out.write(" editeur;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        RuntimeTagElement _jettag_c_choose_36_3 = context.getTagFactory().createRuntimeTag(_jetns_c, "choose", "c:choose", _td_c_choose_36_3); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_choose_36_3.setRuntimeParent(null);
        _jettag_c_choose_36_3.setTagInfo(_td_c_choose_36_3);
        _jettag_c_choose_36_3.doStart(context, out);
        JET2Writer _jettag_c_choose_36_3_saved_out = out;
        while (_jettag_c_choose_36_3.okToProcessBody()) {
            out = out.newNestedContentWriter();
            RuntimeTagElement _jettag_c_when_37_2 = context.getTagFactory().createRuntimeTag(_jetns_c, "when", "c:when", _td_c_when_37_2); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_when_37_2.setRuntimeParent(_jettag_c_choose_36_3);
            _jettag_c_when_37_2.setTagInfo(_td_c_when_37_2);
            _jettag_c_when_37_2.doStart(context, out);
            JET2Writer _jettag_c_when_37_2_saved_out = out;
            while (_jettag_c_when_37_2.okToProcessBody()) {
                out = out.newNestedContentWriter();
                out.write("  private final IParameterDescriptor parameterDescriptor;");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("  private final ");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_39_17 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_39_17); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_39_17.setRuntimeParent(_jettag_c_when_37_2);
                _jettag_c_get_39_17.setTagInfo(_td_c_get_39_17);
                _jettag_c_get_39_17.doStart(context, out);
                _jettag_c_get_39_17.doEnd();
                out.write("ReqAO parametres;");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_when_37_2.handleBodyContent(out);
            }
            out = _jettag_c_when_37_2_saved_out;
            _jettag_c_when_37_2.doEnd();
            _jettag_c_choose_36_3.handleBodyContent(out);
        }
        out = _jettag_c_choose_36_3_saved_out;
        _jettag_c_choose_36_3.doEnd();
        RuntimeTagElement _jettag_c_choose_42_3 = context.getTagFactory().createRuntimeTag(_jetns_c, "choose", "c:choose", _td_c_choose_42_3); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_choose_42_3.setRuntimeParent(null);
        _jettag_c_choose_42_3.setTagInfo(_td_c_choose_42_3);
        _jettag_c_choose_42_3.doStart(context, out);
        JET2Writer _jettag_c_choose_42_3_saved_out = out;
        while (_jettag_c_choose_42_3.okToProcessBody()) {
            out = out.newNestedContentWriter();
            RuntimeTagElement _jettag_c_when_43_2 = context.getTagFactory().createRuntimeTag(_jetns_c, "when", "c:when", _td_c_when_43_2); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_when_43_2.setRuntimeParent(_jettag_c_choose_42_3);
            _jettag_c_when_43_2.setTagInfo(_td_c_when_43_2);
            _jettag_c_when_43_2.doStart(context, out);
            JET2Writer _jettag_c_when_43_2_saved_out = out;
            while (_jettag_c_when_43_2.okToProcessBody()) {
                out = out.newNestedContentWriter();
                out.write("\tpublic Controleur");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_44_19 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_44_19); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_44_19.setRuntimeParent(_jettag_c_when_43_2);
                _jettag_c_get_44_19.setTagInfo(_td_c_get_44_19);
                _jettag_c_get_44_19.doStart(context, out);
                _jettag_c_get_44_19.doEnd();
                out.write("(");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_44_54 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_44_54); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_44_54.setRuntimeParent(_jettag_c_when_43_2);
                _jettag_c_get_44_54.setTagInfo(_td_c_get_44_54);
                _jettag_c_get_44_54.doStart(context, out);
                _jettag_c_get_44_54.doEnd();
                out.write("ReqAO parametres,");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t      IParameterDescriptor parameterDescriptor) {");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t    this.parameterDescriptor = parameterDescriptor;");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t    this.parametres = parametres;");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t    modele = new Modele");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_48_26 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_48_26); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_48_26.setRuntimeParent(_jettag_c_when_43_2);
                _jettag_c_get_48_26.setTagInfo(_td_c_get_48_26);
                _jettag_c_get_48_26.doStart(context, out);
                _jettag_c_get_48_26.doEnd();
                out.write("();");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t    appeler();");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t  }");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_when_43_2.handleBodyContent(out);
            }
            out = _jettag_c_when_43_2_saved_out;
            _jettag_c_when_43_2.doEnd();
            RuntimeTagElement _jettag_c_otherwise_52_2 = context.getTagFactory().createRuntimeTag(_jetns_c, "otherwise", "c:otherwise", _td_c_otherwise_52_2); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_otherwise_52_2.setRuntimeParent(_jettag_c_choose_42_3);
            _jettag_c_otherwise_52_2.setTagInfo(_td_c_otherwise_52_2);
            _jettag_c_otherwise_52_2.doStart(context, out);
            JET2Writer _jettag_c_otherwise_52_2_saved_out = out;
            while (_jettag_c_otherwise_52_2.okToProcessBody()) {
                out = out.newNestedContentWriter();
                out.write("\t  public Controleur");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_53_21 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_53_21); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_53_21.setRuntimeParent(_jettag_c_otherwise_52_2);
                _jettag_c_get_53_21.setTagInfo(_td_c_get_53_21);
                _jettag_c_get_53_21.doStart(context, out);
                _jettag_c_get_53_21.doEnd();
                out.write("(");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_53_56 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_53_56); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_53_56.setRuntimeParent(_jettag_c_otherwise_52_2);
                _jettag_c_get_53_56.setTagInfo(_td_c_get_53_56);
                _jettag_c_get_53_56.doStart(context, out);
                _jettag_c_get_53_56.doEnd();
                out.write("ReqAO reqAO) {");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t    // TODO utiliser le reqAO si besoin pour initialiser le modèle");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t    // TODO faire éventuellement un appel service pour valuer le modèle");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t    modele = null;");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t  }");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_otherwise_52_2.handleBodyContent(out);
            }
            out = _jettag_c_otherwise_52_2_saved_out;
            _jettag_c_otherwise_52_2.doEnd();
            _jettag_c_choose_42_3.handleBodyContent(out);
        }
        out = _jettag_c_choose_42_3_saved_out;
        _jettag_c_choose_42_3.doEnd();
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        out.write("  public Modele");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_63_16 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_63_16); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_63_16.setRuntimeParent(null);
        _jettag_c_get_63_16.setTagInfo(_td_c_get_63_16);
        _jettag_c_get_63_16.doStart(context, out);
        _jettag_c_get_63_16.doEnd();
        out.write(" getModele() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return modele;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  protected Editeur");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_67_20 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_67_20); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_67_20.setRuntimeParent(null);
        _jettag_c_get_67_20.setTagInfo(_td_c_get_67_20);
        _jettag_c_get_67_20.doStart(context, out);
        _jettag_c_get_67_20.doEnd();
        out.write(" getEditeur() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return editeur;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  public void setEditeur(Editeur");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_71_33 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_71_33); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_71_33.setRuntimeParent(null);
        _jettag_c_get_71_33.setTagInfo(_td_c_get_71_33);
        _jettag_c_get_71_33.doStart(context, out);
        _jettag_c_get_71_33.doEnd();
        out.write(" editeur) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t    this.editeur = editeur;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public void appeler() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    try {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      // Lancer l'editeur");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      editeur = (Editeur");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_79_25 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_79_25); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_79_25.setRuntimeParent(null);
        _jettag_c_get_79_25.setTagInfo(_td_c_get_79_25);
        _jettag_c_get_79_25.doStart(context, out);
        _jettag_c_get_79_25.doEnd();
        out.write(") PlatformHelper.openEditor(this, Editeur");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_79_100 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_79_100); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_79_100.setRuntimeParent(null);
        _jettag_c_get_79_100.setTagInfo(_td_c_get_79_100);
        _jettag_c_get_79_100.doStart(context, out);
        _jettag_c_get_79_100.doEnd();
        out.write(".ID);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    } catch (PartInitException e) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      throw new RuntimeException(e);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  public boolean save() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    // TODO appel du service de sauvegarde");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return false;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public boolean exists() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return false;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public ImageDescriptor getImageDescriptor() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public IPersistableElement getPersistable() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  @SuppressWarnings(\"rawtypes\")");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public Object getAdapter(Class adapter) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public String getName() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    // TODO a completer");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    // return getModele().getAOE().getDescriptionCourte();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return \"TODO\";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public String getToolTipText() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    // TODO a completer");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    // return getModele().getAOE().getDescriptionLongue();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return \"TODO\";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        RuntimeTagElement _jettag_c_choose_126_3 = context.getTagFactory().createRuntimeTag(_jetns_c, "choose", "c:choose", _td_c_choose_126_3); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_choose_126_3.setRuntimeParent(null);
        _jettag_c_choose_126_3.setTagInfo(_td_c_choose_126_3);
        _jettag_c_choose_126_3.doStart(context, out);
        JET2Writer _jettag_c_choose_126_3_saved_out = out;
        while (_jettag_c_choose_126_3.okToProcessBody()) {
            out = out.newNestedContentWriter();
            RuntimeTagElement _jettag_c_when_127_2 = context.getTagFactory().createRuntimeTag(_jetns_c, "when", "c:when", _td_c_when_127_2); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_when_127_2.setRuntimeParent(_jettag_c_choose_126_3);
            _jettag_c_when_127_2.setTagInfo(_td_c_when_127_2);
            _jettag_c_when_127_2.doStart(context, out);
            JET2Writer _jettag_c_when_127_2_saved_out = out;
            while (_jettag_c_when_127_2.okToProcessBody()) {
                out = out.newNestedContentWriter();
                out.write("  @Override");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("  public IParameterDescriptor getParameter() {");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("    return this.parameterDescriptor instanceof IParameterDescriptor ? (IParameterDescriptor) this.parameterDescriptor");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("        : null;");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("  }");  //$NON-NLS-1$        
                out.write(NL);         
                out.write(NL);         
                out.write("  public Set<String> getErrors() {");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t  Set<String> errors = new HashSet<>();");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t  // TODO récupération des erreurs");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t  return errors;");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("  }");  //$NON-NLS-1$        
                out.write(NL);         
                out.write(NL);         
                out.write("  public ");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_140_10 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_140_10); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_140_10.setRuntimeParent(_jettag_c_when_127_2);
                _jettag_c_get_140_10.setTagInfo(_td_c_get_140_10);
                _jettag_c_get_140_10.doStart(context, out);
                _jettag_c_get_140_10.doEnd();
                out.write("ReqAO getParametres() {");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t    return parametres;");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t  }");  //$NON-NLS-1$        
                out.write(NL);         
                out.write(NL);         
                _jettag_c_when_127_2.handleBodyContent(out);
            }
            out = _jettag_c_when_127_2_saved_out;
            _jettag_c_when_127_2.doEnd();
            _jettag_c_choose_126_3.handleBodyContent(out);
        }
        out = _jettag_c_choose_126_3_saved_out;
        _jettag_c_choose_126_3.doEnd();
        out.write("}");  //$NON-NLS-1$        
    }
}

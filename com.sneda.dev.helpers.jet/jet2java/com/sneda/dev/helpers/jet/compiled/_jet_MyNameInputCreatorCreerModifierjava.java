package com.sneda.dev.helpers.jet.compiled;

import org.eclipse.jet.JET2Context;
import org.eclipse.jet.JET2Template;
import org.eclipse.jet.JET2Writer;
import org.eclipse.jet.taglib.RuntimeTagElement;
import org.eclipse.jet.taglib.TagInfo;

public class _jet_MyNameInputCreatorCreerModifierjava implements JET2Template {
    private static final String _jetns_c = "org.eclipse.jet.controlTags"; //$NON-NLS-1$

    public _jet_MyNameInputCreatorCreerModifierjava() {
        super();
    }

    private static final String NL = System.getProperty("line.separator"); //$NON-NLS-1$
    
    private static final TagInfo _td_c_get_1_9 = new TagInfo("c:get", //$NON-NLS-1$
            1, 9,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/packageCheminInputCreatorCreerModifier", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_17_12 = new TagInfo("c:get", //$NON-NLS-1$
            17, 12,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "/input/project/author", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_20_14 = new TagInfo("c:get", //$NON-NLS-1$
            20, 14,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_27_28 = new TagInfo("c:get", //$NON-NLS-1$
            27, 28,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_29_10 = new TagInfo("c:get", //$NON-NLS-1$
            29, 10,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_30_18 = new TagInfo("c:get", //$NON-NLS-1$
            30, 18,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_41_80 = new TagInfo("c:get", //$NON-NLS-1$
            41, 80,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_46_12 = new TagInfo("c:get", //$NON-NLS-1$
            46, 12,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_63_16 = new TagInfo("c:get", //$NON-NLS-1$
            63, 16,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );

    public void generate(final JET2Context context, final JET2Writer __out) {
        JET2Writer out = __out;
        out.write("package ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_1_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_1_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_1_9.setRuntimeParent(null);
        _jettag_c_get_1_9.setTagInfo(_td_c_get_1_9);
        _jettag_c_get_1_9.doStart(context, out);
        _jettag_c_get_1_9.doEnd();
        out.write(";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import java.util.Collection;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import com.progress.common.exception.ProException;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.progress.open4gl.Parameter;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.progress.open4gl.ProDataGraph;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.progress.open4gl.ProDataGraphMetaData;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.progress.open4gl.ProDataObject;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.progress.open4gl.ProDataObjectMetaData;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.appserver.core.ProDataGraphCreator;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.appserver.core.ProDataGraphMetaDataCreator;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.appserver.util.ProDataGraphParameterUtils;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.appserver.util.ProDataObjectUtils;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("/**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * @author ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_17_12 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_17_12); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_17_12.setRuntimeParent(null);
        _jettag_c_get_17_12.setTagInfo(_td_c_get_17_12);
        _jettag_c_get_17_12.doStart(context, out);
        _jettag_c_get_17_12.doEnd();
        out.write(NL);         
        out.write(" *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("public class ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_20_14 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_20_14); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_20_14.setRuntimeParent(null);
        _jettag_c_get_20_14.setTagInfo(_td_c_get_20_14);
        _jettag_c_get_20_14.doStart(context, out);
        _jettag_c_get_20_14.doEnd();
        out.write("InputCreatorCreerModifier implements ProDataGraphCreator {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("// TODO compléter les variables avec les bons noms des champs progress + en ajouter si nécessaire");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  private static final String CODE = \"\";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  private static final String LIBELLE = \"\";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  private static final String LACTIF = \"LACTIF\";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  private final Collection<");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_27_28 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_27_28); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_27_28.setRuntimeParent(null);
        _jettag_c_get_27_28.setTagInfo(_td_c_get_27_28);
        _jettag_c_get_27_28.doStart(context, out);
        _jettag_c_get_27_28.doEnd();
        out.write("> listeNomenclature;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  public ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_29_10 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_29_10); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_29_10.setRuntimeParent(null);
        _jettag_c_get_29_10.setTagInfo(_td_c_get_29_10);
        _jettag_c_get_29_10.doStart(context, out);
        _jettag_c_get_29_10.doEnd();
        out.write("InputCreatorCreerModifier(");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      Collection<");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_30_18 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_30_18); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_30_18.setRuntimeParent(null);
        _jettag_c_get_30_18.setTagInfo(_td_c_get_30_18);
        _jettag_c_get_30_18.doStart(context, out);
        _jettag_c_get_30_18.doEnd();
        out.write("> listeNomenclature) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    super();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    this.listeNomenclature = listeNomenclature;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  /*");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * (non-Javadoc)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @see com.sneda.appserver.core.ProDataGraphCreator#createProDataGraph()");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public ProDataGraph createProDataGraph() throws Exception {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    ProDataGraph dataGraph = ProDataGraphParameterUtils.createProDataGraph(new ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_41_80 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_41_80); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_41_80.setRuntimeParent(null);
        _jettag_c_get_41_80.setTagInfo(_td_c_get_41_80);
        _jettag_c_get_41_80.doStart(context, out);
        _jettag_c_get_41_80.doEnd();
        out.write("InputMetaDataCreator());");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    ProDataObject dataObject = null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("    if (listeNomenclature != null) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("      for (");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_46_12 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_46_12); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_46_12.setRuntimeParent(null);
        _jettag_c_get_46_12.setTagInfo(_td_c_get_46_12);
        _jettag_c_get_46_12.doStart(context, out);
        _jettag_c_get_46_12.doEnd();
        out.write(" nmc : listeNomenclature) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("        if (nmc != null) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        \t// TODO ajouter le nom de la table temporaire en progress");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("          dataObject = dataGraph.createProDataObject(\"tt\");");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("          dataGraph.addProDataObject(dataObject);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("          ProDataObjectUtils.setString(dataObject, \"OID\", nmc.getId() != null ? nmc.getId().getValue().toString() : \"\");");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("          // TODO compléter avec les champs de l'objet");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("          ProDataObjectUtils.setString(dataObject, CODE, nmc.getCode() != null  ? nmc.getCode() : \"\");");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("          ProDataObjectUtils.setBoolean(dataObject, LACTIF, nmc.isActif());");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("          ProDataObjectUtils.setString(dataObject, LIBELLE, nmc.getLibelle() != null ? nmc.getLibelle() : \"\");");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return dataGraph;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  public class ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_63_16 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_63_16); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_63_16.setRuntimeParent(null);
        _jettag_c_get_63_16.setTagInfo(_td_c_get_63_16);
        _jettag_c_get_63_16.doStart(context, out);
        _jettag_c_get_63_16.doEnd();
        out.write("InputMetaDataCreator implements ProDataGraphMetaDataCreator {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("    @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    public ProDataGraphMetaData createProDataGraphMetaData() throws ProException {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      ProDataGraphMetaData pdgMetaData = ProDataGraphParameterUtils.getProDataGraphMetaDataForStandardParameters();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      // TODO compléter avec le nom de la table temporaire + le nombre de champs");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      ProDataObjectMetaData doMetaData = new ProDataObjectMetaData(\"tt\", 4, false, 0, null,");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("          null, null);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      // TODO modifier la structure en fonction des champs de la table");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      doMetaData.setFieldMetaData(1, \"OID\", 0, Parameter.PRO_CHARACTER, 0, 0);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      doMetaData.setFieldMetaData(2, CODE, 0, Parameter.PRO_CHARACTER, 0, 0);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      doMetaData.setFieldMetaData(3, LACTIF, 0, Parameter.PRO_LOGICAL, 0, 0);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      doMetaData.setFieldMetaData(4, LIBELLE, 0, Parameter.PRO_CHARACTER, 0, 0);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      pdgMetaData.addTable(doMetaData);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      return pdgMetaData;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("}");  //$NON-NLS-1$        
        out.write(NL);         
    }
}

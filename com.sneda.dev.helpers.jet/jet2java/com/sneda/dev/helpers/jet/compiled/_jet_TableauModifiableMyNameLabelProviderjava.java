package com.sneda.dev.helpers.jet.compiled;

import org.eclipse.jet.JET2Context;
import org.eclipse.jet.JET2Template;
import org.eclipse.jet.JET2Writer;
import org.eclipse.jet.taglib.RuntimeTagElement;
import org.eclipse.jet.taglib.TagInfo;

public class _jet_TableauModifiableMyNameLabelProviderjava implements JET2Template {
    private static final String _jetns_c = "org.eclipse.jet.controlTags"; //$NON-NLS-1$

    public _jet_TableauModifiableMyNameLabelProviderjava() {
        super();
    }

    private static final String NL = System.getProperty("line.separator"); //$NON-NLS-1$
    
    private static final TagInfo _td_c_get_1_9 = new TagInfo("c:get", //$NON-NLS-1$
            1, 9,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/package", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_17_12 = new TagInfo("c:get", //$NON-NLS-1$
            17, 12,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "/input/project/author", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_21_21 = new TagInfo("c:get", //$NON-NLS-1$
            21, 21,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_23_24 = new TagInfo("c:get", //$NON-NLS-1$
            23, 24,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_28_16 = new TagInfo("c:get", //$NON-NLS-1$
            28, 16,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_28_76 = new TagInfo("c:get", //$NON-NLS-1$
            28, 76,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_46_44 = new TagInfo("c:get", //$NON-NLS-1$
            46, 44,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_47_15 = new TagInfo("c:get", //$NON-NLS-1$
            47, 15,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_47_57 = new TagInfo("c:get", //$NON-NLS-1$
            47, 57,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_47_102 = new TagInfo("c:get", //$NON-NLS-1$
            47, 102,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_iterate_50_10 = new TagInfo("c:iterate", //$NON-NLS-1$
            50, 10,
            new String[] {
                "select", //$NON-NLS-1$
                "var", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/columnTable", //$NON-NLS-1$
                "columnTable", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_51_15 = new TagInfo("c:get", //$NON-NLS-1$
            51, 15,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/nameColumn", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_choose_52_11 = new TagInfo("c:choose", //$NON-NLS-1$
            52, 11,
            new String[] {
            },
            new String[] {
            } );
    private static final TagInfo _td_c_when_53_12 = new TagInfo("c:when", //$NON-NLS-1$
            53, 12,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/settingsTable/isCellAndAssistant = 'CellAndAssistant'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_54_13 = new TagInfo("c:if", //$NON-NLS-1$
            54, 13,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/editableColumn = 'true'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_when_58_12 = new TagInfo("c:when", //$NON-NLS-1$
            58, 12,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/settingsTable/isCell = 'Cellule'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_59_13 = new TagInfo("c:if", //$NON-NLS-1$
            59, 13,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/editableColumn = 'true'", //$NON-NLS-1$
            } );

    public void generate(final JET2Context context, final JET2Writer __out) {
        JET2Writer out = __out;
        out.write("package ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_1_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_1_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_1_9.setRuntimeParent(null);
        _jettag_c_get_1_9.setTagInfo(_td_c_get_1_9);
        _jettag_c_get_1_9.doStart(context, out);
        _jettag_c_get_1_9.doEnd();
        out.write(";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import org.eclipse.jface.viewers.CellLabelProvider;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.apache.commons.lang3.StringUtils;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.viewers.ViewerCell;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.swt.graphics.Font;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.swt.graphics.RGB;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.updatableviewer.UpdatableItem;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.external.ISharedColorsToolKit;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.external.ISharedImagesToolKit;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.external.SharedColorsToolKit;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.external.SharedImagesToolKit;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.table.ISpreadsheetExportProvider;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.swt.graphics.Image;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("/**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * @author ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_17_12 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_17_12); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_17_12.setRuntimeParent(null);
        _jettag_c_get_17_12.setTagInfo(_td_c_get_17_12);
        _jettag_c_get_17_12.doStart(context, out);
        _jettag_c_get_17_12.doEnd();
        out.write(NL);         
        out.write(" *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("public class Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_21_21 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_21_21); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_21_21.setRuntimeParent(null);
        _jettag_c_get_21_21.setTagInfo(_td_c_get_21_21);
        _jettag_c_get_21_21.doStart(context, out);
        _jettag_c_get_21_21.doEnd();
        out.write("LabelProvider extends CellLabelProvider implements ISpreadsheetExportProvider{");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  private final Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_23_24 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_23_24); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_23_24.setRuntimeParent(null);
        _jettag_c_get_23_24.setTagInfo(_td_c_get_23_24);
        _jettag_c_get_23_24.doStart(context, out);
        _jettag_c_get_23_24.doEnd();
        out.write("ColumnEnum colonne;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t/**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t * @param colonne");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\tpublic Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_28_16 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_28_16); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_28_16.setRuntimeParent(null);
        _jettag_c_get_28_16.setTagInfo(_td_c_get_28_16);
        _jettag_c_get_28_16.doStart(context, out);
        _jettag_c_get_28_16.doEnd();
        out.write("LabelProvider(Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_28_76 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_28_76); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_28_76.setRuntimeParent(null);
        _jettag_c_get_28_76.setTagInfo(_td_c_get_28_76);
        _jettag_c_get_28_76.doStart(context, out);
        _jettag_c_get_28_76.doEnd();
        out.write("ColumnEnum colonne)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t{");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\tthis.colonne = colonne;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t/*");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t * (non-Javadoc)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t * @see");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t * org.eclipse.jface.viewers.CellLabelProvider#update(org.eclipse.jface.viewers.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t * ViewerCell)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t@Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t  public void update(ViewerCell cell) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t  if (cell != null && cell.getElement() != null) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t          String texte = StringUtils.EMPTY;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t          if (cell.getElement() instanceof UpdatableItem) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t            UpdatableItem item = (UpdatableItem) cell.getElement();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t            if (item.getItem() instanceof ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_46_44 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_46_44); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_46_44.setRuntimeParent(null);
        _jettag_c_get_46_44.setTagInfo(_td_c_get_46_44);
        _jettag_c_get_46_44.doStart(context, out);
        _jettag_c_get_46_44.doEnd();
        out.write(") {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t            \t");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_47_15 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_47_15); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_47_15.setRuntimeParent(null);
        _jettag_c_get_47_15.setTagInfo(_td_c_get_47_15);
        _jettag_c_get_47_15.doStart(context, out);
        _jettag_c_get_47_15.doEnd();
        out.write(" ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_47_57 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_47_57); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_47_57.setRuntimeParent(null);
        _jettag_c_get_47_57.setTagInfo(_td_c_get_47_57);
        _jettag_c_get_47_57.doStart(context, out);
        _jettag_c_get_47_57.doEnd();
        out.write(" = (");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_47_102 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_47_102); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_47_102.setRuntimeParent(null);
        _jettag_c_get_47_102.setTagInfo(_td_c_get_47_102);
        _jettag_c_get_47_102.doStart(context, out);
        _jettag_c_get_47_102.doEnd();
        out.write(") item.getItem();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t        switch (colonne) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t        //TODO Le texte est la valeur que la colonne doit afficher");  //$NON-NLS-1$        
        out.write(NL);         
        RuntimeTagElement _jettag_c_iterate_50_10 = context.getTagFactory().createRuntimeTag(_jetns_c, "iterate", "c:iterate", _td_c_iterate_50_10); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_iterate_50_10.setRuntimeParent(null);
        _jettag_c_iterate_50_10.setTagInfo(_td_c_iterate_50_10);
        _jettag_c_iterate_50_10.doStart(context, out);
        while (_jettag_c_iterate_50_10.okToProcessBody()) {
            out.write("\t\t      \tcase ");  //$NON-NLS-1$        
            RuntimeTagElement _jettag_c_get_51_15 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_51_15); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_get_51_15.setRuntimeParent(_jettag_c_iterate_50_10);
            _jettag_c_get_51_15.setTagInfo(_td_c_get_51_15);
            _jettag_c_get_51_15.doStart(context, out);
            _jettag_c_get_51_15.doEnd();
            out.write(":");  //$NON-NLS-1$        
            out.write(NL);         
            RuntimeTagElement _jettag_c_choose_52_11 = context.getTagFactory().createRuntimeTag(_jetns_c, "choose", "c:choose", _td_c_choose_52_11); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_choose_52_11.setRuntimeParent(_jettag_c_iterate_50_10);
            _jettag_c_choose_52_11.setTagInfo(_td_c_choose_52_11);
            _jettag_c_choose_52_11.doStart(context, out);
            JET2Writer _jettag_c_choose_52_11_saved_out = out;
            while (_jettag_c_choose_52_11.okToProcessBody()) {
                out = out.newNestedContentWriter();
                RuntimeTagElement _jettag_c_when_53_12 = context.getTagFactory().createRuntimeTag(_jetns_c, "when", "c:when", _td_c_when_53_12); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_when_53_12.setRuntimeParent(_jettag_c_choose_52_11);
                _jettag_c_when_53_12.setTagInfo(_td_c_when_53_12);
                _jettag_c_when_53_12.doStart(context, out);
                JET2Writer _jettag_c_when_53_12_saved_out = out;
                while (_jettag_c_when_53_12.okToProcessBody()) {
                    out = out.newNestedContentWriter();
                    RuntimeTagElement _jettag_c_if_54_13 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_54_13); //$NON-NLS-1$ //$NON-NLS-2$
                    _jettag_c_if_54_13.setRuntimeParent(_jettag_c_when_53_12);
                    _jettag_c_if_54_13.setTagInfo(_td_c_if_54_13);
                    _jettag_c_if_54_13.doStart(context, out);
                    while (_jettag_c_if_54_13.okToProcessBody()) {
                        out.write("\t\t      \t\tcell.setBackground(SharedColorsToolKit.getColor(ISharedColorsToolKit.UPDATABLE_CELL));");  //$NON-NLS-1$        
                        out.write(NL);         
                        _jettag_c_if_54_13.handleBodyContent(out);
                    }
                    _jettag_c_if_54_13.doEnd();
                    _jettag_c_when_53_12.handleBodyContent(out);
                }
                out = _jettag_c_when_53_12_saved_out;
                _jettag_c_when_53_12.doEnd();
                RuntimeTagElement _jettag_c_when_58_12 = context.getTagFactory().createRuntimeTag(_jetns_c, "when", "c:when", _td_c_when_58_12); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_when_58_12.setRuntimeParent(_jettag_c_choose_52_11);
                _jettag_c_when_58_12.setTagInfo(_td_c_when_58_12);
                _jettag_c_when_58_12.doStart(context, out);
                JET2Writer _jettag_c_when_58_12_saved_out = out;
                while (_jettag_c_when_58_12.okToProcessBody()) {
                    out = out.newNestedContentWriter();
                    RuntimeTagElement _jettag_c_if_59_13 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_59_13); //$NON-NLS-1$ //$NON-NLS-2$
                    _jettag_c_if_59_13.setRuntimeParent(_jettag_c_when_58_12);
                    _jettag_c_if_59_13.setTagInfo(_td_c_if_59_13);
                    _jettag_c_if_59_13.doStart(context, out);
                    while (_jettag_c_if_59_13.okToProcessBody()) {
                        out.write("\t\t\t      \tcell.setBackground(SharedColorsToolKit.getColor(ISharedColorsToolKit.UPDATABLE_CELL));");  //$NON-NLS-1$        
                        out.write(NL);         
                        _jettag_c_if_59_13.handleBodyContent(out);
                    }
                    _jettag_c_if_59_13.doEnd();
                    _jettag_c_when_58_12.handleBodyContent(out);
                }
                out = _jettag_c_when_58_12_saved_out;
                _jettag_c_when_58_12.doEnd();
                _jettag_c_choose_52_11.handleBodyContent(out);
            }
            out = _jettag_c_choose_52_11_saved_out;
            _jettag_c_choose_52_11.doEnd();
            out.write("\t                texte = ;");  //$NON-NLS-1$        
            out.write(NL);         
            out.write("\t            break;");  //$NON-NLS-1$        
            out.write(NL);         
            _jettag_c_iterate_50_10.handleBodyContent(out);
        }
        _jettag_c_iterate_50_10.doEnd();
        out.write("\t          default:");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t            break;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t        }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t      cell.setText(texte);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t            }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t        }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t    }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\tpublic Image getImage(Object element) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\treturn SharedImagesToolKit.getImage(ISharedImagesToolKit.ICON_EMPTY);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t@Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\tpublic EDataType getDataType(Object element, int columnIndex) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\tswitch (colonne) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\treturn null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t@Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\tpublic String getDataFormat(Object element, int columnIndex) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\tswitch (colonne) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\treturn null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t@Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\tpublic Font getFont(Object element, int columnIndex) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\treturn null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t@Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\tpublic RGB getBackground(Object element, int columnIndex) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\treturn null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t@Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\tpublic RGB getForeground(Object element, int columnIndex) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\treturn null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t/*");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t * (non-Javadoc)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t * @see org.eclipse.jface.viewers.CellLabelProvider#useNativeToolTip(java.lang.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t * Object)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t@Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\tpublic boolean useNativeToolTip(Object object) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\treturn false;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t/*");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t * (non-Javadoc)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t * @see");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t * org.eclipse.jface.viewers.CellLabelProvider#getToolTipText(java.lang.Object)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t@Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\tpublic String getToolTipText(Object element) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\treturn colonne.getTooltipText();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t/*");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t * (non-Javadoc)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t * @see");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t * com.sneda.toolkit.widget.raksha.table.ISpreadsheetExportProvider#getValue(");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t * java.lang.Object, int)");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t@Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\tpublic Object getValue(Object element, int columnIndex) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t// TODO Auto-generated method stub");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\treturn null;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("}");  //$NON-NLS-1$        
        out.write(NL);         
    }
}

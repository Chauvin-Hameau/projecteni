package com.sneda.dev.helpers.jet.compiled;

import org.eclipse.jet.JET2Context;
import org.eclipse.jet.JET2Template;
import org.eclipse.jet.JET2Writer;
import org.eclipse.jet.taglib.RuntimeTagElement;
import org.eclipse.jet.taglib.TagInfo;

public class _jet_TableauModifiableMyNamejava implements JET2Template {
    private static final String _jetns_c = "org.eclipse.jet.controlTags"; //$NON-NLS-1$

    public _jet_TableauModifiableMyNamejava() {
        super();
    }

    private static final String NL = System.getProperty("line.separator"); //$NON-NLS-1$
    
    private static final TagInfo _td_c_get_1_9 = new TagInfo("c:get", //$NON-NLS-1$
            1, 9,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/package", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_setVariable_27_1 = new TagInfo("c:setVariable", //$NON-NLS-1$
            27, 1,
            new String[] {
                "select", //$NON-NLS-1$
                "var", //$NON-NLS-1$
            },
            new String[] {
                "0", //$NON-NLS-1$
                "numIndexColumn", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_setVariable_28_1 = new TagInfo("c:setVariable", //$NON-NLS-1$
            28, 1,
            new String[] {
                "select", //$NON-NLS-1$
                "var", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/sizeIndexColumnEditables", //$NON-NLS-1$
                "sizeIndex", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_30_15 = new TagInfo("c:get", //$NON-NLS-1$
            30, 15,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "/input/project/author", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_33_22 = new TagInfo("c:get", //$NON-NLS-1$
            33, 22,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_36_45 = new TagInfo("c:get", //$NON-NLS-1$
            36, 45,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/package", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_36_95 = new TagInfo("c:get", //$NON-NLS-1$
            36, 95,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_38_27 = new TagInfo("c:get", //$NON-NLS-1$
            38, 27,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_38_84 = new TagInfo("c:get", //$NON-NLS-1$
            38, 84,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_43_16 = new TagInfo("c:get", //$NON-NLS-1$
            43, 16,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_44_38 = new TagInfo("c:get", //$NON-NLS-1$
            44, 38,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_59_114 = new TagInfo("c:if", //$NON-NLS-1$
            59, 114,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/settingsTable/isMultiSelection = 'multiSelection'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_choose_60_20 = new TagInfo("c:choose", //$NON-NLS-1$
            60, 20,
            new String[] {
            },
            new String[] {
            } );
    private static final TagInfo _td_c_when_60_30 = new TagInfo("c:when", //$NON-NLS-1$
            60, 30,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/settingsTable/isCellAndAssistant = 'CellAndAssistant'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_iterate_60_117 = new TagInfo("c:iterate", //$NON-NLS-1$
            60, 117,
            new String[] {
                "select", //$NON-NLS-1$
                "var", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/editableColumnIndex/indexColumn", //$NON-NLS-1$
                "currColumnIndex", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_60_209 = new TagInfo("c:get", //$NON-NLS-1$
            60, 209,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currColumnIndex", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_60_243 = new TagInfo("c:if", //$NON-NLS-1$
            60, 243,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$numIndexColumn < $sizeIndex - 1", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_setVariable_60_297 = new TagInfo("c:setVariable", //$NON-NLS-1$
            60, 297,
            new String[] {
                "var", //$NON-NLS-1$
                "select", //$NON-NLS-1$
            },
            new String[] {
                "numIndexColumn", //$NON-NLS-1$
                "$numIndexColumn+1", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_when_60_383 = new TagInfo("c:when", //$NON-NLS-1$
            60, 383,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/settingsTable/isCell = 'Cellule'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_iterate_60_449 = new TagInfo("c:iterate", //$NON-NLS-1$
            60, 449,
            new String[] {
                "select", //$NON-NLS-1$
                "var", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/editableColumnIndex/indexColumn", //$NON-NLS-1$
                "currColumnIndex", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_60_541 = new TagInfo("c:get", //$NON-NLS-1$
            60, 541,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currColumnIndex", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_60_575 = new TagInfo("c:if", //$NON-NLS-1$
            60, 575,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$numIndexColumn < $sizeIndex - 1", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_setVariable_60_629 = new TagInfo("c:setVariable", //$NON-NLS-1$
            60, 629,
            new String[] {
                "var", //$NON-NLS-1$
                "select", //$NON-NLS-1$
            },
            new String[] {
                "numIndexColumn", //$NON-NLS-1$
                "$numIndexColumn+1", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_iterate_70_3 = new TagInfo("c:iterate", //$NON-NLS-1$
            70, 3,
            new String[] {
                "select", //$NON-NLS-1$
                "var", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/settingsTable", //$NON-NLS-1$
                "currSetting", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_71_5 = new TagInfo("c:if", //$NON-NLS-1$
            71, 5,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currSetting/isAddItem = 'false'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_74_5 = new TagInfo("c:if", //$NON-NLS-1$
            74, 5,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currSetting/isUpdateItem = 'false'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_77_5 = new TagInfo("c:if", //$NON-NLS-1$
            77, 5,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currSetting/isUpdateItems = 'false'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_80_5 = new TagInfo("c:if", //$NON-NLS-1$
            80, 5,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currSetting/isDellItem = 'false'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_83_5 = new TagInfo("c:if", //$NON-NLS-1$
            83, 5,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currSetting/isDuplicateItem = 'false'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_86_5 = new TagInfo("c:if", //$NON-NLS-1$
            86, 5,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currSetting/isMovingItem = 'false'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_99_18 = new TagInfo("c:get", //$NON-NLS-1$
            99, 18,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_99_85 = new TagInfo("c:get", //$NON-NLS-1$
            99, 85,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_iterate_101_5 = new TagInfo("c:iterate", //$NON-NLS-1$
            101, 5,
            new String[] {
                "select", //$NON-NLS-1$
                "var", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/columnTable", //$NON-NLS-1$
                "columnTable", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_102_14 = new TagInfo("c:get", //$NON-NLS-1$
            102, 14,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/nameColumn", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_103_23 = new TagInfo("c:get", //$NON-NLS-1$
            103, 23,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/sizeColumn", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_103_114 = new TagInfo("c:get", //$NON-NLS-1$
            103, 114,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/typeColumn", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_103_167 = new TagInfo("c:get", //$NON-NLS-1$
            103, 167,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/resizableColumn", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_103_215 = new TagInfo("c:get", //$NON-NLS-1$
            103, 215,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/masquableColumn", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_113_41 = new TagInfo("c:get", //$NON-NLS-1$
            113, 41,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_choose_150_8 = new TagInfo("c:choose", //$NON-NLS-1$
            150, 8,
            new String[] {
            },
            new String[] {
            } );
    private static final TagInfo _td_c_when_151_10 = new TagInfo("c:when", //$NON-NLS-1$
            151, 10,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/settingsTable/isCellAndAssistant = 'CellAndAssistant'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_154_29 = new TagInfo("c:get", //$NON-NLS-1$
            154, 29,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_when_157_9 = new TagInfo("c:when", //$NON-NLS-1$
            157, 9,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/settingsTable/isCell = 'Cellule'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_160_29 = new TagInfo("c:get", //$NON-NLS-1$
            160, 29,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_168_40 = new TagInfo("c:get", //$NON-NLS-1$
            168, 40,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_169_16 = new TagInfo("c:get", //$NON-NLS-1$
            169, 16,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_iterate_174_3 = new TagInfo("c:iterate", //$NON-NLS-1$
            174, 3,
            new String[] {
                "select", //$NON-NLS-1$
                "var", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/settingsTable", //$NON-NLS-1$
                "currSetting", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_175_6 = new TagInfo("c:if", //$NON-NLS-1$
            175, 6,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currSetting/isAddItem = 'addItem'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_177_9 = new TagInfo("c:if", //$NON-NLS-1$
            177, 9,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/settingsTable/isAssistant = 'Assistant'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_178_28 = new TagInfo("c:get", //$NON-NLS-1$
            178, 28,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_180_34 = new TagInfo("c:get", //$NON-NLS-1$
            180, 34,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_183_6 = new TagInfo("c:if", //$NON-NLS-1$
            183, 6,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currSetting/isUpdateItem = 'updateItem'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_185_9 = new TagInfo("c:if", //$NON-NLS-1$
            185, 9,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/settingsTable/isAssistant = 'Assistant'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_186_26 = new TagInfo("c:get", //$NON-NLS-1$
            186, 26,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_187_31 = new TagInfo("c:get", //$NON-NLS-1$
            187, 31,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_189_9 = new TagInfo("c:if", //$NON-NLS-1$
            189, 9,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/settingsTable/isCell = 'Cellule'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_if_194_6 = new TagInfo("c:if", //$NON-NLS-1$
            194, 6,
            new String[] {
                "test", //$NON-NLS-1$
            },
            new String[] {
                "$currSetting/isDellItem = 'dellItem'", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_201_61 = new TagInfo("c:get", //$NON-NLS-1$
            201, 61,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_202_16 = new TagInfo("c:get", //$NON-NLS-1$
            202, 16,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_202_58 = new TagInfo("c:get", //$NON-NLS-1$
            202, 58,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_202_103 = new TagInfo("c:get", //$NON-NLS-1$
            202, 103,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_204_41 = new TagInfo("c:get", //$NON-NLS-1$
            204, 41,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_204_83 = new TagInfo("c:get", //$NON-NLS-1$
            204, 83,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_230_27 = new TagInfo("c:get", //$NON-NLS-1$
            230, 27,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_231_29 = new TagInfo("c:get", //$NON-NLS-1$
            231, 29,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_251_46 = new TagInfo("c:get", //$NON-NLS-1$
            251, 46,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_252_27 = new TagInfo("c:get", //$NON-NLS-1$
            252, 27,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );

    public void generate(final JET2Context context, final JET2Writer __out) {
        JET2Writer out = __out;
        out.write("package ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_1_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_1_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_1_9.setRuntimeParent(null);
        _jettag_c_get_1_9.setTagInfo(_td_c_get_1_9);
        _jettag_c_get_1_9.doStart(context, out);
        _jettag_c_get_1_9.doEnd();
        out.write(";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import org.eclipse.jface.dialogs.MessageDialog;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.layout.GridDataFactory;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.viewers.EditingSupport;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.viewers.StructuredSelection;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.viewers.TableViewerColumn;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.swt.SWT;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.swt.layout.GridData;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.swt.widgets.Composite;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.factory.ContentProvidersFactory;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.factory.TablesFactory;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.factory.layout.GridLayoutFactory;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.factory.wb.TableFactory;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.column.ColumnConfigurationAdapter;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.column.ColumnsContainerConfigurator;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.column.IColumnPopupManager;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.table.UpdatableTableViewerViaTableCells;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.updatableviewer.UpdatableItem;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.updatableviewer.listener.AddItemEvent;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.updatableviewer.listener.AddItemListener;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.updatableviewer.listener.RemoveItemsEvent;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.updatableviewer.listener.RemoveItemsListener;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.updatableviewer.listener.UpdateManyItemsEvent;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.updatableviewer.listener.UpdateManyItemsListener;");  //$NON-NLS-1$        
        out.write(NL);         
        RuntimeTagElement _jettag_c_setVariable_27_1 = context.getTagFactory().createRuntimeTag(_jetns_c, "setVariable", "c:setVariable", _td_c_setVariable_27_1); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_setVariable_27_1.setRuntimeParent(null);
        _jettag_c_setVariable_27_1.setTagInfo(_td_c_setVariable_27_1);
        _jettag_c_setVariable_27_1.doStart(context, out);
        _jettag_c_setVariable_27_1.doEnd();
        RuntimeTagElement _jettag_c_setVariable_28_1 = context.getTagFactory().createRuntimeTag(_jetns_c, "setVariable", "c:setVariable", _td_c_setVariable_28_1); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_setVariable_28_1.setRuntimeParent(null);
        _jettag_c_setVariable_28_1.setTagInfo(_td_c_setVariable_28_1);
        _jettag_c_setVariable_28_1.doStart(context, out);
        _jettag_c_setVariable_28_1.doEnd();
        out.write("/**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * @author by ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_30_15 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_30_15); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_30_15.setRuntimeParent(null);
        _jettag_c_get_30_15.setTagInfo(_td_c_get_30_15);
        _jettag_c_get_30_15.doStart(context, out);
        _jettag_c_get_30_15.doEnd();
        out.write(NL);         
        out.write(" *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" public class Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_33_22 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_33_22); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_33_22.setRuntimeParent(null);
        _jettag_c_get_33_22.setTagInfo(_td_c_get_33_22);
        _jettag_c_get_33_22.doStart(context, out);
        _jettag_c_get_33_22.doEnd();
        out.write(" {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write(" \t//TODO Nommer l'id par exemple si votre tableau se nomme tableauGestionFrais : TABLEAU_GESTION_FRAIS_ID");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" \tprivate static final String TABLEAU_ID = \"");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_36_45 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_36_45); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_36_45.setRuntimeParent(null);
        _jettag_c_get_36_45.setTagInfo(_td_c_get_36_45);
        _jettag_c_get_36_45.doStart(context, out);
        _jettag_c_get_36_45.doEnd();
        out.write(".Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_36_95 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_36_95); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_36_95.setRuntimeParent(null);
        _jettag_c_get_36_95.setTagInfo(_td_c_get_36_95);
        _jettag_c_get_36_95.doStart(context, out);
        _jettag_c_get_36_95.doEnd();
        out.write("\";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\tprivate ControleurTableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_38_27 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_38_27); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_38_27.setRuntimeParent(null);
        _jettag_c_get_38_27.setTagInfo(_td_c_get_38_27);
        _jettag_c_get_38_27.doStart(context, out);
        _jettag_c_get_38_27.doEnd();
        out.write(" controleurTableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_38_84 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_38_84); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_38_84.setRuntimeParent(null);
        _jettag_c_get_38_84.setTagInfo(_td_c_get_38_84);
        _jettag_c_get_38_84.doStart(context, out);
        _jettag_c_get_38_84.doEnd();
        out.write(";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\tprivate UpdatableTableViewerViaTableCells updatableTableViewerViaTableCells;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\tprivate IColumnPopupManager popup;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\tpublic Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_43_16 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_43_16); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_43_16.setRuntimeParent(null);
        _jettag_c_get_43_16.setTagInfo(_td_c_get_43_16);
        _jettag_c_get_43_16.doStart(context, out);
        _jettag_c_get_43_16.doEnd();
        out.write("(Composite parent,Controleur controleur){ // controleur principal");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\tsetControleur(new ControleurTableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_44_38 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_44_38); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_44_38.setRuntimeParent(null);
        _jettag_c_get_44_38.setTagInfo(_td_c_get_44_38);
        _jettag_c_get_44_38.doStart(context, out);
        _jettag_c_get_44_38.doEnd();
        out.write("(controleur, this));");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\tcreateUpdateTableViewer(parent);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\taddListeners();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  /**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * <p>");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * Méthode permettant de créer le tableau modifiable et d'initialiser ses attributs.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * </p>");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\tprivate void createUpdateTableViewer(Composite parent){");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\tComposite composite = new Composite(parent, SWT.NONE);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    GridDataFactory.fillDefaults().grab(true, false).applyTo(composite);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    composite.setLayout(GridLayoutFactory.getGridLayoutNoMargin(1));");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("    updatableTableViewerViaTableCells = TableFactory.getUpdatableTableViewerViaTableCells(composite, SWT.CENTER  ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_if_59_114 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_59_114); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_if_59_114.setRuntimeParent(null);
        _jettag_c_if_59_114.setTagInfo(_td_c_if_59_114);
        _jettag_c_if_59_114.doStart(context, out);
        while (_jettag_c_if_59_114.okToProcessBody()) {
            out.write("| SWT.MULTI ");  //$NON-NLS-1$        
            _jettag_c_if_59_114.handleBodyContent(out);
        }
        _jettag_c_if_59_114.doEnd();
        out.write("| SWT.FULL_SELECTION,");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        new int[] {");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_choose_60_20 = context.getTagFactory().createRuntimeTag(_jetns_c, "choose", "c:choose", _td_c_choose_60_20); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_choose_60_20.setRuntimeParent(null);
        _jettag_c_choose_60_20.setTagInfo(_td_c_choose_60_20);
        _jettag_c_choose_60_20.doStart(context, out);
        JET2Writer _jettag_c_choose_60_20_saved_out = out;
        while (_jettag_c_choose_60_20.okToProcessBody()) {
            out = out.newNestedContentWriter();
            RuntimeTagElement _jettag_c_when_60_30 = context.getTagFactory().createRuntimeTag(_jetns_c, "when", "c:when", _td_c_when_60_30); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_when_60_30.setRuntimeParent(_jettag_c_choose_60_20);
            _jettag_c_when_60_30.setTagInfo(_td_c_when_60_30);
            _jettag_c_when_60_30.doStart(context, out);
            JET2Writer _jettag_c_when_60_30_saved_out = out;
            while (_jettag_c_when_60_30.okToProcessBody()) {
                out = out.newNestedContentWriter();
                RuntimeTagElement _jettag_c_iterate_60_117 = context.getTagFactory().createRuntimeTag(_jetns_c, "iterate", "c:iterate", _td_c_iterate_60_117); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_iterate_60_117.setRuntimeParent(_jettag_c_when_60_30);
                _jettag_c_iterate_60_117.setTagInfo(_td_c_iterate_60_117);
                _jettag_c_iterate_60_117.doStart(context, out);
                while (_jettag_c_iterate_60_117.okToProcessBody()) {
                    RuntimeTagElement _jettag_c_get_60_209 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_60_209); //$NON-NLS-1$ //$NON-NLS-2$
                    _jettag_c_get_60_209.setRuntimeParent(_jettag_c_iterate_60_117);
                    _jettag_c_get_60_209.setTagInfo(_td_c_get_60_209);
                    _jettag_c_get_60_209.doStart(context, out);
                    _jettag_c_get_60_209.doEnd();
                    RuntimeTagElement _jettag_c_if_60_243 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_60_243); //$NON-NLS-1$ //$NON-NLS-2$
                    _jettag_c_if_60_243.setRuntimeParent(_jettag_c_iterate_60_117);
                    _jettag_c_if_60_243.setTagInfo(_td_c_if_60_243);
                    _jettag_c_if_60_243.doStart(context, out);
                    while (_jettag_c_if_60_243.okToProcessBody()) {
                        out.write(",");  //$NON-NLS-1$        
                        _jettag_c_if_60_243.handleBodyContent(out);
                    }
                    _jettag_c_if_60_243.doEnd();
                    RuntimeTagElement _jettag_c_setVariable_60_297 = context.getTagFactory().createRuntimeTag(_jetns_c, "setVariable", "c:setVariable", _td_c_setVariable_60_297); //$NON-NLS-1$ //$NON-NLS-2$
                    _jettag_c_setVariable_60_297.setRuntimeParent(_jettag_c_iterate_60_117);
                    _jettag_c_setVariable_60_297.setTagInfo(_td_c_setVariable_60_297);
                    _jettag_c_setVariable_60_297.doStart(context, out);
                    _jettag_c_setVariable_60_297.doEnd();
                    _jettag_c_iterate_60_117.handleBodyContent(out);
                }
                _jettag_c_iterate_60_117.doEnd();
                _jettag_c_when_60_30.handleBodyContent(out);
            }
            out = _jettag_c_when_60_30_saved_out;
            _jettag_c_when_60_30.doEnd();
            RuntimeTagElement _jettag_c_when_60_383 = context.getTagFactory().createRuntimeTag(_jetns_c, "when", "c:when", _td_c_when_60_383); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_when_60_383.setRuntimeParent(_jettag_c_choose_60_20);
            _jettag_c_when_60_383.setTagInfo(_td_c_when_60_383);
            _jettag_c_when_60_383.doStart(context, out);
            JET2Writer _jettag_c_when_60_383_saved_out = out;
            while (_jettag_c_when_60_383.okToProcessBody()) {
                out = out.newNestedContentWriter();
                RuntimeTagElement _jettag_c_iterate_60_449 = context.getTagFactory().createRuntimeTag(_jetns_c, "iterate", "c:iterate", _td_c_iterate_60_449); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_iterate_60_449.setRuntimeParent(_jettag_c_when_60_383);
                _jettag_c_iterate_60_449.setTagInfo(_td_c_iterate_60_449);
                _jettag_c_iterate_60_449.doStart(context, out);
                while (_jettag_c_iterate_60_449.okToProcessBody()) {
                    RuntimeTagElement _jettag_c_get_60_541 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_60_541); //$NON-NLS-1$ //$NON-NLS-2$
                    _jettag_c_get_60_541.setRuntimeParent(_jettag_c_iterate_60_449);
                    _jettag_c_get_60_541.setTagInfo(_td_c_get_60_541);
                    _jettag_c_get_60_541.doStart(context, out);
                    _jettag_c_get_60_541.doEnd();
                    RuntimeTagElement _jettag_c_if_60_575 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_60_575); //$NON-NLS-1$ //$NON-NLS-2$
                    _jettag_c_if_60_575.setRuntimeParent(_jettag_c_iterate_60_449);
                    _jettag_c_if_60_575.setTagInfo(_td_c_if_60_575);
                    _jettag_c_if_60_575.doStart(context, out);
                    while (_jettag_c_if_60_575.okToProcessBody()) {
                        out.write(",");  //$NON-NLS-1$        
                        _jettag_c_if_60_575.handleBodyContent(out);
                    }
                    _jettag_c_if_60_575.doEnd();
                    RuntimeTagElement _jettag_c_setVariable_60_629 = context.getTagFactory().createRuntimeTag(_jetns_c, "setVariable", "c:setVariable", _td_c_setVariable_60_629); //$NON-NLS-1$ //$NON-NLS-2$
                    _jettag_c_setVariable_60_629.setRuntimeParent(_jettag_c_iterate_60_449);
                    _jettag_c_setVariable_60_629.setTagInfo(_td_c_setVariable_60_629);
                    _jettag_c_setVariable_60_629.doStart(context, out);
                    _jettag_c_setVariable_60_629.doEnd();
                    _jettag_c_iterate_60_449.handleBodyContent(out);
                }
                _jettag_c_iterate_60_449.doEnd();
                out.write(" ");  //$NON-NLS-1$        
                _jettag_c_when_60_383.handleBodyContent(out);
            }
            out = _jettag_c_when_60_383_saved_out;
            _jettag_c_when_60_383.doEnd();
            _jettag_c_choose_60_20.handleBodyContent(out);
        }
        out = _jettag_c_choose_60_20_saved_out;
        _jettag_c_choose_60_20.doEnd();
        out.write("});");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("    updatableTableViewerViaTableCells.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("    updatableTableViewerViaTableCells.getTable().setHeaderVisible(true);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    updatableTableViewerViaTableCells.getTable().setLinesVisible(true);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("    creerColonnes();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\tupdatableTableViewerViaTableCells.setContentProvider(ContentProvidersFactory.getTableListViewerContentProvider());");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        RuntimeTagElement _jettag_c_iterate_70_3 = context.getTagFactory().createRuntimeTag(_jetns_c, "iterate", "c:iterate", _td_c_iterate_70_3); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_iterate_70_3.setRuntimeParent(null);
        _jettag_c_iterate_70_3.setTagInfo(_td_c_iterate_70_3);
        _jettag_c_iterate_70_3.doStart(context, out);
        while (_jettag_c_iterate_70_3.okToProcessBody()) {
            RuntimeTagElement _jettag_c_if_71_5 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_71_5); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_if_71_5.setRuntimeParent(_jettag_c_iterate_70_3);
            _jettag_c_if_71_5.setTagInfo(_td_c_if_71_5);
            _jettag_c_if_71_5.doStart(context, out);
            while (_jettag_c_if_71_5.okToProcessBody()) {
                out.write("\t\t\t\t\tupdatableTableViewerViaTableCells.setToolbarAddItemVisible(false);");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_if_71_5.handleBodyContent(out);
            }
            _jettag_c_if_71_5.doEnd();
            RuntimeTagElement _jettag_c_if_74_5 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_74_5); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_if_74_5.setRuntimeParent(_jettag_c_iterate_70_3);
            _jettag_c_if_74_5.setTagInfo(_td_c_if_74_5);
            _jettag_c_if_74_5.doStart(context, out);
            while (_jettag_c_if_74_5.okToProcessBody()) {
                out.write("\t\t\t\t\tupdatableTableViewerViaTableCells.setToolbarUpdateItemVisible(false);");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_if_74_5.handleBodyContent(out);
            }
            _jettag_c_if_74_5.doEnd();
            RuntimeTagElement _jettag_c_if_77_5 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_77_5); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_if_77_5.setRuntimeParent(_jettag_c_iterate_70_3);
            _jettag_c_if_77_5.setTagInfo(_td_c_if_77_5);
            _jettag_c_if_77_5.doStart(context, out);
            while (_jettag_c_if_77_5.okToProcessBody()) {
                out.write("\t\t\t\t\tupdatableTableViewerViaTableCells.setToolbarUpdateManyItemVisible(false);");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_if_77_5.handleBodyContent(out);
            }
            _jettag_c_if_77_5.doEnd();
            RuntimeTagElement _jettag_c_if_80_5 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_80_5); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_if_80_5.setRuntimeParent(_jettag_c_iterate_70_3);
            _jettag_c_if_80_5.setTagInfo(_td_c_if_80_5);
            _jettag_c_if_80_5.doStart(context, out);
            while (_jettag_c_if_80_5.okToProcessBody()) {
                out.write("\t\t\t\t\tupdatableTableViewerViaTableCells.setToolbarRemoveItemVisible(false);");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_if_80_5.handleBodyContent(out);
            }
            _jettag_c_if_80_5.doEnd();
            RuntimeTagElement _jettag_c_if_83_5 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_83_5); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_if_83_5.setRuntimeParent(_jettag_c_iterate_70_3);
            _jettag_c_if_83_5.setTagInfo(_td_c_if_83_5);
            _jettag_c_if_83_5.doStart(context, out);
            while (_jettag_c_if_83_5.okToProcessBody()) {
                out.write("\t\t\t\t\tupdatableTableViewerViaTableCells.setToolbarDuplicateItemVisible(false);");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_if_83_5.handleBodyContent(out);
            }
            _jettag_c_if_83_5.doEnd();
            RuntimeTagElement _jettag_c_if_86_5 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_86_5); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_if_86_5.setRuntimeParent(_jettag_c_iterate_70_3);
            _jettag_c_if_86_5.setTagInfo(_td_c_if_86_5);
            _jettag_c_if_86_5.doStart(context, out);
            while (_jettag_c_if_86_5.okToProcessBody()) {
                out.write("\t\t\t\t\tupdatableTableViewerViaTableCells.setToolbarBringUpAndDownItemVisible(false);");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_if_86_5.handleBodyContent(out);
            }
            _jettag_c_if_86_5.doEnd();
            _jettag_c_iterate_70_3.handleBodyContent(out);
        }
        _jettag_c_iterate_70_3.doEnd();
        out.write(NL);         
        out.write("\t\tgererPopupMenu();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t/**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   \t* Création des colonnes du tableau.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   \t*/");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  \tprivate void creerColonnes() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t    for (Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_99_18 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_99_18); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_99_18.setRuntimeParent(null);
        _jettag_c_get_99_18.setTagInfo(_td_c_get_99_18);
        _jettag_c_get_99_18.doStart(context, out);
        _jettag_c_get_99_18.doEnd();
        out.write("ColumnEnum colonne : Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_99_85 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_99_85); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_99_85.setRuntimeParent(null);
        _jettag_c_get_99_85.setTagInfo(_td_c_get_99_85);
        _jettag_c_get_99_85.doStart(context, out);
        _jettag_c_get_99_85.doEnd();
        out.write("ColumnEnum.values()) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t    \tswitch(colonne){");  //$NON-NLS-1$        
        out.write(NL);         
        RuntimeTagElement _jettag_c_iterate_101_5 = context.getTagFactory().createRuntimeTag(_jetns_c, "iterate", "c:iterate", _td_c_iterate_101_5); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_iterate_101_5.setRuntimeParent(null);
        _jettag_c_iterate_101_5.setTagInfo(_td_c_iterate_101_5);
        _jettag_c_iterate_101_5.doStart(context, out);
        while (_jettag_c_iterate_101_5.okToProcessBody()) {
            out.write("    \t\t\t\tcase ");  //$NON-NLS-1$        
            RuntimeTagElement _jettag_c_get_102_14 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_102_14); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_get_102_14.setRuntimeParent(_jettag_c_iterate_101_5);
            _jettag_c_get_102_14.setTagInfo(_td_c_get_102_14);
            _jettag_c_get_102_14.doStart(context, out);
            _jettag_c_get_102_14.doEnd();
            out.write(":");  //$NON-NLS-1$        
            out.write(NL);         
            out.write("\t    \t\t\t\tcreerColonne(");  //$NON-NLS-1$        
            RuntimeTagElement _jettag_c_get_103_23 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_103_23); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_get_103_23.setRuntimeParent(_jettag_c_iterate_101_5);
            _jettag_c_get_103_23.setTagInfo(_td_c_get_103_23);
            _jettag_c_get_103_23.doStart(context, out);
            _jettag_c_get_103_23.doEnd();
            out.write(", colonne.getLibelle(), colonne.getTooltipText(), ");  //$NON-NLS-1$        
            RuntimeTagElement _jettag_c_get_103_114 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_103_114); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_get_103_114.setRuntimeParent(_jettag_c_iterate_101_5);
            _jettag_c_get_103_114.setTagInfo(_td_c_get_103_114);
            _jettag_c_get_103_114.doStart(context, out);
            _jettag_c_get_103_114.doEnd();
            out.write(" , colonne, ");  //$NON-NLS-1$        
            RuntimeTagElement _jettag_c_get_103_167 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_103_167); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_get_103_167.setRuntimeParent(_jettag_c_iterate_101_5);
            _jettag_c_get_103_167.setTagInfo(_td_c_get_103_167);
            _jettag_c_get_103_167.doStart(context, out);
            _jettag_c_get_103_167.doEnd();
            out.write(", ");  //$NON-NLS-1$        
            RuntimeTagElement _jettag_c_get_103_215 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_103_215); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_get_103_215.setRuntimeParent(_jettag_c_iterate_101_5);
            _jettag_c_get_103_215.setTagInfo(_td_c_get_103_215);
            _jettag_c_get_103_215.doStart(context, out);
            _jettag_c_get_103_215.doEnd();
            out.write(");");  //$NON-NLS-1$        
            out.write(NL);         
            out.write("\t    \t\t\t\tbreak;");  //$NON-NLS-1$        
            out.write(NL);         
            _jettag_c_iterate_101_5.handleBodyContent(out);
        }
        _jettag_c_iterate_101_5.doEnd();
        out.write("\t\t\t\t\tdefault:");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t\t\t\tbreak;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t    \t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t    }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("    private TableViewerColumn creerColonne(final int taille, final String titre, final String tooltipText,");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      final Integer style, final Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_113_41 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_113_41); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_113_41.setRuntimeParent(null);
        _jettag_c_get_113_41.setTagInfo(_td_c_get_113_41);
        _jettag_c_get_113_41.doStart(context, out);
        _jettag_c_get_113_41.doEnd();
        out.write("ColumnEnum colonne, final boolean resizable,");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      final boolean masquable) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    TableViewerColumn column = TablesFactory.getTableViewerColumn(this.updatableTableViewerViaTableCells.getTableViewer(), new ColumnConfigurationAdapter(");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        taille, titre) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("      @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      public String getToolTipText() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        return tooltipText;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("      @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      public boolean canHide() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        return masquable;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("      @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      public int getStyle() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        return style;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("      @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      public boolean isMoveable() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        return true;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("      @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      public boolean isResizable() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        return resizable;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("      @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      public String getText() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        return titre;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        RuntimeTagElement _jettag_c_choose_150_8 = context.getTagFactory().createRuntimeTag(_jetns_c, "choose", "c:choose", _td_c_choose_150_8); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_choose_150_8.setRuntimeParent(null);
        _jettag_c_choose_150_8.setTagInfo(_td_c_choose_150_8);
        _jettag_c_choose_150_8.doStart(context, out);
        JET2Writer _jettag_c_choose_150_8_saved_out = out;
        while (_jettag_c_choose_150_8.okToProcessBody()) {
            out = out.newNestedContentWriter();
            RuntimeTagElement _jettag_c_when_151_10 = context.getTagFactory().createRuntimeTag(_jetns_c, "when", "c:when", _td_c_when_151_10); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_when_151_10.setRuntimeParent(_jettag_c_choose_150_8);
            _jettag_c_when_151_10.setTagInfo(_td_c_when_151_10);
            _jettag_c_when_151_10.doStart(context, out);
            JET2Writer _jettag_c_when_151_10_saved_out = out;
            while (_jettag_c_when_151_10.okToProcessBody()) {
                out = out.newNestedContentWriter();
                out.write("        @Override");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("        public EditingSupport getEditingSupport() {");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("          return new Tableau");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_154_29 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_154_29); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_154_29.setRuntimeParent(_jettag_c_when_151_10);
                _jettag_c_get_154_29.setTagInfo(_td_c_get_154_29);
                _jettag_c_get_154_29.doStart(context, out);
                _jettag_c_get_154_29.doEnd();
                out.write("EditingSupport(getUtv().getTableViewer(), colonne);");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("        }");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_when_151_10.handleBodyContent(out);
            }
            out = _jettag_c_when_151_10_saved_out;
            _jettag_c_when_151_10.doEnd();
            RuntimeTagElement _jettag_c_when_157_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "when", "c:when", _td_c_when_157_9); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_when_157_9.setRuntimeParent(_jettag_c_choose_150_8);
            _jettag_c_when_157_9.setTagInfo(_td_c_when_157_9);
            _jettag_c_when_157_9.doStart(context, out);
            JET2Writer _jettag_c_when_157_9_saved_out = out;
            while (_jettag_c_when_157_9.okToProcessBody()) {
                out = out.newNestedContentWriter();
                out.write("\t      @Override");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("        public EditingSupport getEditingSupport() {");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("          return new Tableau");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_160_29 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_160_29); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_160_29.setRuntimeParent(_jettag_c_when_157_9);
                _jettag_c_get_160_29.setTagInfo(_td_c_get_160_29);
                _jettag_c_get_160_29.doStart(context, out);
                _jettag_c_get_160_29.doEnd();
                out.write("EditingSupport(getUtv().getTableViewer(), colonne);");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("        }");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_when_157_9.handleBodyContent(out);
            }
            out = _jettag_c_when_157_9_saved_out;
            _jettag_c_when_157_9.doEnd();
            _jettag_c_choose_150_8.handleBodyContent(out);
        }
        out = _jettag_c_choose_150_8_saved_out;
        _jettag_c_choose_150_8.doEnd();
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        out.write("    });");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    column.setLabelProvider(new Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_168_40 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_168_40); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_168_40.setRuntimeParent(null);
        _jettag_c_get_168_40.setTagInfo(_td_c_get_168_40);
        _jettag_c_get_168_40.doStart(context, out);
        _jettag_c_get_168_40.doEnd();
        out.write("LabelProvider(colonne));");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    new Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_169_16 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_169_16); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_169_16.setRuntimeParent(null);
        _jettag_c_get_169_16.setTagInfo(_td_c_get_169_16);
        _jettag_c_get_169_16.doStart(context, out);
        _jettag_c_get_169_16.doEnd();
        out.write("Sorter(getUtv().getTableViewer(), column, colonne);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return column;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\tpublic void addListeners(){");  //$NON-NLS-1$        
        out.write(NL);         
        RuntimeTagElement _jettag_c_iterate_174_3 = context.getTagFactory().createRuntimeTag(_jetns_c, "iterate", "c:iterate", _td_c_iterate_174_3); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_iterate_174_3.setRuntimeParent(null);
        _jettag_c_iterate_174_3.setTagInfo(_td_c_iterate_174_3);
        _jettag_c_iterate_174_3.doStart(context, out);
        while (_jettag_c_iterate_174_3.okToProcessBody()) {
            RuntimeTagElement _jettag_c_if_175_6 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_175_6); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_if_175_6.setRuntimeParent(_jettag_c_iterate_174_3);
            _jettag_c_if_175_6.setTagInfo(_td_c_if_175_6);
            _jettag_c_if_175_6.doStart(context, out);
            while (_jettag_c_if_175_6.okToProcessBody()) {
                out.write("\t\t\t\t\t\tthis.updatableTableViewerViaTableCells.addAddItemListener(event -> {");  //$NON-NLS-1$        
                out.write(NL);         
                RuntimeTagElement _jettag_c_if_177_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_177_9); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_if_177_9.setRuntimeParent(_jettag_c_if_175_6);
                _jettag_c_if_177_9.setTagInfo(_td_c_if_177_9);
                _jettag_c_if_177_9.doStart(context, out);
                while (_jettag_c_if_177_9.okToProcessBody()) {
                    out.write("    \t\t\t\t\t\tcontroleurTableau");  //$NON-NLS-1$        
                    RuntimeTagElement _jettag_c_get_178_28 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_178_28); //$NON-NLS-1$ //$NON-NLS-2$
                    _jettag_c_get_178_28.setRuntimeParent(_jettag_c_if_177_9);
                    _jettag_c_get_178_28.setTagInfo(_td_c_get_178_28);
                    _jettag_c_get_178_28.doStart(context, out);
                    _jettag_c_get_178_28.doEnd();
                    out.write(".setModeCreation(true);");  //$NON-NLS-1$        
                    out.write(NL);         
                    _jettag_c_if_177_9.handleBodyContent(out);
                }
                _jettag_c_if_177_9.doEnd();
                out.write("\t\t\t\t\t\t\t\tgetControleur().createNew");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_180_34 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_180_34); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_180_34.setRuntimeParent(_jettag_c_if_175_6);
                _jettag_c_get_180_34.setTagInfo(_td_c_get_180_34);
                _jettag_c_get_180_34.doStart(context, out);
                _jettag_c_get_180_34.doEnd();
                out.write("();");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t\t\t\t\t });");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_if_175_6.handleBodyContent(out);
            }
            _jettag_c_if_175_6.doEnd();
            RuntimeTagElement _jettag_c_if_183_6 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_183_6); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_if_183_6.setRuntimeParent(_jettag_c_iterate_174_3);
            _jettag_c_if_183_6.setTagInfo(_td_c_if_183_6);
            _jettag_c_if_183_6.doStart(context, out);
            while (_jettag_c_if_183_6.okToProcessBody()) {
                out.write("\t\t\t\t\t\tthis.updatableTableViewerViaTableCells.addUpdateItemsListener(event -> {");  //$NON-NLS-1$        
                out.write(NL);         
                RuntimeTagElement _jettag_c_if_185_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_185_9); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_if_185_9.setRuntimeParent(_jettag_c_if_183_6);
                _jettag_c_if_185_9.setTagInfo(_td_c_if_185_9);
                _jettag_c_if_185_9.doStart(context, out);
                while (_jettag_c_if_185_9.okToProcessBody()) {
                    out.write("\t\t\t\t\t\t\t\tcontroleurTableau");  //$NON-NLS-1$        
                    RuntimeTagElement _jettag_c_get_186_26 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_186_26); //$NON-NLS-1$ //$NON-NLS-2$
                    _jettag_c_get_186_26.setRuntimeParent(_jettag_c_if_185_9);
                    _jettag_c_get_186_26.setTagInfo(_td_c_get_186_26);
                    _jettag_c_get_186_26.doStart(context, out);
                    _jettag_c_get_186_26.doEnd();
                    out.write(".setModeCreation(false);");  //$NON-NLS-1$        
                    out.write(NL);         
                    out.write("\t\t\t\t\t\t\t\tgetControleur().modify");  //$NON-NLS-1$        
                    RuntimeTagElement _jettag_c_get_187_31 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_187_31); //$NON-NLS-1$ //$NON-NLS-2$
                    _jettag_c_get_187_31.setRuntimeParent(_jettag_c_if_185_9);
                    _jettag_c_get_187_31.setTagInfo(_td_c_get_187_31);
                    _jettag_c_get_187_31.doStart(context, out);
                    _jettag_c_get_187_31.doEnd();
                    out.write("();");  //$NON-NLS-1$        
                    out.write(NL);         
                    _jettag_c_if_185_9.handleBodyContent(out);
                }
                _jettag_c_if_185_9.doEnd();
                RuntimeTagElement _jettag_c_if_189_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_189_9); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_if_189_9.setRuntimeParent(_jettag_c_if_183_6);
                _jettag_c_if_189_9.setTagInfo(_td_c_if_189_9);
                _jettag_c_if_189_9.doStart(context, out);
                while (_jettag_c_if_189_9.okToProcessBody()) {
                    out.write("    \t\t\t\t\t\tupdatableTableViewerViaTableCells.activateInput();");  //$NON-NLS-1$        
                    out.write(NL);         
                    _jettag_c_if_189_9.handleBodyContent(out);
                }
                _jettag_c_if_189_9.doEnd();
                out.write("\t\t\t\t\t\t });");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_if_183_6.handleBodyContent(out);
            }
            _jettag_c_if_183_6.doEnd();
            RuntimeTagElement _jettag_c_if_194_6 = context.getTagFactory().createRuntimeTag(_jetns_c, "if", "c:if", _td_c_if_194_6); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_if_194_6.setRuntimeParent(_jettag_c_iterate_174_3);
            _jettag_c_if_194_6.setTagInfo(_td_c_if_194_6);
            _jettag_c_if_194_6.doStart(context, out);
            while (_jettag_c_if_194_6.okToProcessBody()) {
                out.write("\t\t\t\t\t\tthis.updatableTableViewerViaTableCells.addRemoveItemsListener(event -> {");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t\t\t\t      //TODO Pensez à externaliser les chaînes de caractères");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t\t\t\t      boolean deleteOk = MessageDialog.openConfirm(null, \"Confirmation\",\"Confirmez-vous la suppression ?\");");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t\t\t\t      if(deleteOk){");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t\t\t\t    \t StructuredSelection selection = (StructuredSelection) event.getSelection();");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t\t\t\t         UpdatableItem item = (UpdatableItem) selection.getFirstElement();");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t\t\t\t         if (item != null && item.getItem() instanceof ");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_201_61 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_201_61); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_201_61.setRuntimeParent(_jettag_c_if_194_6);
                _jettag_c_get_201_61.setTagInfo(_td_c_get_201_61);
                _jettag_c_get_201_61.doStart(context, out);
                _jettag_c_get_201_61.doEnd();
                out.write(") {");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t\t\t\t        \t ");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_202_16 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_202_16); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_202_16.setRuntimeParent(_jettag_c_if_194_6);
                _jettag_c_get_202_16.setTagInfo(_td_c_get_202_16);
                _jettag_c_get_202_16.doStart(context, out);
                _jettag_c_get_202_16.doEnd();
                out.write(" ");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_202_58 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_202_58); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_202_58.setRuntimeParent(_jettag_c_if_194_6);
                _jettag_c_get_202_58.setTagInfo(_td_c_get_202_58);
                _jettag_c_get_202_58.doStart(context, out);
                _jettag_c_get_202_58.doEnd();
                out.write(" = (");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_202_103 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_202_103); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_202_103.setRuntimeParent(_jettag_c_if_194_6);
                _jettag_c_get_202_103.setTagInfo(_td_c_get_202_103);
                _jettag_c_get_202_103.doStart(context, out);
                _jettag_c_get_202_103.doEnd();
                out.write(") item.getItem();");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t\t\t\t             updatableTableViewerViaTableCells.removeItem(item);");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t\t\t\t             getControleur().delete");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_204_41 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_204_41); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_204_41.setRuntimeParent(_jettag_c_if_194_6);
                _jettag_c_get_204_41.setTagInfo(_td_c_get_204_41);
                _jettag_c_get_204_41.doStart(context, out);
                _jettag_c_get_204_41.doEnd();
                out.write("(");  //$NON-NLS-1$        
                RuntimeTagElement _jettag_c_get_204_83 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_204_83); //$NON-NLS-1$ //$NON-NLS-2$
                _jettag_c_get_204_83.setRuntimeParent(_jettag_c_if_194_6);
                _jettag_c_get_204_83.setTagInfo(_td_c_get_204_83);
                _jettag_c_get_204_83.doStart(context, out);
                _jettag_c_get_204_83.doEnd();
                out.write(");");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t\t\t\t             updatableTableViewerViaTableCells.getTableViewer().refresh();");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t\t\t\t           }");  //$NON-NLS-1$        
                out.write(NL);         
                out.write(NL);         
                out.write("\t\t\t\t\t      }");  //$NON-NLS-1$        
                out.write(NL);         
                out.write("\t\t\t\t\t\t });");  //$NON-NLS-1$        
                out.write(NL);         
                _jettag_c_if_194_6.handleBodyContent(out);
            }
            _jettag_c_if_194_6.doEnd();
            _jettag_c_iterate_174_3.handleBodyContent(out);
        }
        _jettag_c_iterate_174_3.doEnd();
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t/**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    * Gère la popup du tableau");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    //TODO Changer la variable TABLEAU_ID et choisir l'activator qui correspond à votre domaine.");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  \tprivate void gererPopupMenu() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    popup = ColumnsContainerConfigurator.getColumnPopupManager(TABLEAU_ID, updatableTableViewerViaTableCells.getTableViewer());");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    popup.createAdditionnalMenuExtensions();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    ColumnsContainerConfigurator.managePreferences(TABLEAU_ID,");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        Activator.getDefault().getPreferenceStore(), updatableTableViewerViaTableCells.getTableViewer());");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        out.write("  /**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @return the controleur");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public ControleurTableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_230_27 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_230_27); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_230_27.setRuntimeParent(null);
        _jettag_c_get_230_27.setTagInfo(_td_c_get_230_27);
        _jettag_c_get_230_27.doStart(context, out);
        _jettag_c_get_230_27.doEnd();
        out.write(" getControleur() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return controleurTableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_231_29 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_231_29); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_231_29.setRuntimeParent(null);
        _jettag_c_get_231_29.setTagInfo(_td_c_get_231_29);
        _jettag_c_get_231_29.doStart(context, out);
        _jettag_c_get_231_29.doEnd();
        out.write(";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  /**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @return the utv");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public UpdatableTableViewerViaTableCells getUtv() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return updatableTableViewerViaTableCells;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  /**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @param utv the utv to set");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public void setUtv(UpdatableTableViewerViaTableCells utv) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    this.updatableTableViewerViaTableCells = utv;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  /**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @param controleur the controleur to set");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public void setControleur(ControleurTableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_251_46 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_251_46); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_251_46.setRuntimeParent(null);
        _jettag_c_get_251_46.setTagInfo(_td_c_get_251_46);
        _jettag_c_get_251_46.doStart(context, out);
        _jettag_c_get_251_46.doEnd();
        out.write(" controleur) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    this.controleurTableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_252_27 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_252_27); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_252_27.setRuntimeParent(null);
        _jettag_c_get_252_27.setTagInfo(_td_c_get_252_27);
        _jettag_c_get_252_27.doStart(context, out);
        _jettag_c_get_252_27.doEnd();
        out.write(" = controleur;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  /**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @return the popup");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public IColumnPopupManager getPopup() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return popup;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  /**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   * @param popup the popup to set");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("   */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public void setPopup(IColumnPopupManager popup) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    this.popup = popup;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write(" }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
    }
}

package com.sneda.dev.helpers.jet.compiled;

import org.eclipse.jet.JET2Context;
import org.eclipse.jet.JET2Template;
import org.eclipse.jet.JET2Writer;
import org.eclipse.jet.taglib.RuntimeTagElement;
import org.eclipse.jet.taglib.TagInfo;

public class _jet_TableauModifiableMyNameSorterjava implements JET2Template {
    private static final String _jetns_c = "org.eclipse.jet.controlTags"; //$NON-NLS-1$

    public _jet_TableauModifiableMyNameSorterjava() {
        super();
    }

    private static final String NL = System.getProperty("line.separator"); //$NON-NLS-1$
    
    private static final TagInfo _td_c_get_1_9 = new TagInfo("c:get", //$NON-NLS-1$
            1, 9,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/package", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_17_12 = new TagInfo("c:get", //$NON-NLS-1$
            17, 12,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "/input/project/author", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_20_22 = new TagInfo("c:get", //$NON-NLS-1$
            20, 22,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_22_23 = new TagInfo("c:get", //$NON-NLS-1$
            22, 23,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_25_16 = new TagInfo("c:get", //$NON-NLS-1$
            25, 16,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_25_118 = new TagInfo("c:get", //$NON-NLS-1$
            25, 118,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_36_34 = new TagInfo("c:get", //$NON-NLS-1$
            36, 34,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_36_106 = new TagInfo("c:get", //$NON-NLS-1$
            36, 106,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_37_5 = new TagInfo("c:get", //$NON-NLS-1$
            37, 5,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_37_55 = new TagInfo("c:get", //$NON-NLS-1$
            37, 55,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_38_5 = new TagInfo("c:get", //$NON-NLS-1$
            38, 5,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_38_55 = new TagInfo("c:get", //$NON-NLS-1$
            38, 55,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/Entity", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_iterate_41_9 = new TagInfo("c:iterate", //$NON-NLS-1$
            41, 9,
            new String[] {
                "select", //$NON-NLS-1$
                "var", //$NON-NLS-1$
            },
            new String[] {
                "$currUpdateTable/columnTable", //$NON-NLS-1$
                "columnTable", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_42_16 = new TagInfo("c:get", //$NON-NLS-1$
            42, 16,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$columnTable/nameColumn", //$NON-NLS-1$
            } );

    public void generate(final JET2Context context, final JET2Writer __out) {
        JET2Writer out = __out;
        out.write("package ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_1_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_1_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_1_9.setRuntimeParent(null);
        _jettag_c_get_1_9.setTagInfo(_td_c_get_1_9);
        _jettag_c_get_1_9.doStart(context, out);
        _jettag_c_get_1_9.doEnd();
        out.write(";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import java.util.Calendar;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import org.apache.commons.lang3.StringUtils;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.viewers.ColumnViewer;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.viewers.TableViewerColumn;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import org.eclipse.jface.viewers.Viewer;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.updatableviewer.UpdatableItem;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import com.sneda.globalimmo.toolkit.sorter.DateComparator;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.globalimmo.toolkit.sorter.StringComparator;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.toolkit.widget.raksha.table.ColumnSorter;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        out.write("/**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * @author ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_17_12 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_17_12); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_17_12.setRuntimeParent(null);
        _jettag_c_get_17_12.setTagInfo(_td_c_get_17_12);
        _jettag_c_get_17_12.doStart(context, out);
        _jettag_c_get_17_12.doEnd();
        out.write(NL);         
        out.write(" *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" public class Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_20_22 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_20_22); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_20_22.setRuntimeParent(null);
        _jettag_c_get_20_22.setTagInfo(_td_c_get_20_22);
        _jettag_c_get_20_22.doStart(context, out);
        _jettag_c_get_20_22.doEnd();
        out.write("Sorter extends ColumnSorter {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\tprivate final Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_22_23 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_22_23); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_22_23.setRuntimeParent(null);
        _jettag_c_get_22_23.setTagInfo(_td_c_get_22_23);
        _jettag_c_get_22_23.doStart(context, out);
        _jettag_c_get_22_23.doEnd();
        out.write("ColumnEnum colonne;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write(NL);         
        out.write("\tpublic Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_25_16 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_25_16); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_25_16.setRuntimeParent(null);
        _jettag_c_get_25_16.setTagInfo(_td_c_get_25_16);
        _jettag_c_get_25_16.doStart(context, out);
        _jettag_c_get_25_16.doEnd();
        out.write("Sorter(ColumnViewer pViewer, TableViewerColumn pColumn, Tableau");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_25_118 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_25_118); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_25_118.setRuntimeParent(null);
        _jettag_c_get_25_118.setTagInfo(_td_c_get_25_118);
        _jettag_c_get_25_118.doStart(context, out);
        _jettag_c_get_25_118.doEnd();
        out.write("ColumnEnum colonne) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\tsuper(pViewer, pColumn);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\tthis.colonne = colonne;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t@Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t  public int columnCompare(Viewer viewer, Object e1, Object e2) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t\tif(e1 instanceof UpdatableItem && e2 instanceof UpdatableItem) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t\tUpdatableItem itemA = (UpdatableItem) e1;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t\tUpdatableItem itemB = (UpdatableItem) e2;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t\tif(itemA.getItem() instanceof ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_36_34 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_36_34); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_36_34.setRuntimeParent(null);
        _jettag_c_get_36_34.setTagInfo(_td_c_get_36_34);
        _jettag_c_get_36_34.doStart(context, out);
        _jettag_c_get_36_34.doEnd();
        out.write(" && itemB.getItem() instanceof ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_36_106 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_36_106); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_36_106.setRuntimeParent(null);
        _jettag_c_get_36_106.setTagInfo(_td_c_get_36_106);
        _jettag_c_get_36_106.doStart(context, out);
        _jettag_c_get_36_106.doEnd();
        out.write(") {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t\t\t");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_37_5 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_37_5); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_37_5.setRuntimeParent(null);
        _jettag_c_get_37_5.setTagInfo(_td_c_get_37_5);
        _jettag_c_get_37_5.doStart(context, out);
        _jettag_c_get_37_5.doEnd();
        out.write(" objA = (");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_37_55 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_37_55); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_37_55.setRuntimeParent(null);
        _jettag_c_get_37_55.setTagInfo(_td_c_get_37_55);
        _jettag_c_get_37_55.doStart(context, out);
        _jettag_c_get_37_55.doEnd();
        out.write(") itemA.getItem();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t\t\t");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_38_5 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_38_5); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_38_5.setRuntimeParent(null);
        _jettag_c_get_38_5.setTagInfo(_td_c_get_38_5);
        _jettag_c_get_38_5.doStart(context, out);
        _jettag_c_get_38_5.doEnd();
        out.write(" objB = (");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_38_55 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_38_55); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_38_55.setRuntimeParent(null);
        _jettag_c_get_38_55.setTagInfo(_td_c_get_38_55);
        _jettag_c_get_38_55.doStart(context, out);
        _jettag_c_get_38_55.doEnd();
        out.write(") itemB.getItem();");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t\t\t\t switch (colonne) {");  //$NON-NLS-1$        
        out.write(NL);         
        RuntimeTagElement _jettag_c_iterate_41_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "iterate", "c:iterate", _td_c_iterate_41_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_iterate_41_9.setRuntimeParent(null);
        _jettag_c_iterate_41_9.setTagInfo(_td_c_iterate_41_9);
        _jettag_c_iterate_41_9.doStart(context, out);
        while (_jettag_c_iterate_41_9.okToProcessBody()) {
            out.write("\t\t\t\t      case ");  //$NON-NLS-1$        
            RuntimeTagElement _jettag_c_get_42_16 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_42_16); //$NON-NLS-1$ //$NON-NLS-2$
            _jettag_c_get_42_16.setRuntimeParent(_jettag_c_iterate_41_9);
            _jettag_c_get_42_16.setTagInfo(_td_c_get_42_16);
            _jettag_c_get_42_16.doStart(context, out);
            _jettag_c_get_42_16.doEnd();
            out.write(":");  //$NON-NLS-1$        
            out.write(NL);         
            out.write("\t\t\t          return compare;");  //$NON-NLS-1$        
            out.write(NL);         
            _jettag_c_iterate_41_9.handleBodyContent(out);
        }
        _jettag_c_iterate_41_9.doEnd();
        out.write("\t\t\t      default:");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t\t        return 0;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t\t    }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t return 0;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t\t private int compareString(String str1, String str2) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t\t return new StringUtils().compare(str1, str2);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t  \t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t  \tprivate int compareDate(Calendar date1, Calendar date2) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t    \treturn DateComparator.getInstance().compare(date1, date2);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t  \t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t  \tprivate int compareInteger(int x, int y){");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t\t\treturn Integer.compare(x,y);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t  \t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t  \tprivate int compareDouble(double x, double y) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    \t\treturn Double.compare(x, y);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  \t\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  \t\tprivate int compareFloat(float x, float y) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    \t\treturn Float.compare(x, y);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  \t\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  \t\tprivate int compareByte(byte x, byte y) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    \t\treturn Byte.compare(x, y);");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  \t\t}");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t}");  //$NON-NLS-1$        
        out.write(NL);         
    }
}

package com.sneda.dev.helpers.jet.compiled;

import org.eclipse.jet.JET2Context;
import org.eclipse.jet.JET2Template;
import org.eclipse.jet.JET2Writer;
import org.eclipse.jet.taglib.RuntimeTagElement;
import org.eclipse.jet.taglib.TagInfo;

public class _jet_MyNameNMCMapperjava implements JET2Template {
    private static final String _jetns_c = "org.eclipse.jet.controlTags"; //$NON-NLS-1$

    public _jet_MyNameNMCMapperjava() {
        super();
    }

    private static final String NL = System.getProperty("line.separator"); //$NON-NLS-1$
    
    private static final TagInfo _td_c_get_1_9 = new TagInfo("c:get", //$NON-NLS-1$
            1, 9,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/packageCheminNMCMapper", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_15_14 = new TagInfo("c:get", //$NON-NLS-1$
            15, 14,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/name", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_15_89 = new TagInfo("c:get", //$NON-NLS-1$
            15, 89,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_24_10 = new TagInfo("c:get", //$NON-NLS-1$
            24, 10,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_25_4 = new TagInfo("c:get", //$NON-NLS-1$
            25, 4,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_25_76 = new TagInfo("c:get", //$NON-NLS-1$
            25, 76,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteVariable", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_25_157 = new TagInfo("c:get", //$NON-NLS-1$
            25, 157,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteObjet", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_29_4 = new TagInfo("c:get", //$NON-NLS-1$
            29, 4,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteVariable", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_30_4 = new TagInfo("c:get", //$NON-NLS-1$
            30, 4,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteVariable", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_41_5 = new TagInfo("c:get", //$NON-NLS-1$
            41, 5,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteVariable", //$NON-NLS-1$
            } );
    private static final TagInfo _td_c_get_43_12 = new TagInfo("c:get", //$NON-NLS-1$
            43, 12,
            new String[] {
                "select", //$NON-NLS-1$
            },
            new String[] {
                "$currEditor/parametresEditorIhmEtendue/nomEntiteVariable", //$NON-NLS-1$
            } );

    public void generate(final JET2Context context, final JET2Writer __out) {
        JET2Writer out = __out;
        out.write("package ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_1_9 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_1_9); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_1_9.setRuntimeParent(null);
        _jettag_c_get_1_9.setTagInfo(_td_c_get_1_9);
        _jettag_c_get_1_9.doStart(context, out);
        _jettag_c_get_1_9.doEnd();
        out.write(";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import java.util.List;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("import com.progress.common.exception.ProException;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.progress.open4gl.ProDataObject;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.applicativearchitecture.domains.OIDImpl;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.appserver.core.ProDataObjectMapper;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("import com.sneda.appserver.util.ProDataObjectUtils;");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("/**");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" * @author saregnier");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" *");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(" */");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("public class ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_15_14 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_15_14); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_15_14.setRuntimeParent(null);
        _jettag_c_get_15_14.setTagInfo(_td_c_get_15_14);
        _jettag_c_get_15_14.doStart(context, out);
        _jettag_c_get_15_14.doEnd();
        out.write("NMCMapper implements ProDataObjectMapper<");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_15_89 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_15_89); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_15_89.setRuntimeParent(null);
        _jettag_c_get_15_89.setTagInfo(_td_c_get_15_89);
        _jettag_c_get_15_89.doStart(context, out);
        _jettag_c_get_15_89.doEnd();
        out.write("> {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public String getTableName() {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t  // TODO renommer avec le nom de la table temporaire");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    return \"tt\";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("  @Override");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  public ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_24_10 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_24_10); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_24_10.setRuntimeParent(null);
        _jettag_c_get_24_10.setTagInfo(_td_c_get_24_10);
        _jettag_c_get_24_10.doStart(context, out);
        _jettag_c_get_24_10.doEnd();
        out.write(" mapProDataObject(ProDataObject proDataObject, int rowNum) throws ProException {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t  ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_25_4 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_25_4); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_25_4.setRuntimeParent(null);
        _jettag_c_get_25_4.setTagInfo(_td_c_get_25_4);
        _jettag_c_get_25_4.doStart(context, out);
        _jettag_c_get_25_4.doEnd();
        out.write(" ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_25_76 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_25_76); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_25_76.setRuntimeParent(null);
        _jettag_c_get_25_76.setTagInfo(_td_c_get_25_76);
        _jettag_c_get_25_76.doStart(context, out);
        _jettag_c_get_25_76.doEnd();
        out.write(" = new ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_25_157 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_25_157); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_25_157.setRuntimeParent(null);
        _jettag_c_get_25_157.setTagInfo(_td_c_get_25_157);
        _jettag_c_get_25_157.doStart(context, out);
        _jettag_c_get_25_157.doEnd();
        out.write("(new OIDImpl(");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        proDataObject.getString(\"OID\"), NomenclatureEntityType.)); // TODO compléter avec la bonne Nomenclature et la bonne NMC");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("\t  // TODO compléter le remplissage des attributs en Java avec les champs en Progress + ajouter des champs si nécessaire");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t  ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_29_4 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_29_4); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_29_4.setRuntimeParent(null);
        _jettag_c_get_29_4.setTagInfo(_td_c_get_29_4);
        _jettag_c_get_29_4.doStart(context, out);
        _jettag_c_get_29_4.doEnd();
        out.write(".setCode(\"\");");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("\t  ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_30_4 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_30_4); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_30_4.setRuntimeParent(null);
        _jettag_c_get_30_4.setTagInfo(_td_c_get_30_4);
        _jettag_c_get_30_4.doStart(context, out);
        _jettag_c_get_30_4.doEnd();
        out.write(".setLibelle(\"\");");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("    // TODO à compléter si il existe des relations entre deux tables");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    if (ProDataObjectUtils.isRelation(proDataObject, \"\")) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      List<ProDataObject> pdoList = ProDataObjectUtils.getChildRelationResult(proDataObject,");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("          \"\");");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      for (ProDataObject dataObject2 : pdoList) {");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("        // TODO si relation : remplissage des champs appartenant à la relation");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("      }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("    }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("    ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_41_5 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_41_5); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_41_5.setRuntimeParent(null);
        _jettag_c_get_41_5.setTagInfo(_td_c_get_41_5);
        _jettag_c_get_41_5.doStart(context, out);
        _jettag_c_get_41_5.doEnd();
        out.write(".setActif(proDataObject.getBoolean(\"LACTIF\"));");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("    return ");  //$NON-NLS-1$        
        RuntimeTagElement _jettag_c_get_43_12 = context.getTagFactory().createRuntimeTag(_jetns_c, "get", "c:get", _td_c_get_43_12); //$NON-NLS-1$ //$NON-NLS-2$
        _jettag_c_get_43_12.setRuntimeParent(null);
        _jettag_c_get_43_12.setTagInfo(_td_c_get_43_12);
        _jettag_c_get_43_12.doStart(context, out);
        _jettag_c_get_43_12.doEnd();
        out.write(";");  //$NON-NLS-1$        
        out.write(NL);         
        out.write("  }");  //$NON-NLS-1$        
        out.write(NL);         
        out.write(NL);         
        out.write("}");  //$NON-NLS-1$        
        out.write(NL);         
    }
}

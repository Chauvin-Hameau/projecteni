NOTA BENE 
---------

 - L'encoding des projets est l'UTF-8.
 - Utiliser l'extension ".jet2" qui force la prise en compte de l'UTF-8 par JET
 
LIENS
-----

JET Developer Guide :
  http://help.eclipse.org/helios/index.jsp?topic=/org.eclipse.jet.doc/references/taglibs/controlTags/overview.html